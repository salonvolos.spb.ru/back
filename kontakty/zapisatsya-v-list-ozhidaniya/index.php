<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "");
$APPLICATION->SetTitle("Записаться в лист ожидания");
$APPLICATION->SetPageProperty("title", "Записаться в лист ожидания");
?><div class="container">
    <div class="content">

      <h1><? $APPLICATION->ShowTitle("h1", false) ?></h1>

          <p>Берегите себя, оставайтесь дома. Вы&nbsp;можете записаться в&nbsp;лист ожидания на&nbsp;любую услугу. <strong>Перезвоним вам, как только получим разрешение принимать клиентов, и&nbsp;согласуем дату визита.</strong></p>
          <div>
            <div class="block-marked block-marked--blue block-marked--centered" style="font-size: 1.125rem; line-height: 1.5"><strong>Тем, кто записался в&nbsp;лист ожидания до&nbsp;конца мая&nbsp;&mdash; в&nbsp;подарок бонус 2000₽ на&nbsp;личный депозит.<br>Бонусные рубли позволяют оплатить до&nbsp;30% от&nbsp;стоимости услуги.</strong></div>
          </div>
          <h2>Как записаться?</h2>
          <div class="block-wrap  ">
            <div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width12 block-wrap__item_s-width6">
              <h3>Отправьте заявку через форму</h3>

              <form
                  class="consultation-form zapisatsya-v-list-ozhidaniya validated-form submitJS"
                  method="post"
              >

                  <input type="text" class="req_name" name="req_name" value="">

                <div class="consultation-form__row">
                  <div class="consultation-form__label">
                    <label for="consultation-form__name">Имя</label>
                  </div>
                  <div class="consultation-form__input">
                    <input
                        id="consultation-form__name"
                        type="text"
                        name="fio"
                        class="validated required"
                    >
                    <div class="validation-msg validation-msg-required">Обязательное поле</div>
                  </div>
                </div>

                <div class="consultation-form__row">
                  <div class="consultation-form__label">
                    <label for="consultation-form__tel">Телефон</label>
                  </div>
                  <div class="consultation-form__input">
                    <input
                        id="consultation-form__tel"
                        type="tel"
                        name="phone"
                        class="validated required"
                    >
                    <div class="validation-msg validation-msg-required">Обязательное поле</div>
                  </div>
                </div>

                <div class="consultation-form__row">
                  <div class="consultation-form__label">
                    <label for="consultation-form__question">Услуга</label>
                  </div>
                  <div class="consultation-form__input">
                    <textarea
                        class="validated required"
                        id="consultation-form__question"
                        name="question"
                    ></textarea>
                    <div class="validation-msg validation-msg-required">Обязательное поле</div>
                  </div>
                </div>

                <div class="consultation-form__row consultation-form__row--agreement">
                    <div class="consultation-form__input">
                        <input type="checkbox" required checked id="consultation-form__agreement">
                    </div>
                    <div class="consultation-form__label">
                        <label for="consultation-form__agreement">Нажимая на кнопку, я даю свое <a href="/soglasheniya-na-obrabotku-personalnykh-dannykh/">согласие на обработку своих персональных данных</a>.</label>
                    </div>
                </div>

                <div class="consultation-form__row consultation-form__row--submit">
                  <button class="btn" type="submit">Записаться</button>
                </div>

              </form>

            </div>
            <div class="block-wrap__item block-wrap__item_xl-width1 block-wrap__item_l-width1 block-wrap__item_m-width12 block-wrap__item_s-width6">
              <p style="margin-top: 1.95rem; text-align: center;"><strong>или</strong></p>
            </div>
            <div class="block-wrap__item block-wrap__item_xl-width5 block-wrap__item_l-width5 block-wrap__item_m-width12 block-wrap__item_s-width6">
              <h3>Позвоните нам <span style="white-space: nowrap; font: inherit;">+7 (812) 385-01-51</span></h3>
            </div>
          </div>
    </div>
</div>

<?
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');
