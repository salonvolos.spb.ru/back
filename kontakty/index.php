<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Контактная информация салона Hair Care Center: адрес, телефоны, почта, skype, схема проезда от метро, информация о парковке.");
$APPLICATION->SetTitle("Контакты");
$APPLICATION->SetPageProperty("title", "Контакты");
?>

<div class="page-subsection">
  <div class="grid">
    <div class="grid__cell grid__cell--m--6 grid__cell--s--12 grid__cell--xs--12">
      <h2 class="h3">Адрес</h2>
      <p>г. Санкт-Петербург <br> наб. Обводного канала д. 108</p>
      <p>Работаем ежедневно: <br> пн-пт: с&nbsp;10:00 до&nbsp;21:00 <br> выходные и праздничные дни: с 10.00 до 19.00.</p>
      <p><strong>График работы в праздники</strong>:
        <br> 7.03 с&nbsp;10:00 до&nbsp;19:00 
        <br> 8.03&nbsp;&mdash; нерабочий день 
			</p>
    </div>
    <div class="grid__cell grid__cell--m--6 grid__cell--s--12 grid__cell--xs--12">
      <div class="grid"> 
        <div class="grid__cell grid__cell--xs--12">
          <h2 class="h3">Телефон</h2><a class="page-contacts__phone par" href="tel:+78123850151">+7 (812) 385-01-51</a>
        </div>
        <div class="grid__cell grid__cell--xs--12">
          <h2 class="h3">Мессенджеры</h2>
          <div class="grid grid--align-items--center par">
            <div class="grid__cell grid__cell--s--3 grid__cell--xs--12">
              <div class="link-icons link-icons--inline"><a class="link-icons__link link-icons__link--whatsapp" href="whatsapp://send?phone=79817245628&text=" target="_blank"></a><a class="link-icons__link link-icons__link--viber" href="viber://chat?number=79817245628" target="_blank"></a><a class="link-icons__link link-icons__link--telegram" href="tg://resolve?domain=HairCareCenter" target="_blank"></a>
              </div>
            </div>
            <div class="grid__cell grid__cell--s--9 grid__cell--xs--12">
              <p>Вы&nbsp;можете связаться с&nbsp;нами при помощи приложений Viber, WhatsApp и Telegram, добавив наш номер +7&nbsp;(981)&nbsp;724-56-28 в&nbsp;список контактов.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="grid__cell grid__cell--l--6 grid__cell--m--6 grid__cell--s--12 grid__cell--xs--12">
      <div class="page-contacts__map">
        <script type="text/javascript" data-skip-moving="true" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A9df9fa03e5ad1d4ecbf1c29c7d7a832f05fc16065206a604260854b0a0b8f5e3&amp;width=100%&amp;lang=ru_RU&amp;scroll=true"></script>
      </div>
    </div>
    <div class="grid__cell grid__cell--l--6 grid__cell--m--6 grid__cell--s--12 grid__cell--xs--12 page-contacts__route block-marked block-marked--to-border block-marked--to-border--right">
      <h2 class="h3">Припарковаться на&nbsp;машине</h2>
      <p>Двигайтесь вдоль Обводного канала от&nbsp;Балтийского вокзала в&nbsp;сторону Московского пр.</p>
      <p>Заезд есть перед и&nbsp;после здания.</p>
      <p>Вдоль здания находятся парковочные места. Обычно 2-3 места свободные. Если все занято, то&nbsp;правее от&nbsp;здания есть площадка для машин.</p>
      <h2 class="h3">Добраться пешком</h2>
      <div class="grid par">
        <div class="grid__cell grid__cell--xs--12"><span class="icon-inline icon-inline--metro"><strong>Балтийская</strong></span>
          <p class="page-contacts__route-time">10&nbsp;минут</p>
          <p>После выхода из&nbsp;метро двигайтесь вдоль Обводного канала в&nbsp;сторону Московского пр. Ориентируйтесь на&nbsp;церковь. После нее будет сквозной проход через дом, далее желтое здание.</p>
        </div>
        <div class="grid__cell grid__cell--xs--12"><span class="icon-inline icon-inline--metro"><strong>Технологический институт</strong></span>
          <p class="page-contacts__route-time">10&nbsp;минут</p>
        </div>
        <? /* <div class="grid__cell grid__cell--xs--12"><span class="icon-inline icon-inline--metro"><strong>Фрунзенская</strong></span>
          <p class="page-contacts__route-time">5&nbsp;минут</p>
          <p>После выхода из&nbsp;метро поверните налево и&nbsp;двигайтесь вдоль Московского&nbsp;пр.&nbsp;до&nbsp;Обводного канала. Затем поверните налево и&nbsp;двигайтесь вдоль Обводного канала до&nbsp;желтого здания.</p> 
        </div> */?>
      </div>
    </div>
  </div>
</div>
<div class="page-subsection">
  <h2 class="h3">Фотографии офиса </h2>
  <p>Вход находится справа от центра здания</p>
  <div class="gallery par"><a class="gallery__item" href="/images/contacts/office-1.webp" data-glightbox>
      <picture>
        <source srcset="/images/contacts/office-1.webp" type="image/webp"/>
        <source srcset="/images/contacts/office-1.jpg"/><img class="border-radius border-radius--small" src="/images/contacts/office-1.jpg" loading="lazy" decoding="async" alt="Фотографии офиса"/>
      </picture></a><a class="gallery__item" href="/images/contacts/office-2.webp" data-glightbox>
      <picture>
        <source srcset="/images/contacts/office-2.webp" type="image/webp"/>
        <source srcset="/images/contacts/office-2.jpg"/><img class="border-radius border-radius--small" src="/images/contacts/office-2.jpg" loading="lazy" decoding="async" alt="Фотографии офиса"/>
      </picture></a><a class="gallery__item" href="/images/contacts/office-3.webp" data-glightbox>
      <picture>
        <source srcset="/images/contacts/office-3.webp" type="image/webp"/>
        <source srcset="/images/contacts/office-3.jpg"/><img class="border-radius border-radius--small" src="/images/contacts/office-3.jpg" loading="lazy" decoding="async" alt="Фотографии офиса"/>
      </picture></a><a class="gallery__item" href="/images/contacts/office-4.webp" data-glightbox>
      <picture>
        <source srcset="/images/contacts/office-4.webp" type="image/webp"/>
        <source srcset="/images/contacts/office-4.jpg"/><img class="border-radius border-radius--small" src="/images/contacts/office-4.jpg" loading="lazy" decoding="async" alt="Фотографии офиса"/>
      </picture></a><a class="gallery__item" href="/images/contacts/office-5.webp" data-glightbox>
      <picture>
        <source srcset="/images/contacts/office-5.webp" type="image/webp"/>
        <source srcset="/images/contacts/office-5.jpg"/><img class="border-radius border-radius--small" src="/images/contacts/office-5.jpg" loading="lazy" decoding="async" alt="Фотографии офиса"/>
      </picture></a><a class="gallery__item" href="/images/contacts/office-6.webp" data-glightbox>
      <picture>
        <source srcset="/images/contacts/office-6.webp" type="image/webp"/>
        <source srcset="/images/contacts/office-6.jpg"/><img class="border-radius border-radius--small" src="/images/contacts/office-6.jpg" loading="lazy" decoding="async" alt="Фотографии офиса"/>
      </picture></a><a class="gallery__item" href="/images/contacts/office-7.webp" data-glightbox>
      <picture>
        <source srcset="/images/contacts/office-7.webp" type="image/webp"/>
        <source srcset="/images/contacts/office-7.jpg"/><img class="border-radius border-radius--small" src="/images/contacts/office-7.jpg" loading="lazy" decoding="async" alt="Фотографии офиса"/>
      </picture></a><a class="gallery__item" href="/images/contacts/office-8.webp" data-glightbox>
      <picture>
        <source srcset="/images/contacts/office-8.webp" type="image/webp"/>
        <source srcset="/images/contacts/office-8.jpg"/><img class="border-radius border-radius--small" src="/images/contacts/office-8.jpg" loading="lazy" decoding="async" alt="Фотографии офиса"/>
      </picture></a><a class="gallery__item" href="/images/contacts/office-9.webp" data-glightbox>
      <picture>
        <source srcset="/images/contacts/office-9.webp" type="image/webp"/>
        <source srcset="/images/contacts/office-9.jpg"/><img class="border-radius border-radius--small" src="/images/contacts/office-9.jpg" loading="lazy" decoding="async" alt="Фотографии офиса"/>
      </picture></a><a class="gallery__item" href="/images/contacts/office-10.webp" data-glightbox>
      <picture>
        <source srcset="/images/contacts/office-10.webp" type="image/webp"/>
        <source srcset="/images/contacts/office-10.jpg"/><img class="border-radius border-radius--small" src="/images/contacts/office-10.jpg" loading="lazy" decoding="async" alt="Фотографии офиса"/>
      </picture></a><a class="gallery__item" href="/images/contacts/office-11.webp" data-glightbox>
      <picture>
        <source srcset="/images/contacts/office-11.webp" type="image/webp"/>
        <source srcset="/images/contacts/office-11.jpg"/><img class="border-radius border-radius--small" src="/images/contacts/office-11.jpg" loading="lazy" decoding="async" alt="Фотографии офиса"/>
      </picture></a><a class="gallery__item" href="/images/contacts/office-12.webp" data-glightbox>
      <picture>
        <source srcset="/images/contacts/office-12.webp" type="image/webp"/>
        <source srcset="/images/contacts/office-12.jpg"/><img class="border-radius border-radius--small" src="/images/contacts/office-12.jpg" loading="lazy" decoding="async" alt="Фотографии офиса"/>
      </picture></a><a class="gallery__item" href="/images/contacts/office-13.webp" data-glightbox>
      <picture>
        <source srcset="/images/contacts/office-13.webp" type="image/webp"/>
        <source srcset="/images/contacts/office-13.jpg"/><img class="border-radius border-radius--small" src="/images/contacts/office-13.jpg" loading="lazy" decoding="async" alt="Фотографии офиса"/>
      </picture></a><a class="gallery__item" href="/images/contacts/office-14.webp" data-glightbox>
      <picture>
        <source srcset="/images/contacts/office-14.webp" type="image/webp"/>
        <source srcset="/images/contacts/office-14.jpg"/><img class="border-radius border-radius--small" src="/images/contacts/office-14.jpg" loading="lazy" decoding="async" alt="Фотографии офиса"/>
      </picture></a>
  </div>
</div>
<div class="page-subsection">
  <div class="grid">
    <div class="grid__cell grid__cell--m--12 grid__cell--xs--12">
      <h2 class="h3">3D-тур</h2>
      <div class="page-contacts__tour par">
        <?/* <div class="iframe-responsive">
          <div class="iframe-responsive__iframe">
            <iframe src="https://www.google.com/maps/embed?pb=!1m0!3m2!1sru!2s!4v1486634358815!6m8!1m7!1sK35p8NtUkMUAAAQvxa493A!2m2!1d59.90796101212884!2d30.31258920335586!3f221.86485814753792!4f-14.060279966658854!5f0.7820865974627469" allowfullscreen="allowfullscreen"></iframe>
          </div>
        </div> */?>
        <iframe src="/3dtur/index.html?media-index=1" name="Виртуальный тур" width="100%" height="600" frameborder="0" allow="fullscreen; accelerometer; gyroscope; magnetometer; vr; xr; xr-spatial-tracking; autoplay; camera; microphone" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" oallowfullscreen="true" msallowfullscreen="true"></iframe>
      </div>
    </div>
  </div>
</div>
<div class="page-subsection"> 
  <h2 class="h3">Видео 360 градусов</h2>
  <video class="video-js vjs-default-skin par" playsinline id="video360" controls preload="auto" poster="/video/3dtur.jpg">
    <source src="/video/3dtur.webm" type="video/webm">
  </video>
</div>

<?
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');
