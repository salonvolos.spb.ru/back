<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)die();

global $APPLICATION;

$arOrder = ["SORT" => "DESC"];
$arFilter = [
    "IBLOCK_ID" => ID_IBLOCK_CONSULTATION,
    "ACTIVE" => "Y",
    "GLOBAL_ACTIVE" => "Y",
    "DEPTH_LEVEL" => "1",
];
$bIncCnt = true;
$Select = [
    "ID",
    "NAME",
    "IBLOCK_ID",
    "SECTION_PAGE_URL"
];
$NavStartParams = false;

$res = CIBlockSection::GetList($arOrder, $arFilter, $bIncCnt, $Select, $NavStartParams);

$aMenuLinksExt = [];
// наполняем массив меню пунктами меню
while($sectionObj = $res->GetNext())
{
     $aMenuLinksExt[] = [
        $sectionObj["NAME"],
        $sectionObj["SECTION_PAGE_URL"],
        [],
        ["ELEMENT_CNT" => $sectionObj["ELEMENT_CNT"]],
        ""
    ];
}

$aMenuLinks = array_merge($aMenuLinks, $aMenuLinksExt); // меню сформировано