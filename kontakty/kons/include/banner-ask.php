<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="page-subsection">
    <div class="block-marked consultation-banner">
        <div class="grid">

            <div class="grid__cell grid__cell--l--8 grid__cell--m--8 grid__cell--s--12 grid__cell--xs--12">
                <p>Отвечаем на вопросы про услуги салона красоты.</p>
                <p>Если Вы не нашли ответ на свой вопрос, обратитесь за консультацией к нашим специалистам.</p>
            </div>

            <div class="grid__cell grid__cell--l--4 grid__cell--m--4 grid__cell--s--12 grid__cell--xs--12 consultation-banner__aside">
                <a class="button" href="/kontakty/kons/ask.html">Спросить совет</a>
                <p class="consultation-banner__advice"><strong>Советуем с 2007 года</strong></p>
            </div>

        </div>
    </div>
</div>
