<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<h3 class="aside-header">Категории</h3>
<?
$APPLICATION->IncludeComponent(
    "bitrix:menu",
    "consultation-categories",
    Array(
        "ROOT_MENU_TYPE" => "left",
        "MAX_LEVEL" => "2",
        "CHILD_MENU_TYPE" => "top_sub",
        "USE_EXT" => "Y",
        "DELAY" => "N",
        "ALLOW_MULTI_SELECT" => "N",
        "MENU_CACHE_TYPE" => "N",
        "MENU_CACHE_TIME" => "3600",
        "MENU_CACHE_USE_GROUPS" => "Y",
        "MENU_CACHE_GET_VARS" => "",
    ),
    false
);

