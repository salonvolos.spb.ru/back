<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Консультация по различным вопросам, связанным с услугами салона красоты. Задать вопрос парикмахеру.");
$APPLICATION->SetTitle("Консультация");
$APPLICATION->SetPageProperty("title", "Консультация парикмахера");
?>
<? if(isConsultationDetailPage()): ?>
<section class="container page-section">
    <div class="content">
<? endif ?>

        <div class="page-subsection">
            <div class="grid">

                <div class="grid__cell grid__cell--m--9 grid__cell--s--12 grid__cell--xs--12">
					<? if(isConsultationDetailPage()): ?>
						<? include __DIR__."/include/component.php" ?>
						<? include __DIR__."/include/banner-ask.php" ?>
					<? else: ?>
						<? include __DIR__."/include/banner-ask.php" ?>
						<? include __DIR__."/include/component.php" ?>
					<? endif ?>
                </div>

                <div class="grid__cell grid__cell--m--3 grid__cell--s--12 grid__cell--xs--12">
                    <? include __DIR__."/include/categories.php" ?>
                </div>

            </div>
        </div>

<? if(isConsultationDetailPage()): ?>
    </div>
</section>
<? endif ?>
<?
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');