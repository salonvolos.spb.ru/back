<?php
if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $data = json_decode(file_get_contents('php://input'));
    if (!empty($data) && $data->status == 'ok'){
        // Путь к файлу
        $file_path = $_SERVER['DOCUMENT_ROOT'].'/bitrix/templates/tpl_dev/components/bitrix/news.list/our_works/component_epilog.php';

        if (!file_exists($file_path)) {
            echo json_encode("Ошибка: Файл не существует.", JSON_UNESCAPED_UNICODE);
            exit;
        }

        if (!is_readable($file_path) || !is_writable($file_path)) {
            echo json_encode("Ошибка: Нет доступа к файлу.", JSON_UNESCAPED_UNICODE);
            exit;
        }

        $file_contents = file_get_contents($file_path);

        if ($file_contents === false) {
            echo "Ошибка: Не удалось прочитать файл.";
            exit;
        }






        if ($contentsName === null) {
            echo  json_encode("Ошибка: Не удалось выполнить замену.");
            exit;
        }

        if (file_put_contents($file_path, $contentPrice) === false) {
            echo json_encode ("Ошибка: Не удалось записать обновленное содержимое в файл.", JSON_UNESCAPED_UNICODE);
            exit;
        }


        echo json_encode('ok');
    }


}
