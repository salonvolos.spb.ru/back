<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {die();}

/** @var array $arParams */

$serviceOnlineCode = $arParams["USER_PARAM_ONLINE_CODE"];
$serviceLink = $arParams["USER_PARAM_SERVICE_LINK"];
?>
<section class="container page-section page-section--appointment home-appointment">
    <div class="content grid grid--justify--flex-end grid--align-items--center home-appointment__inner">
        <div class="grid__cell grid__cell--l--6 grid__cell--m--6 grid__cell--xs--12 grid grid--justify--center home-appointment__info">
            <div class="grid__cell grid__cell--l--8 grid__cell--xs--12">

                <h2 class="home-appointment__title not-menu-content">Скидка 5% при записи с сайта!</h2>

                <a
                    class="button home-appointment__button <? if($serviceLink != "/uslugi/narashchivanie-volos/"): ?> ms_booking<? endif ?>"
					<? if($serviceLink == "/uslugi/narashchivanie-volos/"): ?>
                        href="#hair-extention-sign" data-fancybox data-name="наращивание"
					<? else: ?>
                        href="#"
						<? if($serviceOnlineCode): ?>
                            data-url="https://n<?= $serviceOnlineCode ?>.yclients.com/company:170918"
						<? else: ?>
                            data-url="https://n371027.yclients.com/company:170918"
						<? endif ?>
					<? endif ?>
                >Выбрать время и специалиста</a>

            </div>
        </div>
    </div>
</section>
