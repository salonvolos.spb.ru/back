<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)die();

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
global $APPLICATION;
$aMenuLinksExt=$APPLICATION->IncludeComponent("bitrix:menu.sections", "", array(
    "IS_SEF" => "Y",
    "SEF_BASE_URL" => "",
    "SECTION_PAGE_URL" => "#SECTION_CODE_PATH#/",
    "DETAIL_PAGE_URL" => "#SECTION_CODE_PATH#/#ELEMENT_CODE#/",
    "IBLOCK_TYPE" => "SALON",
    "IBLOCK_ID" => ID_IBLOCK_SERVICES,
    "DEPTH_LEVEL" => "3",
    "CACHE_TYPE" => "N",
    "CACHE_TIME" => "3600"
),
    false
);

// меню сформировано
$aMenuLinks = array_merge($aMenuLinks, $aMenuLinksExt);