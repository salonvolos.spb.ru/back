<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Информация о салоне красоты Hair Care Center: история создания, развитие, концепция, преимущества, сотрудники.");
$APPLICATION->SetTitle("");
$APPLICATION->SetPageProperty("title", "Цены");
$APPLICATION->ShowNavChain();

$priceSubsectionCode = parse_url($_GET["price_subsection_code"]);
$priceSubsectionCode = $priceSubsectionCode["path"];
$priceSubsectionCode = stripslashes(trim(htmlspecialchars($priceSubsectionCode, ENT_QUOTES)));
?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("О нас");
$APPLICATION->SetPageProperty("title", "О салоне");
$APPLICATION->ShowNavChain();

$priceSubsectionCode = parse_url($_GET["price_subsection_code"]);
$priceSubsectionCode = $priceSubsectionCode["path"];
$priceSubsectionCode = stripslashes(trim(htmlspecialchars($priceSubsectionCode, ENT_QUOTES)));
?>

<div class="page-subsection">
    <p>Дата открытия салона пришлась на лето 2002 года. Салон создавался именно для тех, кого заботит состояние волосяного покрова.</p>
    <p>Hair Care Center — это не что иное, как специализированный салон, где заняты специалисты высокого квалификационного уровня, где, кроме предоставления различных парикмахерских услуг, презентуется и множество методик, связанных с лечением волос.</p>

    <div class="block-marked par">
            <p>Мы называем себя «салоном одного клиента»</p>
    </div>
    <p>Мы именуем себя «салоном одного клиента». По этой причине каждый, кто нас посещает, может быть спокоен относительно конфиденциальности его проблемы. Ведь к нему будет применен индивидуальный подход, а также чуткое отношение персонала.</p>
    <p>За все время функционирования центра мы смогли предоставить помощь десяткам людей. Почти каждый клиент становится постоянным.</p>
    <p>Сотрудники центра — творческие личности, которые к решению такой значимой проблемы как утрата волос стараются приложить максимум творчества. В нашем салоне предоставляются многочисленные парикмахерские услуги, поэтому вы имеете возможность не только вернуть здоровье коже головы и волосам, но также и сменить собственный облик: сменить прическу, подобрать оттенок волос и многое другое.</p>
    <h2 class='h3'>Наши выгодные отличия:</h2>
    <ul class="unordered-list par">
        <li class="unordered-list__item">Высокий уровень профессионализма, обширные практические навыки и теоретические знания персонала, который постоянно повышает уровень собственной квалификации;</li>
        <li class="unordered-list__item">Внимательное отношение, индивидуальный подход к каждому клиенту, комфортные условия пребывания в салоне;</li>
        <li class="unordered-list__item">Демократические расценки. Действующая система скидок для постоянных клиентов. Ясная, четкая и прозрачная система подготовки счетов за услуги;</li>
    </ul>
</div>

<?
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');
