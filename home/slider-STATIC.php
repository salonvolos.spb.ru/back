<section class="container page-section page-section--home-header page-section--no-padding--top">
	<div class="content">
		<div class="slider home-header">
			<div class="slider__slides swiper">

				<div class="slider__wrapper swiper-wrapper">
					<div class="slide swiper-slide home-header__slide">
						<div class="grid">
							<div class="grid__cell grid__cell--l--6 grid__cell--m--8 grid__cell--xs--12 home-header__info">
								<div class="home-header__title">Салон для ценителей красоты и здоровья волос</div>
								<div class="home-header__description">Сохраняем красоту и здоровое состояние волос за счет знаний в трихологии и подбирая профессиональные уходовые процедуры</div>
								<a class="button home-header__button" href="#">Выбрать мастера
								</a>
							</div>
							<div class="grid__cell grid__cell--l--6 grid__cell--m--4 grid__cell--xs--12 home-header__image hidden-s hidden-xs">
								<picture>
									<source srcset="/images/home-header__image.webp, /images/home-header__image@2x.webp 2x" type="image/webp"/>
									<source srcset="/images/home-header__image.png, /images/home-header__image@2x.png 2x"/><img src="/images/home-header__image.png" loading="lazy" decoding="async"/>
								</picture>
							</div>
						</div>
					</div>
					<div class="slide swiper-slide home-header__slide">
						<div class="grid">
							<div class="grid__cell grid__cell--l--6 grid__cell--m--8 grid__cell--xs--12 home-header__info">
								<div class="home-header__title">Салон для ценителей красоты и здоровья волос</div>
								<div class="home-header__description">Сохраняем красоту и здоровое состояние волос за счет знаний в трихологии и подбирая профессиональные уходовые процедуры</div>
								<a class="button home-header__button" href="#">Выбрать мастера
								</a>
							</div>
							<div class="grid__cell grid__cell--l--6 grid__cell--m--4 grid__cell--xs--12 home-header__image hidden-s hidden-xs">
								<picture>
									<source srcset="/images/home-header__image.webp, /images/home-header__image@2x.webp 2x" type="image/webp"/>
									<source srcset="/images/home-header__image.png, /images/home-header__image@2x.png 2x"/><img src="/images/home-header__image.png" loading="lazy" decoding="async"/>
								</picture>
							</div>
						</div>
					</div>
					<div class="slide swiper-slide home-header__slide">
						<div class="grid">
							<div class="grid__cell grid__cell--l--6 grid__cell--m--8 grid__cell--xs--12 home-header__info">
								<div class="home-header__title">Салон для ценителей красоты и здоровья волос</div>
								<div class="home-header__description">Сохраняем красоту и здоровое состояние волос за счет знаний в трихологии и подбирая профессиональные уходовые процедуры</div>
								<a class="button home-header__button" href="#">Выбрать мастера
								</a>
							</div>
							<div class="grid__cell grid__cell--l--6 grid__cell--m--4 grid__cell--xs--12 home-header__image hidden-s hidden-xs">
								<picture>
									<source srcset="/images/home-header__image.webp, /images/home-header__image@2x.webp 2x" type="image/webp"/>
									<source srcset="/images/home-header__image.png, /images/home-header__image@2x.png 2x"/><img src="/images/home-header__image.png" loading="lazy" decoding="async"/>
								</picture>
							</div>
						</div>
					</div>
					<div class="slide swiper-slide home-header__slide">
						<div class="grid">
							<div class="grid__cell grid__cell--l--6 grid__cell--m--8 grid__cell--xs--12 home-header__info">
								<div class="home-header__title">Салон для ценителей красоты и здоровья волос</div>
								<div class="home-header__description">Сохраняем красоту и здоровое состояние волос за счет знаний в трихологии и подбирая профессиональные уходовые процедуры</div>
								<a class="button home-header__button" href="#">Выбрать мастера
								</a>
							</div>
							<div class="grid__cell grid__cell--l--6 grid__cell--m--4 grid__cell--xs--12 home-header__image hidden-s hidden-xs">
								<picture>
									<source srcset="/images/home-header__image.webp, /images/home-header__image@2x.webp 2x" type="image/webp"/>
									<source srcset="/images/home-header__image.png, /images/home-header__image@2x.png 2x"/><img src="/images/home-header__image.png" loading="lazy" decoding="async"/>
								</picture>
							</div>
						</div>
					</div>
				</div>

				<div class="slider__arrow slider__arrow--prev"></div>
				<div class="slider__arrow slider__arrow--next"></div>

				<div class="slider__pagination"></div>

			</div>
		</div>
	</div>
</section>
