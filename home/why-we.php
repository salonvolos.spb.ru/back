<section class="container page-section page-section--home-why-we">
	<div class="content">

		<h2 class="h1 appear">Почему стоит выбрать нас?</h2>

		<div class="page-subsection grid grid--align-items--flex-end">

			<div class="block-marked block-marked--to-border block-marked--to-border--left grid__cell grid__cell--l--6 grid__cell--xs--12 appear saturate saturate--lighter">
				<div class="page-subsection">
					<h3 class="appear saturate">Отдельные кабинеты для каждого клиента</h3>
					<p class="text-marked text-marked--padding--left font-size-14 appear">Вы остаетесь наедине с мастером, что позволяет <strong>расслабиться</strong> и, не отвлекаясь на других людей, заняться красотой.</p>
				</div>
				<div class="page-subsection">
					<h3 class="appear saturate">Сохраним волосы здоровыми и красивыми</h3>
					<ul class="text-marked text-marked--padding--left unordered-list font-size-14">
						<li class="unordered-list__item appear saturate">Мастера имеют <strong>знания и опыт</strong> в сфере</li>
						<li class="unordered-list__item appear saturate">Подберем <strong>индивидуальный подход</strong> к оказанию услуги</li>
						<li class="unordered-list__item appear saturate">Предложим ряд <strong>профессиональных</strong> уходовых процедур</li>
					</ul>
				</div>
			</div>

			<div class="grid__cell grid__cell--l--6 grid__cell--xs--12 appear">
				<figure class="figure">
					<picture>
						<source srcset="/images/home-why-we.webp, /images/home-why-we@2x.webp 2x" type="image/webp"/>
						<source srcset="/images/home-why-we.jpg, /images/home-why-we@2x.jpg 2x"/><img class="figure__image" src="/images/home-why-we.jpg" loading="lazy" decoding="async"/>
					</picture>
					<figcaption class="figure__caption">Салон красоты Hair Care Center - это одно из направлений группы компаний: Центр Дизайна Волос. Центр с 2002 года помогает людям с поредением или потерей волос выглядеть безупречно.</figcaption>
				</figure>
			</div>

		</div>

	</div>
</section>
