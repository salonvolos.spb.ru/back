<section class="container page-section page-section--home-brands">
	<div class="content">

		<h2 class="h1 appear">Работаем с брендами</h2>

		<div class="page-subsection grid grid--justify--center grid--columns--10">
			<div class="brand-item brand-item--estel grid__cell grid__cell--l--2 grid__cell--m--3 grid__cell--xs--5 appear saturate"></div>
			<?/*<div class="brand-item brand-item--loreal grid__cell grid__cell--l--2 grid__cell--m--3 grid__cell--xs--5 appear saturate"></div>
			<div class="brand-item brand-item--schwarzkopf grid__cell grid__cell--m--2 grid__cell--xs--5 appear saturate"></div>
			<div class="brand-item brand-item--cadiveu grid__cell grid__cell--m--2 grid__cell--xs--5 appear saturate"></div>*/?>
			<div class="brand-item brand-item--malecula grid__cell grid__cell--l--2 grid__cell--m--3 grid__cell--xs--5 appear saturate"></div>
			<div class="brand-item brand-item--matrix grid__cell grid__cell--l--2 grid__cell--m--3 grid__cell--xs--5 appear saturate"></div>
		</div>

	</div>
</section>
