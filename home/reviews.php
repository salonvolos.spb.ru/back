<section class="container page-section page-section--home-reviews">
	<div class="content">

		<h2 class="h1 appear">Отзывы</h2>

		<div class="review-item page-subsection appear">
			<div class="review-item__image">
				<picture>
					<source srcset="/images/home-review.webp, /images/home-review@2x.webp 2x" type="image/webp"/>
					<source srcset="/images/home-review.jpg, /images/home-review@2x.jpg 2x"/><img src="/images/home-review.jpg" loading="lazy" decoding="async" alt="Глафира Козулина, актриса театра и кино" title="Глафира Козулина, актриса театра и кино" width="277" height="277"/>
				</picture>
			</div>
			<h3 class="review-item__author">Глафира Козулина</h3>
			<div class="review-item__occupation">актриса театра и кино</div>
			<div class="review-item__body">
				<p>Сейчас очень сложно найти своего мастера, так как выбор большой, салонов много.</p>
				<p>Я рада, что я познакомилась с мастером Дашей из салона Hair Care Center. Теперь если у меня возникают вопросы по уходу, я всегда обращаюсь к ней. Не раз мы делали различные процедуры, эксперементировали с макияжем, я всегда была довольна. Если мне необходимо прийти красивой на какое либо мероприятие или встречу, я знаю, что могу быть уверена в Даше и довериться ее золотым ручкам!</p>
				<p>В салоне всегда приятный, улыбчивый персонал. Предложат кофе, чай, конфетку! Вообще политика салона очень грамотная!</p>
			</div>
		</div>

	</div>
</section>
