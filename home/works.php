<section class="container page-section page-section--home-works">
	<div class="content">

		<h2 class="h1 appear">Галерея работ</h2>

		<div class="page-subsection grid grid--align-items--flex-start">

			<a href="/images/works-1.webp" class="works-item grid__cell grid__cell--l--3 grid__cell--m--4 grid__cell--s--6 grid__cell--xs--6 appear" data-glightbox >
				<div class="works-item__title">Прическа и макияж</div>
				<div class="works-item__price">3 100 ₽</div>
				<picture>
					<source srcset="/images/works-1.webp" type="image/webp"/>
					<source srcset="/images/works-1.jpg"/><img class="works-item__image" src="/images/works-1.jpg" loading="lazy" decoding="async" alt="Прическа и макияж" title="Прическа и макияж"/>
				</picture>
			</a>

			<a href="/images/works-2.webp" class="works-item grid__cell grid__cell--l--3 grid__cell--m--4 grid__cell--s--6 grid__cell--xs--6 appear" data-glightbox>
				<div class="works-item__title">Стрижка и укладка</div>
				<div class="works-item__price">1 400 ₽</div>
				<picture>
					<source srcset="/images/works-2.webp" type="image/webp"/>
					<source srcset="/images/works-2.jpg"/><img class="works-item__image" src="/images/works-2.jpg" loading="lazy" decoding="async" alt="Стрижка и укладка" title="Стрижка и укладка"/>
				</picture>
			</a>

			<a href="/images/works-creative-style.webp" class="works-item grid__cell grid__cell--l--3 grid__cell--m--4 grid__cell--s--6 grid__cell--xs--6 appear" data-glightbox>
				<div class="works-item__title">Вечерний макияж</div>
				<div class="works-item__price">1 600 ₽</div>
				<picture>
					<source srcset="/images/works-creative-style.webp" type="image/webp"/>
					<source srcset="/images/works-creative-style.jpg"/><img class="works-item__image" src="/images/works-creative-style.jpg" loading="lazy" decoding="async" alt="Вечерний макияж" title="Вечерний макияж"/>
				</picture>
			</a>

			<a href="/images/works-evening-makeup.webp" class="works-item grid__cell grid__cell--l--3 grid__cell--m--4 grid__cell--s--6 grid__cell--xs--6 appear" data-glightbox>
				<div class="works-item__title">Наращивание волос</div>
				<div class="works-item__price">30 ₽</div>
				<picture>
					<source srcset="/images/works-evening-makeup.webp" type="image/webp"/>
					<source srcset="/images/works-evening-makeup.jpg"/><img class="works-item__image" src="/images/works-evening-makeup.jpg" loading="lazy" decoding="async" alt="Наращивание волос" title="Наращивание волос"/>
				</picture>
			</a>

			<a href="/images/works-extention.webp" class="works-item grid__cell grid__cell--l--3 grid__cell--m--4 grid__cell--s--6 grid__cell--xs--6 appear" data-glightbox>
				<div class="works-item__title">Свадебная прическа и макияж</div>
				<div class="works-item__price">6 100 ₽</div>
				<picture>
					<source srcset="/images/works-extention.webp" type="image/webp"/>
					<source srcset="/images/works-extention.jpg"/><img class="works-item__image" src="/images/works-extention.jpg" loading="lazy" decoding="async" alt="Свадебная прическа и макияж" title="Свадебная прическа и макияж"/>
				</picture>
			</a>

			<a href="/images/works-long-hair.webp" class="works-item grid__cell grid__cell--l--3 grid__cell--m--4 grid__cell--s--6 grid__cell--xs--6 appear" data-glightbox>
				<div class="works-item__title">Креативное окрашивания и макияж</div>
				<div class="works-item__price">7 500 ₽</div>
				<picture>
					<source srcset="/images/works-long-hair.webp" type="image/webp"/>
					<source srcset="/images/works-long-hair.jpg"/><img class="works-item__image" src="/images/works-long-hair.jpg" loading="lazy" decoding="async" alt="Креативное окрашивания и макияж" title="Креативное окрашивания и макияж"/>
				</picture>
			</a>

			<a href="/images/works-wedding-style.webp" class="works-item grid__cell grid__cell--l--3 grid__cell--m--4 grid__cell--s--6 grid__cell--xs--6 appear" data-glightbox>
				<div class="works-item__title">Стрижка и окрашивание</div>
				<div class="works-item__price">8 500 ₽</div>
				<picture>
					<source srcset="/images/works-wedding-style.webp" type="image/webp"/>
					<source srcset="/images/works-wedding-style.jpg"/><img class="works-item__image" src="/images/works-wedding-style.jpg" loading="lazy" decoding="async" alt="Стрижка и окрашивание" title="Стрижка и окрашивание"/>
				</picture>
			</a>

		</div>

	</div>
</section>
