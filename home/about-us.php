<section class="container page-section page-section--home-advantages">
	<div class="content">

		<h2 class="h1">О нас</h2>

		<div class="page-subsection grid grid--justify--center">

			<div class="advantage-block advantage-block--professionalism grid__cell grid__cell--l--4 grid__cell--m--4 grid__cell--s--6 grid__cell--xs--12 appear">
				<div class="advantage-block__title">Профессионализм</div>
				<div class="advantage-block__description">Мастера обучались в ведущих студиях и академиях: Coiffure, Локон, Londa, Schwarzkopf, Wella</div>
			</div>

			<div class="advantage-block advantage-block--cabinets grid__cell grid__cell--l--4 grid__cell--m--4 grid__cell--s--6 grid__cell--xs--12 appear">
				<div class="advantage-block__title">Отдельные кабинеты</div>
				<div class="advantage-block__description">Реализуем индивидуальный подход и позволяем вам по настоящему раскрыться перед мастером</div>
			</div>

			<div class="advantage-block advantage-block--celebrities grid__cell grid__cell--l--4 grid__cell--m--4 grid__cell--s--6 grid__cell--xs--12 appear">
				<div class="advantage-block__title">Знаменитости</div>
				<div class="advantage-block__description">Среди наших клиентов актрисы театра и кино, а также звезды реалити-шоу</div>
			</div>

		</div>

	</div>
</section>
