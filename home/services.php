<section class="container page-section page-section--home-services">
	<div class="content">
		<h2 class="h1 appear">Оказываем услуги</h2>
		<div class="home-services page-subsection">
			<div class="home-services__item home-services__item--cite appear">
				<blockquote class="home-services__quote">
					<p>«Миловидная внешность — это еще не все, но добавьте обаяния, и мир — у ваших ног»</p>
					<cite>Софи Лорен</cite>
				</blockquote>
			</div>
			<div class="home-service home-service--big home-service--women home-services__item home-services__item--women appear">
				<div class="home-service__title">Женская стрижка</div>
				<ul class="home-service__list">
					<li class="home-service__item"><a href="/uslugi/strizhka/goryachimi-nozhnicami/">Горячими ножницами</a></li>
					<li class="home-service__item"><a href="/uslugi/strizhka/kare/">Каре</a></li>
					<li class="home-service__item"><a href="/uslugi/strizhka/kaskad/">Каскад</a></li>
					<li class="home-service__item"><a href="/uslugi/strizhka/konchikov-volos/">Стрижка кончиков</a></li>
					<li class="home-service__item"><a href="/uslugi/strizhka/mallet/">Маллет</a></li>
					<li class="home-service__item"><a href="/uslugi/strizhka/piksi/">Пикси</a></li>
					<li class="home-service__item"><a href="/uslugi/strizhka/korotkaya/">На короткие волосы</a></li>
				</ul><a class="link-arrow home-service__more" href="/uslugi/strizhka/zhenskaya-strizhka/">Узнать подробнее</a>
				<picture>
					<source srcset="/images/home-service-women.webp" type="image/webp"/>
					<source srcset="/images/home-service-women.jpg"/><img class="home-service__image" src="/images/home-service-women.jpg" loading="lazy" decoding="async"/>
				</picture>
			</div>
			<div class="home-service home-service--straightening home-services__item home-services__item--straightening appear">
				<div class="home-service__title">Кератиновое выпрямление волос</div>
				<ul class="home-service__list">
					<li class="home-service__item"><a href="/uslugi/vypryamlenie-volos/brazilskoe/">Бразильское</a></li>
					<li class="home-service__item"><a href="/uslugi/vypryamlenie-volos/chelka/">Челки</a></li>
					<li class="home-service__item"><a href="/uslugi/vypryamlenie-volos/muzhskoye/">Мужское</a></li>
				</ul><a class="link-arrow home-service__more" href="/uslugi/vypryamlenie-volos/">Узнать подробнее</a>
				<picture>
					<source srcset="/images/home-service-straightening.webp" type="image/webp"/>
					<source srcset="/images/home-service-straightening.jpg"/><img class="home-service__image" src="/images/home-service-straightening.jpg" loading="lazy" decoding="async"/>
				</picture>
			</div>
			<div class="home-service home-service--restoration home-services__item home-services__item--restoration appear">
				<div class="home-service__title">Уход и лечение</div>
				<ul class="home-service__list">
					<li class="home-service__item"><a href="/uslugi/uhod/botoks-dlya-volos/">Ботокс</a></li>
					<li class="home-service__item"><a href="/uslugi/uhod/nanoplastika/">Нанопластика</a></li>
				</ul><a class="link-arrow home-service__more" href="/uslugi/uhod/">Узнать подробнее</a>
				<picture>
					<source srcset="/images/home-service-restoration.webp" type="image/webp"/>
					<source srcset="/images/home-service-restoration.jpg"/><img class="home-service__image" src="/images/home-service-restoration.jpg" loading="lazy" decoding="async"/>
				</picture>
			</div>
			<div class="home-service home-service--curling home-services__item home-services__item--curling appear">
				<div class="home-service__title">Завивка</div><a class="link-arrow home-service__more" href="/uslugi/zavivka-volos/">Узнать подробнее</a>
				<picture>
					<source srcset="/images/home-service-curling.webp" type="image/webp"/>
					<source srcset="/images/home-service-curling.jpg"/><img class="home-service__image" src="/images/home-service-curling.jpg" loading="lazy" decoding="async"/>
				</picture>
			</div>
			<div class="home-service home-service--extension home-services__item home-services__item--extension appear">
				<div class="home-service__title">Наращивание волос</div>
				<ul class="home-service__list">
					<li class="home-service__item"><a href="/uslugi/narashchivanie-volos/gollivudskoe/">Голливудское</a></li>
					<li class="home-service__item"><a href="/uslugi/narashchivanie-volos/kapsulnoe/">Капсульное</a></li>
					<li class="home-service__item"><a href="/uslugi/narashchivanie-volos/kapsuljatsija/">Капсуляция</a></li>
					<li class="home-service__item"><a href="/uslugi/narashchivanie-volos/korrekciya/">Коррекция</a></li>
					<li class="home-service__item"><a href="/uslugi/narashchivanie-volos/obuchenie/">Курсы по наращиванию</a></li>
					<li class="home-service__item"><a href="/uslugi/narashchivanie-volos/lentochnoe/">Ленточное</a></li>
					<li class="home-service__item"><a href="/uslugi/narashchivanie-volos/muzhskoe/">Мужское</a></li>
					<li class="home-service__item"><a href="/uslugi/narashchivanie-volos/na-korotkie/">На короткие волосы</a></li>
					<li class="home-service__item"><a href="/uslugi/narashchivanie-volos/snyatie/">Снятие</a></li>
					<li class="home-service__item"><a href="/uslugi/narashchivanie-volos/holodnoe/">Холодное</a></li>
					<li class="home-service__item"><a href="/uslugi/narashchivanie-volos/tsvetnoe/">Цветное</a></li>
				</ul><a class="link-arrow home-service__more" href="/uslugi/narashchivanie-volos/">Узнать подробнее</a>
				<picture>
					<source srcset="/images/home-service-extension.webp?v=1" type="image/webp"/>
					<source srcset="/images/home-service-extension.jpg?v=1"/><img class="home-service__image" src="/images/home-service-extension.jpg?v=1" loading="lazy" decoding="async"/>
				</picture>
			</div>
			<div class="home-service home-service--styling home-services__item home-services__item--styling appear">
				<div class="home-service__title">Укладка</div><a class="link-arrow home-service__more" href="/uslugi/ukladka/">Узнать подробнее</a>
				<picture>
					<source srcset="/images/home-service-styling.webp" type="image/webp"/>
					<source srcset="/images/home-service-styling.jpg"/><img class="home-service__image" src="/images/home-service-styling.jpg" loading="lazy" decoding="async"/>
				</picture>
			</div>
			<div class="home-service home-service--styles home-services__item home-services__item--styles appear">
				<div class="home-service__title">Прически и косы</div>
				<ul class="home-service__list">
					<li class="home-service__item"><a href="/uslugi/pricheska/afrokudri/">Афрокудри</a></li>
					<li class="home-service__item"><a href="/uslugi/pricheska/vechernyaya/">Вечерняя</a></li>
					<li class="home-service__item"><a href="/uslugi/pricheska/detskaya/">Детская</a></li>
					<li class="home-service__item"><a href="/uslugi/pricheska/pletenie-kos/">Плетение кос</a></li>
					<li class="home-service__item"><a href="/uslugi/pricheska/svadebnaja/">Свадебная</a></li>
				</ul><a class="link-arrow home-service__more" href="/uslugi/pricheska/">Узнать подробнее</a>
				<picture>
					<source srcset="/images/home-service-styles.webp" type="image/webp"/>
					<source srcset="/images/home-service-styles.jpg"/><img class="home-service__image" src="/images/home-service-styles.jpg" loading="lazy" decoding="async"/>
				</picture>
			</div>
			<div class="home-service home-service--coloring home-services__item home-services__item--coloring appear">
				<div class="home-service__title">Окрашивание</div>
				<ul class="home-service__list">
					<li class="home-service__item"><a href="/uslugi/okrashivanie-volos/ombre/">Омбре</a></li>
					<li class="home-service__item"><a href="/uslugi/okrashivanie-volos/blondirovanie/">Блондирование</a></li>
					<li class="home-service__item"><a href="/uslugi/okrashivanie-volos/brondirovanie/">Брондирование</a></li>
					<li class="home-service__item"><a href="/uslugi/okrashivanie-volos/dekapirovanie/">Декапирование</a></li>
					<li class="home-service__item"><a href="/uslugi/okrashivanie-volos/kaliforniyskoe/">Калифорнийское мелирование</a></li>
					<li class="home-service__item"><a href="/uslugi/okrashivanie-volos/kolorirovanie/">Колорирование</a></li>
					<li class="home-service__item"><a href="/uslugi/okrashivanie-volos/konturing/">Контуринг</a></li>
					<li class="home-service__item"><a href="/uslugi/okrashivanie-volos/melirovaniye/">Мелирование</a></li>
					<li class="home-service__item"><a href="/uslugi/okrashivanie-volos/muzhskoe-okrashivanie/">Мужское</a></li>
					<li class="home-service__item"><a href="/uslugi/okrashivanie-volos/airtouch/">Airtouch</a></li>
					<li class="home-service__item"><a href="/uslugi/okrashivanie-volos/balayazh/">Балаяж</a></li>
					<li class="home-service__item"><a href="/uslugi/okrashivanie-volos/v-odin-ton/">В один тон</a></li>
					<li class="home-service__item"><a href="/uslugi/okrashivanie-volos/skrytoe/">Скрытое</a></li>
					<li class="home-service__item"><a href="/uslugi/okrashivanie-volos/slozhnoe/">Сложное</a></li>
					<li class="home-service__item"><a href="/uslugi/okrashivanie-volos/tonirovanie/">Тонирование</a></li>
					<li class="home-service__item"><a href="/uslugi/okrashivanie-volos/shatush/">Шатуш</a></li>
					<li class="home-service__item"><a href="/uslugi/okrashivanie-volos/czvetnoe/">Яркое и цветное</a></li>
				</ul><a class="link-arrow home-service__more" href="/uslugi/okrashivanie-volos/">Узнать подробнее</a>
				<picture>
					<source srcset="/images/home-service-coloring.webp?v=1" type="image/webp"/>
					<source srcset="/images/home-service-coloring.jpg?v=1"/><img class="home-service__image" src="/images/home-service-coloring.jpg?v=1" loading="lazy" decoding="async"/>
				</picture>
			</div>
			<div class="home-service home-service--makeup home-services__item home-services__item--makeup appear">
				<div class="home-service__title">Макияж</div><a class="link-arrow home-service__more" href="/uslugi/makiyazh/">Узнать подробнее</a>
				<picture>
					<source srcset="/images/home-service-makeup.webp" type="image/webp"/>
					<source srcset="/images/home-service-makeup.jpg"/><img class="home-service__image" src="/images/home-service-makeup.jpg" loading="lazy" decoding="async"/>
				</picture>
			</div>
			<div class="home-service home-service--men home-services__item home-services__item--men appear">
				<div class="home-service__title">Мужская стрижка</div>
				<ul class="home-service__list">
					<li class="home-service__item"><a href="/uslugi/strizhka/modelnaya/">Модельная</a></li>
					<li class="home-service__item"><a href="/uslugi/strizhka/poluboks/">Полубокс</a></li>
					<li class="home-service__item"><a href="/uslugi/strizhka/crop/">Кроп</a></li>
				</ul><a class="link-arrow home-service__more" href="/uslugi/strizhka/muzhskaya-strizhka/">Узнать подробнее</a>
				<picture>
					<source srcset="/images/home-service-men.webp" type="image/webp"/>
					<source srcset="/images/home-service-men.jpg"/><img class="home-service__image" src="/images/home-service-men.jpg" loading="lazy" decoding="async"/>
				</picture>
			</div>
			<div class="home-service home-service--detskie home-services__item home-services__item--detskie appear">
				<div class="home-service__title">Детские стрижки</div><a class="link-arrow home-service__more" href="/uslugi/strizhka/detskie/">Узнать подробнее</a>
				<picture>
					<source srcset="/images/home-service-detskie.webp" type="image/webp"/>
					<source srcset="/images/home-service-detskie.jpg"/><img class="home-service__image" src="/images/home-service-detskie.jpg" loading="lazy" decoding="async"/>
				</picture>
			</div>
		</div>
	</div>
</section>
