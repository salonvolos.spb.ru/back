<section class="container page-section page-section--home-staff">
	<div class="content">

		<h2 class="h1 appear">Средний стаж мастеров салона — 12 лет</h2>

		<div class="page-subsection grid grid--justify--center">
			<div class="grid__cell grid__cell--l--9 grid__cell--m--10 grid__cell--xs--12 text-align-center">
				<p class="appear">Мастера обучались в ведущих студиях и академиях: <strong>Coiffure, Локон, Londa, Shwarzkopf, Wella</strong>.</p>
				<p class="appear">Сотрудники салона неоднократно участвовали в международном фестивале красоты <strong>«Невские берега»</strong>, крупнейшей в Европе парфюмерно-косметической выставке <strong>Intercharm</strong>.</p>
			</div>
		</div>

		<div class="page-subsection grid grid--columns--10 grid--align-items--flex-start grid--justify--center">

			<div class="staff-item grid__cell grid__cell--l--2 grid__cell--m--2 grid__cell--xs--10">
				<h3 class="saturate appear staff-item__title">Виктория Пристанскова</h3>
				<ul class="staff-item__list">
					<li class="saturate appear staff-item__list-item">Визажист</li>
					<li class="saturate appear staff-item__list-item">Колорист</li>
					<li class="saturate appear staff-item__list-item">Парикмахер-универсал</li>
					<li class="saturate appear staff-item__list-item">Топ мастер по наращиванию</li>
				</ul><a class="link-arrow staff-item__more" href="/mastera/viktoriya-pristanskova.html">Подробнее</a>
				<picture>
					<source srcset="/images/staff-item-pristanskova.webp, /images/staff-item-pristanskova@2x.webp 2x" type="image/webp"/>
					<source srcset="/images/staff-item-pristanskova.jpg, /images/staff-item-pristanskova@2x.jpg 2x"/><img class="saturate appear staff-item__image" src="/images/staff-item-pristanskova.jpg" loading="lazy" decoding="async" alt="Виктория Пристанскова" title="Виктория Пристанскова" width="214" height="214"/>
				</picture>
			</div>

			<div class="staff-item grid__cell grid__cell--l--2 grid__cell--m--2 grid__cell--xs--10">
				<h3 class="saturate appear staff-item__title">Дарья Иванова</h3>
				<ul class="staff-item__list">
					<li class="saturate appear staff-item__list-item">Визажист</li>
					<li class="saturate appear staff-item__list-item">Парикмахер-универсал</li>
					<li class="saturate appear staff-item__list-item">Эксперт мастер по наращиванию</li>
				</ul><a class="link-arrow staff-item__more" href="/mastera/darya-ivanova.html">Подробнее</a>
				<picture>
					<source srcset="/images/staff-item-ivanova.webp?v=1, /images/staff-item-ivanova@2x.webp?v=1 2x" type="image/webp"/>
					<source srcset="/images/staff-item-ivanova.jpg?v=1, /images/staff-item-ivanova@2x.jpg?v=1 2x"/><img class="saturate appear staff-item__image" src="/images/staff-item-ivanova.jpg?v=1" loading="lazy" decoding="async" alt="Дарья Иванова" title="Дарья Иванова" width="214" height="214"/>
				</picture>
			</div>

			<div class="staff-item grid__cell grid__cell--l--2 grid__cell--m--2 grid__cell--xs--10">
				<h3 class="saturate appear staff-item__title">Наталья Михайлова</h3>
				<ul class="staff-item__list">
					<li class="saturate appear staff-item__list-item">Парикмахер-универсал</li>
				</ul><a class="link-arrow staff-item__more" href="/mastera/natalya-mikhaylova.html">Подробнее</a>
				<picture>
					<source srcset="/images/staff-item-mihaylova.webp, /images/staff-item-mihaylova@2x.webp 2x" type="image/webp"/>
					<source srcset="/images/staff-item-mihaylova.jpg, /images/staff-item-mihaylova@2x.jpg 2x"/><img class="saturate appear staff-item__image" src="/images/staff-item-mihaylova.jpg" loading="lazy" decoding="async" alt="Наталья Михайлова" title="Наталья Михайлова" width="214" height="214"/>
				</picture>
			</div>

			<div class="staff-item grid__cell grid__cell--l--2 grid__cell--m--2 grid__cell--xs--10">
				<h3 class="saturate appear staff-item__title">Ирина Богданова</h3>
				<ul class="staff-item__list">
					<li class="saturate appear staff-item__list-item">Парикмахер-универсал</li>
				</ul><a class="link-arrow staff-item__more" href="/mastera/irina-bogdanova.html">Подробнее</a>
				<picture>
					<source srcset="/images/staff-item-bogdanova.webp, /images/staff-item-bogdanova@2x.webp 2x" type="image/webp"/>
					<source srcset="/images/staff-item-bogdanova.jpg, /images/staff-item-bogdanova@2x.jpg 2x"/><img class="saturate appear staff-item__image" src="/images/staff-item-bogdanova.jpg" loading="lazy" decoding="async" alt="Ирина Богданова" title="Ирина Богданова" width="214" height="214"/>
				</picture>
			</div>

			<div class="staff-item grid__cell grid__cell--l--2 grid__cell--m--2 grid__cell--xs--10">
				<h3 class="saturate appear staff-item__title">Екатерина Смирнова</h3>
				<ul class="staff-item__list">
					<li class="saturate appear staff-item__list-item">Парикмахер-универсал</li>
				</ul><a class="link-arrow staff-item__more" href="/mastera/ekaterina-smirnova.html">Подробнее</a>
				<picture>
					<source srcset="/images/staff-item-smirnova.webp, /images/staff-item-smirnova@2x.webp 2x" type="image/webp"/>
					<source srcset="/images/staff-item-smirnova.jpg, /images/staff-item-smirnova@2x.jpg 2x"/><img class="saturate appear staff-item__image" src="/images/staff-item-smirnova.jpg" loading="lazy" decoding="async" alt="Екатерина Смирнова" title="Екатерина Смирнова" width="214" height="214"/>
				</picture>
			</div>

			<div class="staff-item grid__cell grid__cell--l--2 grid__cell--m--2 grid__cell--xs--10">
				<h3 class="saturate appear staff-item__title">Наталья Шаныгина</h3>
				<ul class="staff-item__list">
					<li class="saturate appear staff-item__list-item">Парикмахер-универсал</li>
					<li class="saturate appear staff-item__list-item">Топ мастер по наращиванию</li>
				</ul><a class="link-arrow staff-item__more" href="/mastera/natalya-shanygina.html">Подробнее</a>
				<picture>
					<source srcset="/images/staff-item-shanygina.webp, /images/staff-item-shanygina@2x.webp 2x" type="image/webp"/>
					<source srcset="/images/staff-item-shanygina.jpg, /images/staff-item-shanygina@2x.jpg 2x"/><img class="saturate appear staff-item__image" src="/images/staff-item-shanygina.jpg" loading="lazy" decoding="async" alt="Наталья Шаныгина" title="Наталья Шаныгина" width="214" height="214"/>
				</picture>
			</div>

			<div class="staff-item grid__cell grid__cell--l--2 grid__cell--m--2 grid__cell--xs--10">
				<h3 class="saturate appear staff-item__title">Ольга Шадричева</h3>
				<ul class="staff-item__list">
					<li class="saturate appear staff-item__list-item">Колорист</li>
					<li class="saturate appear staff-item__list-item">Эксперт мастер по наращиванию</li>
				</ul><a class="link-arrow staff-item__more" href="/mastera/shadricheva-olga.html">Подробнее</a>
				<picture>
					<source srcset="/images/staff-item-shadricheva.webp, /images/staff-item-shadricheva@2x.webp 2x" type="image/webp"/>
					<source srcset="/images/staff-item-shadricheva.jpg, /images/staff-item-shadricheva@2x.jpg 2x"/><img class="saturate appear staff-item__image" src="/images/staff-item-shadricheva.jpg" loading="lazy" decoding="async" alt="Ольга Шадричева" title="Ольга Шадричева" width="214" height="214"/>
				</picture>
			</div>

			<? /* <div class="staff-item grid__cell grid__cell--l--2 grid__cell--m--2 grid__cell--xs--10">
				<h3 class="saturate appear staff-item__title">Елена Богданова</h3>
				<ul class="staff-item__list">
					<li class="saturate appear staff-item__list-item">Топ мастер по наращиванию</li>
				</ul><a class="link-arrow staff-item__more" href="/mastera/elena-bogdanova.html">Подробнее</a>
				<picture>
					<source srcset="/images/staff-item-e-bogdanova.webp, /images/staff-item-e-bogdanova@2x.webp 2x" type="image/webp"/>
					<source srcset="/images/staff-item-e-bogdanova.jpg, /images/staff-item-e-bogdanova@2x.jpg 2x"/><img class="saturate appear staff-item__image" src="/images/staff-item-e-bogdanova.jpg" loading="lazy" decoding="async" alt="Елена Богданова" title="Елена Богданова" width="214" height="214"/>
				</picture>
			</div> */?>
			<div class="staff-item grid__cell grid__cell--l--2 grid__cell--m--2 grid__cell--xs--10">
				<h3 class="saturate appear staff-item__title">Яна Даниленко</h3>
				<ul class="staff-item__list">
					<li class="saturate appear staff-item__list-item">Барбер</li>
					<li class="saturate appear staff-item__list-item">Парикмахер-универсал</li>
				</ul><a class="link-arrow staff-item__more" href="/mastera/yana-danilenko.html">Подробнее</a>
				<picture>
					<source srcset="/images/staff-item-danilenko.webp, /images/staff-item-danilenko@2x.webp 2x" type="image/webp"/>
					<source srcset="/images/staff-item-danilenko.jpg, /images/staff-item-danilenko@2x.jpg 2x"/><img class="saturate appear staff-item__image" src="/images/staff-item-danilenko.jpg" loading="lazy" decoding="async" alt="Яна Даниленко" title="Яна Даниленко" width="214" height="214"/>
				</picture>
			</div>

		</div>

	</div>
</section>
