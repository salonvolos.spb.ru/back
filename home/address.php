<section class="container page-section page-section--home-address">
	<div class="content">

		<h2 class="h1 appear">Наш адрес</h2>

		<div class="page-subsection grid grid--align-items--flex-end block-shifted">

			<div class="grid__cell grid__cell--l--6 grid__cell--xs--12 appear block-shifted__image">
				<figure class="figure">
					<picture>
						<source srcset="/images/home-office.webp, /images/home-office@2x.webp 2x" type="image/webp"/>
						<source srcset="/images/home-office.jpg, /images/home-office@2x.jpg 2x"/><img class="figure__image" src="/images/home-office.jpg" loading="lazy" decoding="async"/>
					</picture>
				</figure>
			</div>

			<div class="block-marked block-marked--to-border block-marked--to-border--right grid__cell grid__cell--l--6 grid__cell--xs--12 appear saturate saturate--lighter block-shifted__content">
				<div class="page-subsection">
					<h3 class="appear saturate">Санкт-Петербург, наб. Обводного канала 108</h3>
					<p class="text-marked text-marked--padding--left font-size-14 appear">пн-пт: с 10:00 до 21:00
					</p>
					<p class="text-marked text-marked--padding--left font-size-14 appear">сб-вс: с 10:00 до 19:00
					</p>
				</div>
				<div class="page-subsection">
					<h3 class="appear saturate">Припарковаться на машине</h3>
					<p class="text-marked text-marked--padding--left font-size-14 appear">Перед входом парковочные места. Обычно 2-3 места свободные. Если все занято, то правее от здания есть площадка для машин.
					</p>
				</div>
				<div class="page-subsection">
					<h3 class="appear saturate">Добраться пешком</h3>
					<p class="text-marked text-marked--padding--left font-size-14 appear">
                      <span class="icon-inline icon-inline--metro saturate">Балтийская – 10 мин
                      </span>
					</p>
					<p class="text-marked text-marked--padding--left font-size-14 appear">
                      <span class="icon-inline icon-inline--metro saturate">Фрунзенская – 5 мин
                      </span>
					</p>
				</div>
			</div>

		</div>

	</div>
</section>
