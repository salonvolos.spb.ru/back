<section class="container page-section home-appointment">
	<div class="content grid grid--justify--flex-end grid--align-items--center home-appointment__inner">
		<div class="grid__cell grid__cell--l--6 grid__cell--m--6 grid__cell--xs--12 grid grid--justify--center home-appointment__info">

			<div class="grid__cell grid__cell--l--8 grid__cell--xs--12">

				<h2 class="h1 home-appointment__title appear saturate">Запишитесь сейчас</h2>

				<p class="home-appointment__description appear saturate">online на удобное время</p>

				<a
                    class="button home-appointment__button appear saturate ms_booking"
                    data-href="https://n371027.yclients.com/company:170918"
                >Выбрать время и специалиста</a>

			</div>

		</div>
	</div>
</section>
