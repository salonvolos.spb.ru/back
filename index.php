<? require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');
$APPLICATION->SetPageProperty("title", "Салон красоты у м. Фрунзенская и Балтийская, парикмахерские услуги");
$APPLICATION->SetPageProperty("description", "Cалон красоты у м. Фрунзенская и Балтийская, парикмахерские услуги");
$APPLICATION->SetTitle('Главная');
?>

<? /* include __DIR__."/home/slider.php" */ ?>
<div class="container banner-march">
	<div class="content banner-march__inner">
		<p class="banner-march__duration">с 5 по 20 марта</p>
		<div class="banner-march__clarification"><strong>Дарим скидку <span>8%</span></strong> на&nbsp;коррекцию наращивания и&nbsp;до&nbsp;<span>12%</span> на&nbsp;покупку волос</div>
		<div class="banner-march__clarification">Не&nbsp;откладывай свое весеннее преображение!</div><span class="banner-march__promocode">по промокоду</span>
	</div>
</div>

<? $APPLICATION->IncludeComponent("bitrix:main.include", "", Array(
	"AREA_FILE_SHOW" => "file",
	"PATH" => "/home/about-us.php",
)) ?>

<? $APPLICATION->IncludeComponent("bitrix:main.include", "", Array(
	"AREA_FILE_SHOW" => "file",
	"PATH" => "/home/services.php",
)) ?>

<? include __DIR__."/home/brands.php" ?>

<? $APPLICATION->IncludeComponent("bitrix:main.include", "", Array(
	"AREA_FILE_SHOW" => "file",
	"PATH" => "/home/staff.php",
)) ?>

<? $APPLICATION->IncludeComponent("bitrix:main.include", "", Array(
	"AREA_FILE_SHOW" => "file",
	"PATH" => "/home/why-we.php",
)) ?>

<? $APPLICATION->IncludeComponent("bitrix:main.include", "", Array(
	"AREA_FILE_SHOW" => "file",
	"PATH" => "/home/reviews.php",
)) ?>

<? include __DIR__."/home/yandex-reviews.php" ?>

<? $APPLICATION->IncludeComponent("bitrix:main.include", "", Array(
	"AREA_FILE_SHOW" => "file",
	"PATH" => "/home/works.php",
)) ?>

<? include __DIR__."/home/appointment.php" ?>

<? $APPLICATION->IncludeComponent("bitrix:main.include", "", Array(
	"AREA_FILE_SHOW" => "file",
	"PATH" => "/home/address.php",
)) ?>

<? include __DIR__."/home/map.php" ?>

<?
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');
