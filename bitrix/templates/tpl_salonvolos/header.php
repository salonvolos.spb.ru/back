<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {die();}
global $USER;
?><!DOCTYPE html>
<html lang="ru">

    <head>

        <? $APPLICATION->ShowHead() ?>

        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title><? $APPLICATION->ShowTitle() ?></title>

        <? blockInclude('header', 'header-styles') ?>


        <? blockInclude('header', 'header-icons') ?>


        <? $numbPage = "all" ?>
        <? if($APPLICATION->GetCurPage() == '/'): ?>
            <? $numbPage = "first" ?>
        <? endif ?>

        <? $APPLICATION->ShowPanel() ?>

    </head>

    <body>

        <? if (!$USER->IsAuthorized()): ?>
            <? blockInclude('header', 'header-scripts') ?>
        <? endif ?>

        <? if (!$USER->IsAuthorized()): ?>
            <? blockInclude('body', 'body-top-scripts') ?>
        <? endif ?>

		<? /* <script src="http://www.vse-v-salon.ru/online-record/api/api.js" type="text/javascript"></script> */ ?>
        <? /* <script type="text/javascript">vvs_api_init('248', '#5BA04B');</script> */ ?>

        <header class="header" id="header">

            <? blockInclude('header', 'header-menu-outer-sites') ?>

            <div class="container">
                <div class="content">

                    <div class="header__top">
                        <? blockInclude('header', 'header-tops-mobile-switcher') ?>
                        <? blockInclude('header', 'header-tops-logo') ?>
                        <? blockInclude('header', 'header-tops-info-desctop') ?>
                        <? blockInclude('header', 'header-tops-info-mobile') ?>
                        <? blockInclude('header', 'header-tops-contacts') ?>
                    </div>

                    <div class="header__bottom">
                        <? blockInclude('header', 'header-menu-main') ?>
                    </div>

                </div>
            </div>

        </header>

        <main>

            <? if(isShowBreadcrumbs()): ?>
                <div class="inner-page container">
                    <div class="content">
                        <? blockInclude('header', 'header-breadcrumbs') ?>
                    </div>
                </div>
            <? endif ?>
