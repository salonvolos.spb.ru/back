<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
    </main>

    <footer class="footer">

        <div class="container section_grey">
            <div class="content">
                <div class="footer-top">
                    <div class="block-wrap  block-wrap_nopadding block-wrap_wrap ">
                        <? blockInclude('footer', 'footer-menu-1') ?>
                        <? blockInclude('footer', 'footer-menu-2') ?>
                        <? blockInclude('footer', 'footer-menu-3') ?>
                        <? blockInclude('footer', 'footer-contacts') ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="content">
                <div class="footer-bottom">
                    <div class="block-wrap  block-wrap_wrap ">
                        <? blockInclude('footer', 'footer-bottom-copyright') ?>
                        <? blockInclude('footer', 'footer-bottom-author') ?>
                    </div>
                </div>
            </div>
        </div>

    </footer>

    <? blockInclude('body', 'body-bottom-scripts') ?>
    <? blockInclude('footer', 'footer-scripts') ?>

  </body>

</html>
