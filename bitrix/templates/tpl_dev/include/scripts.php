<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$scripts = [
	'jquery-3.2.1.min',
	'jquery.maskedinput',
	'jquery.activity.min',
	'form_validation',
	'ajax_form',
	'calc_cost_hair_extension',
	'additional',
    'custom'
];

if(isHomePage()){
	$scripts[] = "lazy_loading";
}

foreach($scripts as $script):
    $src = SITE_TEMPLATE_PATH."/scripts/".$script.".js?v=".require "version.php";
    ?>
    <script data-skip-moving="true" src="<?= $src ?>"></script>
<? endforeach ?>

<? if ($APPLICATION->GetCurPage() === '/kontakty/'): ?>
  <script src="https://unpkg.com/video.js/dist/video.min.js"></script>
	<script src="https://unpkg.com/videojs-vr/dist/videojs-vr.min.js"></script>
<? endif ?>

<script src="/local/front/template/scripts/script.js?v=<?= require "version.php" ?>"></script>

<script type="text/javascript" src="https://w371027.yclients.com/widgetJS" charset="UTF-8"></script>
