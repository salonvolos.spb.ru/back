<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$styles = [];

if(isHomePage()){
	$styles[] = "home";
}else{
	$styles[] = "inner";
}

$styles[] = "rest";
?>

<? if ($APPLICATION->GetCurPage() === '/kontakty/'): ?>
  <link href="https://unpkg.com/video.js/dist/video-js.min.css" rel="stylesheet">
<? endif ?>

<? foreach($styles as $style): ?>
	<link href="/local/front/template/styles/<?= $style ?>.css?v=<?= require "version.php" ?>" rel="stylesheet">
<? endforeach ?>
<link href="/bitrix/templates/tpl_dev/styles/add.css?v=<?= require "version.php" ?>" rel="stylesheet">
