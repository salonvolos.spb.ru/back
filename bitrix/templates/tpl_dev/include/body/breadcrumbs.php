<section class="container page-section page-section--no-padding--top page-section--no-padding--bottom">
	<div class="content">
        <? $APPLICATION->IncludeComponent("bitrix:breadcrumb", "breadcrumbs_reviews", Array(
	"START_FROM" => "0",	// Номер пункта, начиная с которого будет построена навигационная цепочка
		"PATH" => "",	// Путь, для которого будет построена навигационная цепочка (по умолчанию, текущий путь)
		"SITE_ID" => "-",	// Cайт (устанавливается в случае многосайтовой версии, когда DOCUMENT_ROOT у сайтов разный)
	),
	false,
	array(
	"ACTIVE_COMPONENT" => "Y"
	)
);?>
	</div>
</section>
