<nav class="header-sites container header__sites">
    <div class="content">
        <ul class="header-sites__list">
        <li class="header-sites__item header-sites__item--hairdesign"><a href="http://www.hairdesign.ru"><span class="hidden-xs hidden-s hidden-m">Центр<br>замещения волос</span><span class="hidden-l hidden-xl hidden-hd">Системы <br> волос </span></a></li>
        <li class="header-sites__item header-sites__item--salonvolos header-sites__item--active hidden-xs hidden-s hidden-m">
            <div class="a-repl"><span>Салон красоты</span></div>
        </li>
        <li class="header-sites__item header-sites__item--vsepariki"><a href="http://www.vsepariki.com"><span class="hidden-xs hidden-s hidden-m">Парики и<br>головные уборы</span><span class="hidden-l hidden-xl hidden-hd">Парики <br> и&nbsp;накладки</span></a></li>
        <li class="header-sites__item header-sites__item--triholog"><a href="http://www.triholog.org/"><span class="hidden-xs hidden-s hidden-m">Лечебная косметика<br>для волос</span><span class="hidden-l hidden-xl hidden-hd">Лечебная <br> косметика </span></a></li>
        <li class="header-sites__item header-sites__item--zagustitel"><a href="https://zagustitelvolos.com/"><span class="hidden-xs hidden-s hidden-m">Загустители волос</span><span class="hidden-l hidden-xl hidden-hd">Загустители <br> для волос</span></a></li>
        </ul>
    </div>
</nav>
