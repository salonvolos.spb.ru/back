<div class="header__contacts hidden-m hidden-s hidden-xs">

	<a
		class="phone phone--icon header__phone"
		href="tel:+78123850151"
	>+7 (812) 385-01-51</a>

	<a
		class="header__call-back"
		href="#" onclick="Comagic.openSitePhonePanel()"
	>Заказать обратный звонок</a>

</div>
