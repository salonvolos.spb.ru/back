<?
global $USER;
if (!$USER->IsAuthorized()): ?>
	<? include __DIR__."/scripts.php" ?>
<? endif ?>

<header class="header">
	<? include __DIR__."/header-sites.php" ?>
	<div class="container">
		<div class="content header__inner">
			<? include __DIR__."/logo.php" ?>
			<? include __DIR__."/address.php" ?>
			<? include __DIR__."/phones.php" ?>
			<? include __DIR__."/mob-buttons.php" ?>
			<? include __DIR__."/appointment-link.php" ?>
			<? include __DIR__."/menu.php" ?>
			<? include __DIR__."/search.php" ?>
		</div>
	</div>
	<div class="container banner-schedule"><a class="banner-schedule__title content" href="#new-year" data-modal>График работы в праздники</a>
		<div class="modal" id="new-year">
			<div class="modal__inner">
				<p>
					<br> 7.03 с&nbsp;10:00 до&nbsp;19:00
					<br> 8.03&nbsp;&mdash; нерабочий день
				</p>
				<button class="modal__close" type="button"></button>
			</div>
		</div>
	</div>
	<? /* <div class="container header__banner text-align-center">
		<div class="content">
			<p><strong>8&nbsp;марта&nbsp;&mdash; выходной. Поздравляем с&nbsp;праздником!</strong></p>
		</div>
	</div>  
	<div class="container banner-schedule"><a class="banner-schedule__title content" href="#new-year" data-modal>График работы в новогодние праздники</a>
		<div class="modal" id="new-year">
			<div class="modal__inner">
				<p>
					30&nbsp;декабря&nbsp;&mdash; 10:00&nbsp;&mdash; 19:00 <br> с&nbsp;31&nbsp;декабря по&nbsp;3&nbsp;января&nbsp;&mdash; выходной 
					<br> с&nbsp;4&nbsp;января по&nbsp;6&nbsp;января&nbsp;&mdash; 10:00&nbsp;&mdash; 19:00
					<br> 7&nbsp;января&nbsp;&mdash; выходной
					<br> 8&nbsp;января&nbsp;&mdash; 10:00&nbsp;&mdash; 19:00
					<br> 10&nbsp;января&nbsp;&mdash; 10:00&nbsp;&mdash; 15:00
				</p>
				<button class="modal__close" type="button"></button>
			</div>
		</div>
	</div> */?>
</header>
