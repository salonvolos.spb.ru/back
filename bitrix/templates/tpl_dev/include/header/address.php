<div class="header__address">

    <span class="icon-inline icon-inline--clock header__address-item hidden-m hidden-s hidden-xs">пн-пт: с 10:00 до 21:00</span>

    <span class="icon-inline icon-inline--clock header__address-item hidden-m hidden-s hidden-xs">сб-вс: с 10:00 до 19:00</span>

    <span class="icon-inline icon-inline--pin header__address-item hidden-m hidden-s hidden-xs">СПб, наб. Обводного канала 108</span>

    <span class="icon-inline icon-inline--metro header__address-item hidden-m hidden-s hidden-xs">Балтийская / Фрунзенская</span>

    <a
        class="icon-inline icon-inline--pin header__address-item hidden-hd hidden-xl hidden-l"
        href="/kontakty/"
    >СПб, наб. Обводного канала 108</a>

</div>
