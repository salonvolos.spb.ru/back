<div class="footer__item">
	<div class="footer__name">© Салон красоты HairCare Center 2006 - 2023</div>
	<div class="footer__agreement">
		<a href="/soglasheniya-na-obrabotku-personalnykh-dannykh/">Соглашения на обработку персональных данных</a>
		<a href="#">Правила использования сайта</a>
	</div>
</div>
