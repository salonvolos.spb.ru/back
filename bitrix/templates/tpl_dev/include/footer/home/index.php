<footer class="container page-section footer">
	<div class="content footer__inner">
        <? include __DIR__."/copyright.php" ?>
        <? include __DIR__."/developer.php" ?>
        <? include __DIR__."/social.php" ?>
	</div>
</footer>
