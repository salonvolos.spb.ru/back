<div class="footer__item footer__developer">
	<a
		class="footer__developer-link"
		href="https://medreclama.ru/"
		target="_blank"
	>Реклама косметологии</a>
	<div class="footer__developer-name">Агентство «Medreclama»</div>
</div>
