<nav class="footer-menu grid__cell grid__cell--l--3 grid__cell--m--4 grid__cell--xs--6">
	<ul class="footer-menu__list footer-menu__list--sub footer-menu__list--sub-6">

		<li class="footer-menu__item footer-menu__item--sub footer-menu__item--sub-6">
			<a href="/uslugi/zavivka-volos/">Завивка для волос</a>
		</li>

		<li class="footer-menu__item footer-menu__item--sub footer-menu__item--sub-6">
			<a href="/uslugi/vypryamlenie-volos/">Выпрямление волос</a>
		</li>

		<li class="footer-menu__item footer-menu__item--sub footer-menu__item--sub-6">
			<a href="/uslugi/okrashivanie-volos/">Окрашивание волос</a>
		</li>

	</ul>
</nav>
