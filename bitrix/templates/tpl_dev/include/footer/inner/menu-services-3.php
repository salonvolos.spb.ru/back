<nav class="footer-menu grid__cell grid__cell--l--3 grid__cell--m--4 grid__cell--xs--6">
	<ul class="footer-menu__list footer-menu__list--sub footer-menu__list--sub-6">

		<li class="footer-menu__item footer-menu__item--sub footer-menu__item--sub-6">
			<a href="/uslugi/narashchivanie-volos/">Наращивание волос</a>
		</li>

		<li class="footer-menu__item footer-menu__item--sub footer-menu__item--sub-6">
			<a href="/uslugi/uhod/">Уход и лечение</a>
		</li>

		<li class="footer-menu__item footer-menu__item--sub footer-menu__item--sub-6">
			<a href="/uslugi/makiyazh/">Макияж</a>
		</li>

	</ul>
</nav>
