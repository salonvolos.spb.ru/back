<nav class="footer-menu grid__cell grid__cell--l--2 grid__cell--m--3 grid__cell--xs--6">
	<ul class="footer-menu__list footer-menu__list--sub footer-menu__list--sub-6">

		<li class="footer-menu__item footer-menu__item--sub footer-menu__item--sub-6">
			<a href="/uslugi/strizhka/">Стрижка</a>
		</li>

		<li class="footer-menu__item footer-menu__item--sub footer-menu__item--sub-6">
			<a href="/uslugi/pricheska/">Прическа</a>
		</li>

		<li class="footer-menu__item footer-menu__item--sub footer-menu__item--sub-6">
			<a href="/uslugi/ukladka/">Укладка</a>
		</li>

	</ul>
</nav>
