<div class="grid__cell grid__cell--l--4 grid__cell--xs--12 footer__contacts">

	<a
		class="phone phone--big"
		href="tel:+78123850151"
	>+7 (812) 385-01-51</a>

	<div class="footer__links">Напишите нам:
		<div class="link-icons link-icons--inline">
			<a class="link-icons__link link-icons__link--whatsapp" href="whatsapp://send?phone=79817245628&text=" target="_blank"></a>
			<a class="link-icons__link link-icons__link--telegram" href="tg://resolve?domain=HairCareCenter" target="_blank"></a>
			<a class="link-icons__link link-icons__link--viber" href="viber://chat?number=79817245628" target="_blank"></a>
			<a class="link-icons__link link-icons__link--vk" href="https://vk.com/club2142527" target="_blank"></a>
		</div>
	</div>

</div>
