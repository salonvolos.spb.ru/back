<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {die();} ?>

            <? if(isShowTitle()): ?>
                </div>
            </section>
            <? endif ?>


<?php
if ($APPLICATION->GetCurPage(false) === '/otzyvy/' || $APPLICATION->GetCurPage(false) === '/gallery/' ){
    $APPLICATION->IncludeComponent(
        "bitrix:main.include",
        "",
        Array(
            "AREA_FILE_SHOW" => "file",
            "AREA_FILE_SUFFIX" => "inc",
            "EDIT_TEMPLATE" => "",
            "PATH" => "/include/sign_up_now_inc.php"
        )
    );
}

?>
        </main>

        <? //if(isHomePage()): ?>
            <? include __DIR__."/include/footer/home/index.php" ?>
        <? //else: ?>
            <? //include __DIR__."/include/footer/inner/index.php" ?>
        <? //endif ?>

    </body>

    <? include __DIR__."/include/styles.php" ?>
    <? include __DIR__."/include/scripts.php" ?>

</html>
