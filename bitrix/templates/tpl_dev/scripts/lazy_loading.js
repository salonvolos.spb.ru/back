window.addEventListener("scroll", userEventsInit);
window.addEventListener("mousemove", userEventsInit);

function userEventsInit()
{
    window.removeEventListener("scroll", userEventsInit);
    window.removeEventListener("mousemove", userEventsInit);

    createGoogleTourIframe();
    createYandexMapReviewsIframe();
    createYandexMapScript();
}

function createGoogleTourIframe()
{
    var iframeWrapper = document.getElementById("iframe-js-tour");
    if(iframeWrapper){
        var iframe = document.createElement("iframe");
        iframe.style = "border:0";
        iframe.allowfullscreen = "";
        iframe.src = "https://www.google.com/maps/embed?pb=!1m0!3m2!1sru!2s!4v1486634358815!6m8!1m7!1sK35p8NtUkMUAAAQvxa493A!2m2!1d59.90796101212884!2d30.31258920335586!3f221.86485814753792!4f-14.060279966658854!5f0.7820865974627469";
        iframeWrapper.appendChild(iframe);
    }
}

function createYandexMapReviewsIframe()
{
    var iframeWrapper = document.getElementById("iframe-yandex-maps-reviews");
    if(iframeWrapper){
        var iframe = document.createElement("iframe");
        iframe.style = "width:100%;height:100%;border:1px solid #e6e6e6;border-radius:8px;box-sizing:border-box";
        iframe.src = "https://yandex.ru/maps-reviews-widget/1770719437?comments";
        iframeWrapper.appendChild(iframe);
    }
}

function createYandexMapScript()
{
    var scriptWrapper = document.getElementById("script-yandex-maps");
    if(scriptWrapper){
        var script = document.createElement("script");
        script.type = "text/javascript";
        script.charset = "utf-8";
        script.src = "https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=0jFSeud2k6YiSEPpq1L9FIjWP8F6PbZr&amp;lang=ru_RU&amp;sourceType=constructor";
        scriptWrapper.appendChild(script);
    }
}