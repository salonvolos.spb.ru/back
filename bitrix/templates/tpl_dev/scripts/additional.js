$.maskfn.definitions['~']='[+-]';
$('body').delegate('input[type="tel"], input.phone', 'focus', function(){
    $(this).maskfn('+7 (999) 999-99-99');
});

$('form.consultation-form-submitJS').on('submitJS', function(event){
    ajaxFormSubmit({
        event:    event,
        handler:  'addFormDataConsultation',
        redirect: '/kontakty/kons/answer_ask-question.html',
        successAction: false
    });
});

$('form.hair-extention-sign-form').on('submitJS', function(event){
    ajaxFormSubmit({
        event: event,
        handler: 'addFormDataHairExtentionSignForm',
        successAction: '$(thisForm).detach(); $(".hair-extention-sign-form__success").show()',
    });
});

$('form.zapisatsya-v-list-ozhidaniya').on('submitJS', function(event){
    ajaxFormSubmit({
        event: event,
        handler: 'addFormDataZapisatsyaVListOzhidaniya',
        popupTitle: "Спасибо за вашу заявку",
        popupText: "Перезвоним вам, как только получим разрешение принимать клиентов и согласуем дату визита"
    });
});

$('form.multi-step-calculator-form').on('submitJS', function(event){
    ajaxFormSubmit({
        event: event,
        handler: 'calculatorForm',
        successAction: '$(thisForm).detach(); $(".multi-step-calculator-form-title").detach(); $(".multi-step-calculator-form-success").show()',
    });
});

$(document).ready(function() {
    $("body").activity({
        "achieveTime": 60,
        "testPeriod": 10,
        useMultiMode: 1,
        callBack: function (e) {
            ym(38802285, "reachGoal", "60seconds");
            console.log("60 sec activity goal achieved!");
        }
    });
});
