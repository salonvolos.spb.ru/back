document.addEventListener('DOMContentLoaded', () => {
    let elTitle = document.querySelectorAll('.works-item__title')
    let elPrice = document.querySelectorAll('.works-item__price')
    let names = [];
    let prices = [];
    let ids = [];
    let arrAttr = [];
    let objects = [];
    let status = false;

    window.addEventListener('scroll', () => {
        if (window.scrollY != 0 && status === false) {

            fetch('/include/work_our_inc.php', {
                method: 'POST', headers: {
                    'Content-Type': 'application/json'
                }, body: JSON.stringify({
                    status: 'ok'
                })
            })

                .then(response => response.json())
                .then(data => {

                    for (let key in data) {
                        // этот код будет вызван для каждого свойства объекта
                        // ..и выведет имя свойства и его значение
                        arrAttr.push(data[key])
                        let parts = data[key].split('/');
                        names.push(parts[0]);
                        prices.push(parts[1]);
                        ids.push(parts[2]);


                    }

                    for (var i = 0; i < ids.length; i++) {
                        var obj = {
                            id: ids[i],
                            price: prices[i],
                            name: names[i]
                        };
                        objects.push(obj);
                    }

                    elTitle.forEach((item) => {
                        for (let key in objects) {
                            if (objects[key].id == item.getAttribute('data-id')) {
                                item.innerHTML = objects[key].name

                            }
                        }
                    })
                    elPrice.forEach(item =>{
                        for (let key in objects) {
                            if (objects[key].id == item.getAttribute('data-id') && objects[key].price != '') {

                                item.innerHTML = objects[key].price

                            }
                        }
                    })


                })


            status = true;

        }
    })


})
