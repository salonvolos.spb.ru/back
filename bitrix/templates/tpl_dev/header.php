<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {die();}
?><!DOCTYPE html>
<html lang="ru">
<head>
	<? include __DIR__."/include/head.php" ?>
</head>
<body class="page">

    <div id="panel"><? $APPLICATION->ShowPanel() ?></div>

	<? include __DIR__."/include/header/index.php" ?>

    <main>

        <? if(isShowBreadcrumbs()): ?>
			<? include __DIR__."/include/body/breadcrumbs.php" ?>
        <? endif ?>

        <? if(isShowTitle()): ?>
            <section class="container page-section">
                <div class="content">
                    <h1><? $APPLICATION->ShowTitle("h1", false) ?></h1>
        <? endif ?>
