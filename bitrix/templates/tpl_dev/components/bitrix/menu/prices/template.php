<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {die();}
?>
<div class="page-subsection prices-navigation">
    <? foreach($arResult as $arItem):

        if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1){
            continue;
        }

        $class = "button button--secondary prices-navigation__item";
        if($arItem["SELECTED"]){
			$class .= " prices-navigation__item--active";
        }
    ?>
        <a
            class="<?= $class ?>"
            href="<?= $arItem["LINK"] ?>"
        ><?= $arItem["TEXT"] ?></a>

    <? endforeach ?>
</div>
