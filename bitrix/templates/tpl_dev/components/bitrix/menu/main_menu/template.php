<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<nav class="header-navigation header__navigation">

    <ul class="header-navigation__list">
		<? $previousLevel = 0 ?>
		<? foreach($arResult as $arItem): ?>
            <? $curLevel = $arItem["DEPTH_LEVEL"] ?>

            <? if ($previousLevel && $curLevel < $previousLevel): ?>
                <?= str_repeat("</ul> </li>", ($previousLevel - $curLevel)) ?>
            <? endif ?>

            <?
            $classes = ["header-navigation__item"];

            if(!empty($arItem["PARAMS"]["add_class"])){
                $classes[] = $arItem["PARAMS"]["add_class"];
            }

            if($arItem["IS_PARENT"]){
                $classes[] = "header-navigation__item--parent";
            }

            if($arItem["SELECTED"]){
                $classes[] = "header-navigation__item--active";
            }

            if($curLevel > 1){
                $classes[] = "header-navigation__item--sub header-navigation__item--sub-".($curLevel - 1);
            }

            $classes = implode(" ", $classes);
            ?>
            <li class="<?= $classes ?>">

                <? if(!empty($arItem['PARAMS']['not_clickable'])): ?>
                    <span><?= $arItem["TEXT"] ?></span>
                <? else: ?>
                    <a href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
                <? endif ?>

                <?  if($arItem["IS_PARENT"]): ?>
                    <ul class="header-navigation__list header-navigation__list--sub header-navigation__list--sub-<?= $curLevel ?>">
                <?  else: ?>
                    </li>
                <?  endif ?>

                <? $previousLevel = $curLevel ?>

        <? endforeach ?>

        <? if ($previousLevel > 1)://close last item tags?>
            <?= str_repeat("</ul> </li>", ($previousLevel-1) ) ?>
        <? endif ?>

    </ul>
</nav>
