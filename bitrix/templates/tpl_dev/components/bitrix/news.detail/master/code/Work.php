<?
namespace Masters;

class Work
{
	private $id;
	public $title;
	public $price;
	public $image;

	public function __construct($resource)
	{
		$this->id = (int)$resource->fields["ID"];
		$this->title = $resource->fields["NAME"];
		$this->price = $resource->props["PRICE"]["VALUE"];
		$this->image = getImageSrc($resource->props["PHOTO"]["VALUE"][0]);
	}
}