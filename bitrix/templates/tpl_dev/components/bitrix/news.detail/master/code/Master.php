<? namespace Masters;

class Master
{
	private $id;
	public $name;
	public $picture;
	public $appointmentUrl;
	public $skills;
	public $experienceTotal;
	public $experienceList;
	public $education;
	public $description;
	public $certificates;
	public $certificatesMax;
	public $works;
	public $feedbacks;

	public function __construct($arResult)
	{
		$props = $arResult["DISPLAY_PROPERTIES"];

		$this->id = $arResult["ID"];
		$this->name = $arResult["NAME"];
		$this->picture = !empty($arResult["PREVIEW_PICTURE"]) ? $arResult["PREVIEW_PICTURE"]["SRC"] : null;
		$this->appointmentUrl = $this->getAppointmentUrl($props);
		$this->skills = !empty($props["SKILLS"]) ? $props["SKILLS"]["VALUE"] : null;
		$this->experienceTotal = $this->getExperienceTotal($props);
		$this->experienceList = !empty($props["EXPERIENCE"]) ? $props["EXPERIENCE"]["VALUE"] : null;
		$this->education = !empty($props["EDUCATION"]) ? $props["EDUCATION"]["VALUE"] : null;
		$this->description = !empty($arResult["DETAIL_TEXT"]) ? $arResult["~DETAIL_TEXT"] : null;
		$this->certificates = $this->getCertificates($props);
		$this->certificatesMax = 6;
		$this->works = $this->getWorks($this->id);
		$this->feedbacks = $this->getFeedbacks($this->id);
	}

	private function getAppointmentUrl($props)
	{
		if(empty($props["YCLIENTS_CODE"])){
			return null;
		}

		return "https://n371027.yclients.com/company:170918?o=m".$props["YCLIENTS_CODE"]["VALUE"];
	}

	private function getExperienceTotal($props)
	{
		if(empty($props["EMPLOYMENT_BEGIN"])){
			return null;
		}

		$employmentBegin = $props["EMPLOYMENT_BEGIN"]["VALUE"];
		$experienceInYears = FormatDate("Ydiff", strtotime($employmentBegin));
		list($value, $title) = explode(" ", $experienceInYears);

		return compact("value", "title");
	}

	private function getCertificates($props)
	{
		if(empty($props["DIPLOMA_HOR_IB"])){
			return null;
		}
		$elementsId = $props["DIPLOMA_HOR_IB"]["VALUE"];

		$certificates = [];
		foreach ($elementsId as $elementId){
			$fields = getIbElementFields($elementId);
			$certificates[] = new Certificate($fields);
		}

		return $certificates;
	}

	private function getWorks($id)
	{
		$arOrder = ["SORT" => "ASC", "ID" => "ASC"];
		$arFilter = [
			"IBLOCK_ID" => ID_IBLOCK_WORKS,
			"PROPERTY_MASTER" => $id,
			"ACTIVE" => "Y",
			"GLOBAL_ACTIVE" => "Y",
		];
		$arSelect = ["ID", "IBLOCK_ID", "NAME"];

		$res = \CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);

		$works = [];
		while($obj = $res->GetNextElement()) {
			$obj->props = $obj->GetProperties();
			$works[] = new Work($obj);
		}

		return $works;
	}

	private function getFeedbacks($id)
	{
		$arOrder = ["created_date" => "DESC", "SORT" => "ASC"];
		$arFilter = [
			"IBLOCK_ID" => ID_IBLOCK_FEEDBACKS,
			"PROPERTY_MASTER" => $id,
			"ACTIVE" => "Y",
			"GLOBAL_ACTIVE" => "Y",
		];
		$arSelect = ["ID", "IBLOCK_ID", "NAME", "PREVIEW_TEXT"];

		$res = \CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);

		$feedbacks = [];
		while($obj = $res->GetNextElement()) {
			$obj->props = $obj->GetProperties();
			$feedbacks[] = new Feedback($obj);
		}

		return $feedbacks;
	}

}
