<?
namespace Masters;

class Feedback
{
	private $id;
	public $name;
	public $text;
	public $isVideo;

	public function __construct($resource)
	{
		$this->id = (int)$resource->fields["ID"];
		$this->name = $resource->fields["NAME"];
		$this->text = $resource->fields["PREVIEW_TEXT"];
		$this->isVideo = $resource->props["IS_VIDEO"]["VALUE"] == "Y";
	}
}