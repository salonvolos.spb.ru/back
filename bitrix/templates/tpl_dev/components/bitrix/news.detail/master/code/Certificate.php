<?
namespace Masters;

class Certificate
{
	public $src;
	public $previewText;
	public $detailText;

	public function __construct($fields)
	{
		$this->src = getImageSrc($fields["PREVIEW_PICTURE"]);
		$this->previewText = $fields["~PREVIEW_TEXT"];
		$this->detailText = $fields["~DETAIL_TEXT"];
	}
}
