<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$description = "";

if(!empty($arResult["~PREVIEW_TEXT"])){
    $description = getTruncatedDataForSeo($arResult["~PREVIEW_TEXT"]);
}

if(empty($description)){
    if(!empty($arResult["~DETAIL_TEXT"])){
        $description = getTruncatedDataForSeo($arResult["~DETAIL_TEXT"]);
    }
}

if(!empty($description)){
    $APPLICATION->SetPageProperty("description", $description);
}

