<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
include __DIR__."/code/Master.php";
include __DIR__."/code/Certificate.php";
include __DIR__."/code/Work.php";
include __DIR__."/code/Feedback.php";
/** @var array $arResult */
$master = new Masters\Master($arResult);
include __DIR__."/template/index.php";
