<? /** @var Masters\Master $master */ ?>
<section class="container page-section page-section--no-padding--top" id="page-works">
	<div class="content">

		<h2>Работы</h2>

		<div class="page-subsection grid grid--align-items--flex-start">

            <? foreach ($master->works as $work): ?>
                <? include __DIR__."/work.php" ?>
			<? endforeach ?>

			<div class="grid__cell grid__cell--xs--12">
                <a href="#" id="works-link">Больше работ</a>
			</div>

		</div>
	</div>
</section>
