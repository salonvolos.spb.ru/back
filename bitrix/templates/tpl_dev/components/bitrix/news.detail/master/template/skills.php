<? /** @var Masters\Master $master */ ?>
<? if($master->skills): ?>
    <ul class="unordered-list">
        <? foreach ($master->skills as $skill): ?>
            <li class="unordered-list__item"><?= $skill ?></li>
        <? endforeach ?>
    </ul>
<? endif ?>
