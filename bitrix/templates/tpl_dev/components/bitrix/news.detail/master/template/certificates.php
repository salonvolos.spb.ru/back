<? /** @var Masters\Master $master */ ?>
<section class="container page-section page-section--staff-certificates page-section--no-padding--top">
    <div class="content">
        <div class="page-subsection">

            <h2>Сертификаты</h2>

            <div class="page-subsection">
                <div class="grid">
                    <? foreach ($master->certificates as $index => $certificate): ?>
                        <? include __DIR__."/certificate.php" ?>
                    <? endforeach ?>
                </div>
            </div>

        </div>
    </div>
</section>
