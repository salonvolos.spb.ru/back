<? /** @var Masters\Master $master */ ?>
<? /** @var Masters\Certificate $certificate */ ?>
<? /** @var int $index */ ?>
<a
	class="grid__cell grid__cell--l--2 grid__cell--m--3 grid__cell--s--6 grid__cell--xs--6"
	href="<?= $certificate->src ?>"
	data-glightbox="data-glightbox"
	data-gallery="certificates"
	<? if($index + 1 > $master->certificatesMax): ?>style="display: none;"<? endif ?>
>
	<picture>
		<source
			srcset="<?= WebPHelper::getOrCreateWebPUrl($certificate->src, [173, 346]) ?>"
			type="image/webp"
		/>
		<img
			src="<?= $certificate->src ?>"
			loading="lazy"
			decoding="async"
			alt="<?= $certificate->previewText ?>"
			title="<?= $certificate->detailText ?>"
			width="<?= DimensionHelper::getWidth($certificate->src) ?>"
			height="<?= DimensionHelper::getHeight($certificate->src) ?>"
		/>
	</picture>
</a>