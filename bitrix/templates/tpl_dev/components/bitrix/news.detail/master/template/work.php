<? /** @var Masters\Work $work */ ?>
<a href="<?= WebPHelper::getOrCreateWebPUrl($work->image) ?>" class="works-item grid__cell grid__cell--l--3 grid__cell--m--4 grid__cell--s--6 grid__cell--xs--6" data-glightbox>

	<div class="works-item__title"><?= $work->title ?></div>
	<div class="works-item__price"><?= $work->price ?></div>

	<picture>
		<source srcset="<?= WebPHelper::getOrCreateWebPUrl($work->image) ?>" type="image/webp"/>
		<img
			class="works-item__image"
			src="<?= $work->image ?>"
			loading="lazy"
			decoding="async"
			alt="<?= $work->title ?>"
			title="<?= $work->title ?>"
		/>
	</picture>

</a>
