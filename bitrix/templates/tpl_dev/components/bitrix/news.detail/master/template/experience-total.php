<? /** @var Masters\Master $master */ ?>
<? if($master->experienceTotal): ?>
    <div class="staff-experience">
        <p class="staff-experience__text">опыт работы</p>
        <p class="staff-experience__year"><?= $master->experienceTotal["value"] ?></p>
        <p class="staff-experience__text"><?= $master->experienceTotal["title"] ?></p>
    </div>
<? endif ?>
