<? /** @var Masters\Master $master */ ?>
<? if($master->appointmentUrl): ?>
    <div class="grid__cell grid__cell--l--8 grid__cell--m--8 grid__cell--s--8 grid__cell--xs--10">
        <a
            class="button staff-info__button ms_booking"
            href="#"
            onclick="event.preventDefault()"
            data-url="<?= $master->appointmentUrl ?>"
        >Записаться</a>
    </div>
<? endif ?>
