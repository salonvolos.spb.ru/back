<? /** @var Masters\Master $master */ ?>
<? if($master->education): ?>

    <h2 class="h3 staff-info__title">Образование</h2>

    <ul class="unordered-list par">
        <? foreach ($master->education as $education): ?>
            <li class="unordered-list__item"><?= $education ?></li>
        <? endforeach ?>
    </ul>

<? endif ?>
