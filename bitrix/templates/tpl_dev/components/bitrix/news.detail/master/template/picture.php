<? /** @var Masters\Master $master */ ?>
<? if($master->picture): ?>
    <picture>
        <source srcset="<?= WebPHelper::getOrCreateWebPUrl($master->picture, [600, 1200]) ?>" type="image/webp"/>
        <img
            src="<?= $master->picture ?>"
            loading="lazy"
            decoding="async"
            alt="<?= $master->name ?>"
            title="<?= $master->name ?>"
            width="<?= DimensionHelper::getWidth($master->picture) ?>"
            height="<?= DimensionHelper::getHeight($master->picture) ?>"
        />
    </picture>
<? endif ?>
