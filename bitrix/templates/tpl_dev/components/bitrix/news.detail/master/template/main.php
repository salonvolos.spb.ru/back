<? /** @var Masters\Master $master */ ?>
<div class="page-subsection">
    <div class="grid staff-info">

        <div class="grid__cell grid__cell--l--4 grid__cell--m--4 grid__cell--s--8 grid__cell--xs--10 staff-info__image">
            <? include __DIR__."/picture.php" ?>
            <? include __DIR__."/appointment.php" ?>
        </div>

        <div class="grid__cell grid__cell--l--8 grid__cell--m--8 grid__cell--s--12 grid__cell--xs--12">
            <div class="block-marked block-marked--padding-small--y staff-info__announcement par">
                <? include __DIR__."/skills.php" ?>
                <? include __DIR__."/experience-total.php" ?>
            </div>
            <? include __DIR__."/education.php" ?>
            <? include __DIR__."/experience-list.php" ?>
            <? include __DIR__."/description.php" ?>
        </div>

    </div>
</div>


