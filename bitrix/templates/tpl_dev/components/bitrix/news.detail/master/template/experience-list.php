<? /** @var Masters\Master $master */ ?>
<? if($master->experienceList): ?>

    <h2 class="h3 staff-info__title">Опыт работы</h2>

    <ul class="unordered-list par">
        <? foreach ($master->experienceList as $experience): ?>
            <li class="unordered-list__item"><?= $experience ?></li>
        <? endforeach ?>
    </ul>

<? endif ?>
