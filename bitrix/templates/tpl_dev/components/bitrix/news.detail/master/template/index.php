<? /** @var Masters\Master $master */ ?>
<section class="container page-section">
    <div class="content">
        <h1><?= $master->name ?></h1>
        <? include __DIR__."/main.php" ?>
    </div>
</section>

<? if($master->certificates): ?>
    <? include __DIR__."/certificates.php" ?>
<? endif ?>

<? if($master->works): ?>
    <? include __DIR__."/works.php" ?>
<? endif ?>

<? if($master->feedbacks): ?>
    <? include __DIR__."/feedbacks.php" ?>
<? endif ?>
