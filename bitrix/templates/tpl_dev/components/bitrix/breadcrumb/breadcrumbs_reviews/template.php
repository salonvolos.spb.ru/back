<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(empty($arResult)){return "";}

$lastIndex = count($arResult) - 1;

$string = '<div class="breadcrumbs">';

foreach ($arResult as $index => $item){
	if($index == 0) {
        $string .= '<div class="breadcrumbs__parent"><a class="link-secondary" href="'.$item["LINK"].'">'.$item["TITLE"].'</a></div>';
        $string .= '<div class="breadcrumbs__separator"></div>';

	}elseif ($index == 1){
        $string .= '<div class="breadcrumbs__parent">'.$item["TITLE"].'</div>';

    }
}

$itemListElements = [];
foreach ($arResult as $index => $item){
	$itemListElements[] = [
		"@type" => "ListItem",
		"position" => $index + 1,
		"item" => [
			"@id" => $item["LINK"],
			"name" => $item["TITLE"]
		]
	];
}
$string .= '<script type="application/ld+json">';
	$string .= '{';
	$string .= '"@context": "https://schema.org",';
	$string .= '"@type": "BreadcrumbList",';
	$string .= '"itemListElement": '.json_encode($itemListElements);
$string .= '}';
$string .= '</script>';

$string .= '</div>';

return $string;
