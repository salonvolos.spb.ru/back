<div class="page-section">
    <div class="container">
        <div class="content">
            <p>
				<? if($arResult["REQUEST"]["HOW"]=="d"): ?>
                    <b><?=GetMessage("SEARCH_SORTED_BY_DATE")?></b>&nbsp;|&nbsp;
                    <a
                        href="<?= $arResult["URL"] ?>&amp;how=r<?= $arResult["REQUEST"]["FROM"]? '&amp;from='.$arResult["REQUEST"]["FROM"]: ''?><?= $arResult["REQUEST"]["TO"]? '&amp;to='.$arResult["REQUEST"]["TO"]: ''?>"
                    ><?= GetMessage("SEARCH_SORT_BY_RANK") ?></a>
				<? else: ?>
                    <b><?= GetMessage("SEARCH_SORTED_BY_RANK") ?></b>&nbsp;|&nbsp;
                    <a
                        href="<?= $arResult["URL"]?>&amp;how=d<?= $arResult["REQUEST"]["FROM"]? '&amp;from='.$arResult["REQUEST"]["FROM"]: ''?><?= $arResult["REQUEST"]["TO"]? '&amp;to='.$arResult["REQUEST"]["TO"]: ''?>"
                    ><?= GetMessage("SEARCH_SORT_BY_DATE") ?></a>
				<? endif ?>
            </p>
        </div>
    </div>
</div>
