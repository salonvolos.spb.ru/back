<div class="page-section">
    <div class="container">
        <div class="content">
            <form action="" method="get" class="header-search">

                <input
                    class="header-search__input"
                    type="search"
                    placeholder="Поиск по сайту"
                    name="q"
                    value="<?= $arResult["REQUEST"]["QUERY"] ?>"
                    size="40"
                >

                <button
                    class="header-search__submit"
                    type="submit"
                ></button>

                <input type="hidden" name="how" value="<?= $arResult["REQUEST"]["HOW"]=="d"? "d": "r" ?>" />

            </form>
        </div>
    </div>
</div>