<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>

        <div class="grid page-subsection">
            <? include __DIR__."/include/form.php" ?>
        </div>


<? if(count($arResult["SEARCH"]) > 0): ?>
        <div class="page-subsection">
            <div class="grid">
                <div class="grid__cell grid__cell--m--9 grid__cell--xs--12">
                    <? foreach($arResult["SEARCH"] as $arItem): ?>
						<? include __DIR__."/include/found-item.php" ?>
					<? endforeach ?>
                </div>
            </div>
        </div>

    </div>
</section>

<section class="page-section container page-section--no-padding--top ">
    <div class="content">

        <div class="page-subsection">
			<?= $arResult["NAV_STRING"] ?>
        </div>

        <div class="page-subsection">
            <? if($arResult["REQUEST"]["HOW"]=="d"): ?>
                <a href="<?=$arResult["URL"]?>&amp;how=r<?echo $arResult["REQUEST"]["FROM"]? '&amp;from='.$arResult["REQUEST"]["FROM"]: ''?><?echo $arResult["REQUEST"]["TO"]? '&amp;to='.$arResult["REQUEST"]["TO"]: ''?>"><?=GetMessage("SEARCH_SORT_BY_RANK")?></a>&nbsp;|&nbsp;<b><?=GetMessage("SEARCH_SORTED_BY_DATE")?></b>
            <? else: ?>
                <b><?=GetMessage("SEARCH_SORTED_BY_RANK")?></b>&nbsp;|&nbsp;<a href="<?=$arResult["URL"]?>&amp;how=d<?echo $arResult["REQUEST"]["FROM"]? '&amp;from='.$arResult["REQUEST"]["FROM"]: ''?><?echo $arResult["REQUEST"]["TO"]? '&amp;to='.$arResult["REQUEST"]["TO"]: ''?>"><?=GetMessage("SEARCH_SORT_BY_DATE")?></a>
            <? endif ?>
        </div>

<? else: ?>
    <? ShowNote(GetMessage("SEARCH_NOTHING_TO_FOUND")) ?>
<? endif ?>
