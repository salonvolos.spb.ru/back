<?
//Navigation chain template
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arChainBody = array();
foreach($arCHAIN as $item)
{
	if(mb_strlen($item["LINK"]) < mb_strlen(SITE_DIR)) {continue;}

	if($item["LINK"] <> ""){
		$chain = '<a href="'.$item["LINK"].'">'.htmlspecialcharsex($item["TITLE"]).'</a>';
		$chain = '<div class="search-result__path-parent">'.$chain.'</div>';
		$arChainBody[] = $chain;
	}
	else{
		$chain = htmlspecialcharsex($item["TITLE"]);
		$chain = '<div class="search-result__path-current">'.$chain.'</div>';
		$arChainBody[] = $chain;
	}

}

$result = implode('<div class="search-result__path-separator"></div>', $arChainBody);
$result = '<div class="search-result__path">'.$result.'</div>';

return $result;
?>
