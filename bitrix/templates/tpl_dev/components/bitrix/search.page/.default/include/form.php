<form
	action=""
	method="get"
	class="grid__cell grid__cell--m--9 grid__cell--xs--12"
>

	<div class="grid grid--align-items--center par search">

		<label class="form-input form-input--text grid__cell grid__cell--m--9 grid__cell--s--8 grid__cell--xs--12">
			<input type="text" name="q" value="<?= $arResult["REQUEST"]["QUERY"] ?>" size="40" class="form-input__field" />
		</label>

		<input
			class="button button--type--submit button--size--s grid__cell grid__cell--m--3 grid__cell--s--4 grid__cell--xs--12"
			type="submit"
			value="Найти"
		/>

		<input type="hidden" name="how" value="<?= $arResult["REQUEST"]["HOW"] == "d" ? "d" : "r" ?>" />
	
	</div>

</form>





