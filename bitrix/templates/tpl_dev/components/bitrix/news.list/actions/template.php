<?php if
(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {die();}
$status = false;
?>

<? foreach ($arResult["ITEMS"] as $arItem){
    if (!empty($arItem["NAME"])){
        $status = true;
    }
}?>

    <?
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
    <section class="container page-section page-section--no-padding--top">
        <div class="content">
            <?php
            if ($status){?>
                <h2 id="title-5">Акции</h2>
                <div class="grid discount-item page-subsection">
                    <? foreach ($arResult["ITEMS"] as $arItem){ ?>
                        <div class="grid__cell grid__cell--l--4 grid__cell--m--6 grid__cell--s--6 grid__cell--xs--12 discount-item__wrapper">
                            <div class="discount-item__image">
                                <a href="<?php echo $arItem['DETAIL_PAGE_URL']?>">
                                    <picture>
                                        <source srcset="<?php echo WebPHelper::getOrCreateWebPUrl($arItem["PREVIEW_PICTURE"]["SRC"], [800, 1600])?>" type="image/webp">
                                        <?php if (!empty($arItem["PREVIEW_PICTURE"]["SRC"])) {?>
                                            <img src="<?php echo $arItem["PREVIEW_PICTURE"]["SRC"]?>" loading="lazy" decoding="async" alt="<?php echo $arItem['NAME']?>" title="<?php echo $arItem['NAME']?>" width="800" height="800">

                                        <?php }?>
                                    </picture>
                                </a>
                            </div>
                            <div class="block-marked discount-item__body">
                                <?php if (!empty($arItem["NAME"])) {?>
                                    <h3>
                                        <a class="discount-item__title" href="<?php echo $arItem['DETAIL_PAGE_URL']?>">
                                            <?php echo $arItem['NAME']?>
                                        </a>
                                    </h3>
                                <?php } ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>


        </div>
    </section>


