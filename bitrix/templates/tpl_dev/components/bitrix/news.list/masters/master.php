<? /** @var array $arItem */ ?>
<?
$tagId = "home-master-description--".$arItem["CODE"];
$photoSrc = !empty($arItem["PREVIEW_PICTURE"]["SRC"]) ? $arItem["PREVIEW_PICTURE"]["SRC"] : false;
$skills = !empty($arItem["DISPLAY_PROPERTIES"]["SKILLS"]) ? $arItem["DISPLAY_PROPERTIES"]["SKILLS"]["VALUE"] : false;
$onlineCode = !empty($arItem["DISPLAY_PROPERTIES"]["YCLIENTS_CODE"]) ? $arItem["DISPLAY_PROPERTIES"]["YCLIENTS_CODE"]["VALUE"] : false;
?>
<div class="grid__cell grid__cell--l--2 grid__cell--m--5 grid__cell--s--10 grid__cell--xs--5 master-item">
	<div class="master-item__image">
		<? if($photoSrc): ?>
            <picture>
                <source srcset="<?= WebPHelper::getOrCreateWebPUrl($photoSrc, [480, 960]) ?>" type="image/webp"/>
                <img
                    src="<?= $photoSrc ?>"
                    alt="<?= $arItem["NAME"] ?>"
                    title="<?= $arItem["NAME"] ?>"
                    class="master-item__image-item"
                    loading="lazy"
                    decoding="async"
                    width="480"
                    height="960"
                />
            </picture>
		<? endif ?>
        <a
            class="button master-item__button ms_booking"
            href="#"
            data-url="https://n371027.yclients.com/company:170918?o=m<?= $onlineCode ?>"
        >Записаться</a>
	</div>

	<h3 class="master-item__title"><?= $arItem["NAME"] ?></h3>

	<? if($skills): ?>
        <ul class="master-item__list">
            <? foreach ($skills as $skill): ?>
                <li class="master-item__list-item"><?= $skill ?></li>
            <? endforeach ?>
        </ul>
	<? endif ?>

	<a
        class="master-item__more"
        href="<?= $arItem["DETAIL_PAGE_URL"] ?>"
    >Подробнее</a>

</div>
