<section class="container page-section page-section--no-padding--top">
	<div class="content">

		<h2 class="not-menu-content">Средний стаж мастеров салона — 12 лет</h2>

        <div class="page-subsection grid grid--justify--center">
			<div class="grid__cell grid__cell--l--9 grid__cell--m--10 grid__cell--xs--12 text-align-center">
				<p>Мастера обучались в ведущих студиях и академиях: <strong>Coiffure, Локон, Londa, Shwarzkopf, Wella.</strong> </p>
				<p>Сотрудники салона неоднократно участвовали в международном фестивале красоты <strong>«Невские берега»,</strong> крупнейшей в Европе парфюмерно-косметической выставке <strong>Intercharm.</strong></p>
			</div>
		</div>

		<div class="grid grid--columns--10 par page-subsection">

            <? foreach ($arResult["ITEMS"] as $arItem): ?>
                <? include __DIR__."/master.php" ?>
            <? endforeach ?>

		</div>
	</div>
</section>
