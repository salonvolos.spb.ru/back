<? /** @var Services\Service $service */ ?>
<? if($arResult["ITEMS"]): ?>
<section class="container page-section page-section--no-padding--top">
	<div class="content">

		<h2>Отзывы клиенток</h2>

        <div class="page-subsection">
			<div class="slider feedback-item">
				<div class="slider__slides swiper">
					<div class="slider__wrapper swiper-wrapper">
                        <? foreach ($arResult["ITEMS"] as $arItem): ?>
                            <? include __DIR__."/item.php" ?>
                        <? endforeach ?>
					</div>

					<div class="slider__arrow slider__arrow--prev"></div>
					<div class="slider__arrow slider__arrow--next"></div>
					<div class="slider__pagination"></div>

				</div>
			</div>
		</div>

	</div>
</section>
<? endif ?>