<? /** @var array $arItem */

$displayProps = $arItem["DISPLAY_PROPERTIES"];

$isVideo = false;
if($displayProps["IS_VIDEO"]["VALUE"] == "Y"){
	$isVideo = true;
}
?>

<div class="slide swiper-slide feedback-item__slide">

    <? /* <div class="hair-extention-review__title">
		<?= $arItem["NAME"] ?>
    </div> */ ?>

	<? if($isVideo): ?>
        <div class="iframe-responsive">
            <iframe
                src="<?= $arItem["~PREVIEW_TEXT"] ?>"
                allowfullscreen=""
            ></iframe>
        </div>
	<? else: ?>
        <div class="plain-text">
			<?= $arItem["PREVIEW_TEXT"] ?>
        </div>
	<? endif ?>

</div>
