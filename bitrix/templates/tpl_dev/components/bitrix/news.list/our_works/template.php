<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {die();}

if($arResult["ITEMS"]): ?>
<section class="container page-section page-section--no-padding--top" id="page-works">
    <div class="content">

        <? if(!$arParams["USER_PARAM_IS_EMBED_OUR_WORKS"]): ?>
            <h2>Наши работы</h2>
        <? endif ?>

        <div class="page-subsection grid grid--align-items--flex-start">

            <? foreach ($arResult["ITEMS"] as $arItem): ?>

                <?
                    $photoFirstSrc = false;
                    if(!empty($arItem["DISPLAY_PROPERTIES"]["PHOTO"])){
                        $photoProp = $arItem["DISPLAY_PROPERTIES"]["PHOTO"];
                        if(count($photoProp["VALUE"]) == 1){
                            $photoFirstSrc = $photoProp["FILE_VALUE"]["SRC"];
                        }else{
                            $photoFirstSrc = $photoProp["FILE_VALUE"][0]["SRC"];
                        }
                    }
                ?>
                <a href="<?= WebPHelper::getOrCreateWebPUrl($photoFirstSrc) ?>" data-glightbox class="works-item grid__cell grid__cell--l--3 grid__cell--m--4 grid__cell--s--6 grid__cell--xs--6">

                    <div class="works-item__title" itemprop="name" data-id="<?=$arItem['ID']?>"></div>
                    <div class="works-item__price" itemprop="description"  data-id="<?=$arItem['ID']?>"></div>

                    <? if ($photoFirstSrc): ?>
                        <picture itemscope itemtype="http://schema.org/ImageObject">
                            <source
                                srcset="<?= WebPHelper::getOrCreateWebPUrl($photoFirstSrc) ?>"
                                type="image/webp"
                            />
                            <img
                                class="works-item__image"
                                src="<?= $photoFirstSrc ?>"
                                loading="lazy"
                                decoding="async"
                                alt="<?= $arItem["NAME"] ?>"
                                title="<?= $arItem["NAME"] ?>"
                            />
                        </picture>
                    <? endif ?>

                </a>
            <? endforeach ?>

            <div class="grid__cell grid__cell--xs--12">
                <a href="#" id="works-link">Больше работ</a>
            </div>

        </div>

        <? if(!empty($arParams["TEXT_OUR_WORKS"])): ?>
            <p class="small"><?= $arParams["TEXT_OUR_WORKS"] ?></p>
        <? endif ?>
    </div>


</section>
<? endif ?>
