<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main\Localization\Loc;
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$displayed_values = [];
?>


<div class="page-subsection service-navigation">
    <a class="button button--secondary service-navigation__item" href="#" data-service="all">Все работы
    </a>

    <?php
    foreach ($arResult['ITEMS'] as $item) {
        if (!empty($item['DISPLAY_PROPERTIES']['TABS']) && is_array($item['DISPLAY_PROPERTIES']['PHOTO']['DISPLAY_VALUE'])) {
            $value = $item['DISPLAY_PROPERTIES']['TABS']['VALUE_XML_ID'];

            if (!in_array($value, $displayed_values)) { // Проверяем, было ли значение уже выведено
                $displayed_values[] = $value; // Добавляем значение в массив выведенных значений

                ?>
                <a class="button button--secondary service-navigation__item" href="#" data-service="<?php echo $value ?>">
                    <?php echo $item['DISPLAY_PROPERTIES']['TABS']['DISPLAY_VALUE']?>
                </a>
                <?php
            }
        }
    }
    ?>

</div>
<div class="page-subsection grid">



    <?php foreach($arResult["ITEMS"] as $arItem){?>
        <?php

    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

    ?>
    <?php if (!empty($arItem['DISPLAY_PROPERTIES']['TABS']) && is_array($arItem['DISPLAY_PROPERTIES']['PHOTO']['DISPLAY_VALUE'])){?>
            <a class="works-item grid__cell grid__cell--l--3 grid__cell--m--4 grid__cell--s--6 grid__cell--xs--6" id="<?=$this->GetEditAreaId($arItem['ID']);?>"
               href="<?php echo $arItem['DISPLAY_PROPERTIES']['PHOTO']['FILE_VALUE'][1]['SRC'] ?>"
                <?php if (!empty($arItem['DISPLAY_PROPERTIES']['SERVICE_NAME']['DISPLAY_VALUE'])) { ?>
                    data-service="<?php echo $arItem['DISPLAY_PROPERTIES']['SERVICE_NAME']['DISPLAY_VALUE'] ?>"
                <?php }; ?>
                <?php if (!empty($arItem['DISPLAY_PROPERTIES']['SERVICE_DESC']['DISPLAY_VALUE'])) { ?>
                    data-description="<?php echo $arItem['DISPLAY_PROPERTIES']['SERVICE_DESC']['DISPLAY_VALUE'] ?>"
                <?php }; ?>
               data-glightbox="data-glightbox">

            <?php if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])) {?>
                    <?php if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])) {?>

                    <?php } ?>
                <?php } ?>
                <?php if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]) {?>
                    <div class="works-item__title"><?php echo $arItem['NAME']?></div>
                    <?php if(!empty($arItem['DISPLAY_PROPERTIES']['PRICE'])) {?>
                        <div class="works-item__price"><?=$arItem['DISPLAY_PROPERTIES']['PRICE']['DISPLAY_VALUE']?></div>

                    <?php }?>

                    <picture>
                        <source srcset="<?= $arItem['DISPLAY_PROPERTIES']['PHOTO']['FILE_VALUE'][1]['SRC'] ?>" type="image/webp">
                        <source srcset="<?= $arItem['DISPLAY_PROPERTIES']['PHOTO']['FILE_VALUE'][0]['SRC'] ?>">
                        <img class="works-item__image"
                            src="<?= $arItem['DISPLAY_PROPERTIES']['PHOTO']['FILE_VALUE'][0]['SRC'] ?>" loading="lazy"
                            decoding="async" alt="">
                    </picture>

                <?php } ?>

            </a>

        <?php } ?>

    <?php }?>

</div>
<div class="grid grid--justify--center page-subsection">
    <a class="button grid__cell grid__cell--m--4 grid__cell--s--6" href="#" id="works-button">
        <?php echo Loc::getMessage('DOWNLOAD_MORE')?>
    </a>
</div>

