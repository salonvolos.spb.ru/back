<? /** @var array $arResult */ ?>
<div class="slider__slides swiper">

    <div class="slider__wrapper swiper-wrapper">
		<? foreach($arResult["ITEMS"] as $arItem): ?>
			<? $slide = new SliderHome\Slide($arItem) ?>
			<? include __DIR__."/slide.php" ?>
		<? endforeach ?>
    </div>

    <div class="slider__arrow slider__arrow--prev"></div>
    <div class="slider__arrow slider__arrow--next"></div>

    <div class="slider__pagination"></div>

</div>

