<? /** @var MastersList\Master $master */ ?>
<div class="grid__cell grid__cell--l--2 grid__cell--m--5 grid__cell--s--10 grid__cell--xs--5 master-item">

    <? if($master->picture): ?>
        <? include __DIR__."/picture.php" ?>
    <? endif ?>

    <h2 class="master-item__title"><?= $master->name ?></h2>

    <? if($master->skills): ?>
    <ul class="master-item__list">
        <? foreach ($master->skills as $skill): ?>
            <li class="master-item__list-item"><?= $skill ?></li>
        <? endforeach ?>
    </ul>
    <? endif ?>

    <a
        class="master-item__more"
        href="<?= $master->url ?>"
    >Подробнее</a>

</div>
