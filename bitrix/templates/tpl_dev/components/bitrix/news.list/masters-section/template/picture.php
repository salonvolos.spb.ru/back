<? /** @var MastersList\Master $master */ ?>
<div class="master-item__image">

	<picture>
        <source srcset="<?= WebPHelper::getOrCreateWebPUrl($master->picture, [480, 960]) ?>" type="image/webp"/>
        <source srcset="<?= $master->picture ?>"/>
        <img
			class="master-item__image-item"
			src="<?= $master->picture ?>"
			loading="lazy"
			decoding="async"
			alt="<?= $master->name ?>"
			title="<?= $master->name ?>"
			width="480"
			height="960"
		/>
	</picture>

	<a
        class="button master-item__button ms_booking"
        href="#"
        data-url="https://n371027.yclients.com/company:170918?o=m<?= $master->onlineCode ?>"
    >Записаться</a>

</div>
