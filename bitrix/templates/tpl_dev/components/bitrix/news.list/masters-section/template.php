<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {die();}
include_once __DIR__."/Master.php";
?>
<div class="page-subsection">
    <div class="grid grid--columns--10">

		<? foreach($arResult["ITEMS"] as $arItem): ?>
			<? $master = new MastersList\Master($arItem) ?>
			<? include __DIR__."/template/master.php" ?>
		<? endforeach ?>

    </div>
</div>
