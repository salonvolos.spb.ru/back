<?
namespace MastersList;

class Master
{
	public $name;
	public $url;
	public $picture;
	public $onlineCode;
	public $skills;

	public function __construct($arItem)
	{
		$this->name = $arItem["NAME"];
		$this->url = $arItem["DETAIL_PAGE_URL"];
		$this->picture = $arItem["PREVIEW_PICTURE"] ? $arItem["PREVIEW_PICTURE"]["SRC"] : null;
		$this->onlineCode = $arItem["DISPLAY_PROPERTIES"]["YCLIENTS_CODE"]["VALUE"];
		$this->skills = $arItem["DISPLAY_PROPERTIES"]["SKILLS"] ? $arItem["DISPLAY_PROPERTIES"]["SKILLS"]["VALUE"] : null;
	}

}
