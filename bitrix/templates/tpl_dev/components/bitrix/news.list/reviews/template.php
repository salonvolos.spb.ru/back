<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main\Localization\Loc;
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$uniqueValues = array();
$serviceDataAttr = '';
$serviceDataItem = '';
$displayed_values = array(); // Массив для отслеживания уже выведенных значений

?>
<div class="grid grid--columns--10 page-subsection feedback-site">
    <div class="grid__cell grid__cell--m--2 grid__cell--s--5 grid__cell--xs--10 block-marked block-marked--padding-small feedback-site__item"><a href="https://yandex.ru/maps/org/tsentr_dizayna_volos/1881119709/reviews/?ll=30.312785%2C59.908087&amp;z=15" target="_blank"> <img class="feedback-site__image" src="<?=SITE_TEMPLATE_PATH?>/images/feedback/yandex-maps.svg" loading="lazy" decoding="async"></a>
        <div class="text-marked text-marked--size--xs text-align-center">
            <?php echo Loc::getMessage('YANDEX_MAPS')?>
        </div>
        <div class="text-align-center"><strong>5 из 5</strong></div>
    </div>
    <div class="grid__cell grid__cell--m--2 grid__cell--s--5 grid__cell--xs--10 block-marked block-marked--padding-small feedback-site__item"><a href="https://www.google.com/maps/place/%D0%A6%D0%B5%D0%BD%D1%82%D1%80+%D0%94%D0%B8%D0%B7%D0%B0%D0%B9%D0%BD%D0%B0+%D0%92%D0%BE%D0%BB%D0%BE%D1%81/@59.9079854,30.3108918,17z/data=!3m1!4b1!4m5!3m4!1s0x469630e5425b37c1:0x8261d1ae80293e03!8m2!3d59.9079827!4d30.3130858?hl=RU" target="_blank"> <img class="feedback-site__image" src="<?=SITE_TEMPLATE_PATH?>/images/feedback/google-maps.svg" loading="lazy" decoding="async"></a>
        <div class="text-marked text-marked--size--xs text-align-center">
            <?php echo Loc::getMessage('GOOGLE_MAPS')?>
        </div>
        <div class="text-align-center"><strong>4,3 из 5</strong></div>
    </div>
    <div class="grid__cell grid__cell--m--2 grid__cell--s--5 grid__cell--xs--10 block-marked block-marked--padding-small feedback-site__item"><a href="https://2gis.ru/spb/search/%D1%86%D0%B5%D0%BD%D1%82%D1%80%20%D0%B4%D0%B8%D0%B7%D0%B0%D0%B9%D0%BD%D0%B0%20%D0%B2%D0%BE%D0%BB%D0%BE%D1%81%20%D1%81%D0%BF%D0%B1/firm/5348552838696892/30.31292%2C59.908049/tab/reviews?m=30.312901%2C59.908072%2F17.15" target="_blank"> <img class="feedback-site__image" src="<?=SITE_TEMPLATE_PATH?>/images/feedback/2gis.svg" loading="lazy" decoding="async"></a>
        <div class="text-marked text-marked--size--xs text-align-center">
            <?php echo Loc::getMessage('2_GIS')?>
        </div>
        <div class="text-align-center"><strong>5 из 5</strong></div>
    </div>
    <div class="grid__cell grid__cell--m--2 grid__cell--s--5 grid__cell--xs--10 block-marked block-marked--padding-small feedback-site__item"><a href="https://zoon.ru/spb/beauty/tsentr_dizajna_volos_na_metro_frunzenskaya/reviews/" target="_blank"> <img class="feedback-site__image" src="<?=SITE_TEMPLATE_PATH?>/images/feedback/zoon.svg" loading="lazy" decoding="async"></a>
        <div class="text-marked text-marked--size--xs text-align-center">
            <?php echo Loc::getMessage('ZOON')?>
        </div>
        <div class="text-align-center"><strong>4,5 из 5</strong></div>
    </div>
    <div class="grid__cell grid__cell--m--2 grid__cell--s--5 grid__cell--xs--10 block-marked block-marked--padding-small feedback-site__item"><a href="https://www.yell.ru/spb/com/tsentr-dizayna-volos_6309646/reviews/" target="_blank"> <img class="feedback-site__image" src="<?=SITE_TEMPLATE_PATH?>/images/feedback/yell.svg" loading="lazy" decoding="async"></a>
        <div class="text-marked text-marked--size--xs text-align-center">
            <?php echo Loc::getMessage('YELL')?>
        </div>
        <div class="text-align-center"><strong>5 из 5</strong></div>
    </div>
</div>

<div class="page-subsection service-navigation">
    <a class="button button--secondary service-navigation__item" href="#" data-service="all">Все работы
    </a>

    <?
    foreach ($arResult['ITEMS'] as $item) {
        if (!empty($item['DISPLAY_PROPERTIES']['TABS'])) {
            $value = $item['DISPLAY_PROPERTIES']['TABS']['VALUE_XML_ID'];

            if (!in_array($value, $displayed_values)) { // Проверяем, было ли значение уже выведено
                $displayed_values[] = $value; // Добавляем значение в массив выведенных значений

                ?>
                <a class="button button--secondary service-navigation__item" href="#" data-service="<?php echo $value ?>">
                    <?php echo $item['DISPLAY_PROPERTIES']['TABS']['DISPLAY_VALUE']?>
                </a>
                <?php
            }
        }
    }
    ?>
    <div class="service-navigation__selects">
        <div class="service-navigation__item service-navigation__item--master" data-master="">
            <label class="form-input form-input--select">
                <select class="form-input__field">
                    <option value="">
                        <?php echo Loc::getMessage('CHOOSE_MASTER')?>
                    </option>
                    <option value="<?php echo Loc::getMessage('MASTER_1')?>">
                        <?php echo Loc::getMessage('MASTER_1')?>
                    </option>
                    <option value="<?php echo Loc::getMessage('MASTER_2')?>">
                        <?php echo Loc::getMessage('MASTER_2')?>
                    </option>
                    <option value="<?php echo Loc::getMessage('MASTER_3')?>">
                        <?php echo Loc::getMessage('MASTER_3')?>
                    </option>
                    <option value="<?php echo Loc::getMessage('MASTER_4')?>">
                        <?php echo Loc::getMessage('MASTER_4')?>
                    </option>
                    <option value="<?php echo Loc::getMessage('MASTER_5')?>">
                        <?php echo Loc::getMessage('MASTER_5')?>
                    </option>
                    <option value="<?php echo Loc::getMessage('MASTER_6')?>">
                        <?php echo Loc::getMessage('MASTER_6')?>
                    </option>
                    <option value="<?php echo Loc::getMessage('MASTER_7')?>">
                        <?php echo Loc::getMessage('MASTER_7')?>
                    </option>
                    <option value="<?php echo Loc::getMessage('MASTER_8')?>">
                        <?php echo Loc::getMessage('MASTER_8')?>
                    </option>
                </select>
            </label>
        </div>
        <div class="service-navigation__item service-navigation__item--site" data-site="">
            <label class="form-input form-input--select">
                <select class="form-input__field">
                    <option value="">
                        <?php echo Loc::getMessage('CHOOSE_REVIEWS')?>
                    </option>
                    <option value=" <?php echo Loc::getMessage('YANDEX_MAPS')?>">
                        <?php echo Loc::getMessage('YANDEX_MAPS')?>
                    </option>
                    <option value="<?php echo Loc::getMessage('GOOGLE_MAPS')?>">
                        <?php echo Loc::getMessage('GOOGLE_MAPS')?>
                    </option>
                    <option value="<?php echo Loc::getMessage('2_GIS')?>">
                        <?php echo Loc::getMessage('2_GIS')?>
                    </option>
                    <option value="<?php echo Loc::getMessage('ZOON')?>">
                        <?php echo Loc::getMessage('ZOON')?>
                    </option>
                    <option value="<?php echo Loc::getMessage('2_GIS')?>">
                        <?php echo Loc::getMessage('YELL')?>

                    </option>
                </select>
            </label>
        </div>
    </div>

</div>
<div class="feedback-item grid page-subsection">


    <?php foreach ($arResult['ITEMS'] as $key => $item) {


        ?>

        <?php if (!empty($item['NAME']) && !empty($item['DISPLAY_PROPERTIES']['TEXT_REVIEWS']['DISPLAY_VALUE'])) {?>
            <div class="feedback-item__slide grid__cell grid__cell--m--6" data-service="<?php echo $item['DISPLAY_PROPERTIES']['SERVICE_NAME']['DISPLAY_VALUE']?>" data-master="<?php echo $item['DISPLAY_PROPERTIES']['MASTER_NAME']['DISPLAY_VALUE'] ?>" data-site="<?php echo $item['DISPLAY_PROPERTIES']['RESOURCE_REV']['DISPLAY_VALUE'] ?>" >
                <div class="feedback-item__header">


                    <p class="feedback-item__info"><?php echo Loc::getMessage('SUB_TITLE')?><a href='<?php echo HTMLToTxt($item['DISPLAY_PROPERTIES']['RESOURCE_REV_LINK']['DISPLAY_VALUE']) ?>' target="_blank"><?php echo $item['DISPLAY_PROPERTIES']['RESOURCE_REV']['DISPLAY_VALUE'] ?></a></p>

                </div>
                <div class="feedback-item__inner">
                    <div class="feedback-item__name"><strong><?=$item['NAME']?></strong></div>
                    <div class="feedback-item__rating-wrapper">
                        <div class="feedback-item__rating rating" data-rating="<?php echo $item['DISPLAY_PROPERTIES']['RATING']['DISPLAY_VALUE'] ?>">
                            <div class="rating__star" ></div>
                            <div class="rating__star" ></div>
                            <div class="rating__star" ></div>
                            <div class="rating__star" ></div>
                            <div class="rating__star"></div>
                        </div>
                        <?php
                        if (!empty($item['DISPLAY_PROPERTIES']['DATE_PUBLISH'])){?>
                            <div class="feedback-item__date"><?php echo $item['DISPLAY_PROPERTIES']['DATE_PUBLISH']['DISPLAY_VALUE'] ?></div>

                        <? } ?>
                    </div>
                    <?php if (!empty($item['DISPLAY_PROPERTIES']['TEXT_REVIEWS'])){?>
                        <div class="feedback-item__text"><?php echo $item['DISPLAY_PROPERTIES']['TEXT_REVIEWS']['DISPLAY_VALUE'] ?></div>

                        <div class="feedback-item__link">
                            <?php if (is_array($item['DISPLAY_PROPERTIES']['SERVICE_LINK']['DISPLAY_VALUE'])){
                                foreach ($item['DISPLAY_PROPERTIES']['SERVICE_LINK']['DISPLAY_VALUE'] as $value){
                                    echo $value;

                                }
                            }else{
                                echo $item['DISPLAY_PROPERTIES']['SERVICE_LINK']['DISPLAY_VALUE'];

                            } ?>
                        </div>



                    <? } ?>

                </div>
            </div>

        <? } ?>
    <? } ?>


</div>
<div class="grid grid--justify--center page-subsection">
    <a class="button grid__cell grid__cell--m--4 grid__cell--s--6 loading-button" href="#">
        <?php echo Loc::getMessage('DOWNLOAD_MORE')?>
    </a>
</div>
