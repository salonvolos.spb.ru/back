<? namespace NewsList;

class Item
{
	public $url;
	public $title;
	public $date;

	public function __construct(array $arItem)
	{
		$this->url = $arItem["DETAIL_PAGE_URL"];
		$this->title = $arItem["NAME"];
		$this->date =$this->getDate($arItem);
	}

	private function getDate($arItem)
	{
		if(!empty($arItem["ACTIVE_FROM"])){
			$date = $arItem["ACTIVE_FROM"];
		}else{
			$date = $arItem["DATE_CREATE"];
		}

		return mb_strtolower(FormatDate("j F Y", MakeTimeStamp($date)), "utf-8");
	}

}
