<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
/** @var array $arResult */
include __DIR__."/code/Item.php";

$items = [];
if ($arResult["ITEMS"]){
    foreach ($arResult["ITEMS"] as $item){
		$items[] = new NewsList\Item($item);
	}
}
include __DIR__."/template/index.php";
