<? /** @var NewsList\Item $item */
?>
<div class="news-item">

    <a
        class="news-item__link"
        href="<?= $item->url ?>"
    ><?= $item->title ?></a>

    <p class="news-item__date"><?= $item->date ?></p>

</div>
