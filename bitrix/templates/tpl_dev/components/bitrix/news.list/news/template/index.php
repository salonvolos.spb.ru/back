<? /** @var array $items */ ?>
<? /** @var array $arResult */

if($items): ?>
    <div class="page-subsection">
        <? foreach ($items as $item): ?>
            <? include __DIR__."/item.php" ?>
        <? endforeach ?>
    </div>
    <?= $arResult["NAV_STRING"] ?>
<? else: ?>
    <div class="page-subsection">
        <p>В разделе пока нет элементов</p>
    </div>
<? endif ?>
