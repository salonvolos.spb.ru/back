<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$pagination = "";
$curTitle = $APPLICATION->GetPageProperty("title");

$isPagination = false;
if(!empty($_GET["PAGEN_1"])){
    $isPagination = true;
    $pagination = " / Страница ".(int)$_GET["PAGEN_1"];
}

// Страница пагинации
if($isPagination){

	$APPLICATION->SetPageProperty("title", $curTitle.$pagination);

	$keywords = "салон красоты новости события акции скидки";
	$APPLICATION->SetPageProperty("keywords", $keywords);

}