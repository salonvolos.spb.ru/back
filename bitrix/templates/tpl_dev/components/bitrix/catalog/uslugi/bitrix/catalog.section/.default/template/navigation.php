<? /** @var Services\Service $service */ ?>
<? if($service->subServices): ?>
    <div class="page-subsection service-header__navigation">
        <? foreach ($service->subServices as $subService): ?>
            <?

            if ($subService['title'] === 'Коррекция наращенных волос в Санкт-Петербурге') {
                $resultStr  = 'Коррекция наращенных волос';

            } elseif ($subService['title'] === 'Капсуляция волос для наращивания в Санкт-Петербурге') {
                $resultStr  = 'Капсуляция волос для наращивания';
            } elseif ($subService['title'] === 'Снятие наращенных волос в Санкт-Петербурге') {
                $resultStr  = 'Снятие наращенных волос';
            } elseif ($subService['title'] === 'Наращивание на короткие волосы в Санкт-Петербурге') {
                $resultStr  = 'Наращивание на короткие волосы';
            } elseif ($subService['title'] === 'Окрашивание волос Airtouch в Санкт-Петербурге') {
                $resultStr  = 'Окрашивание Airtouch';
            } elseif ($subService['title'] === 'Шатуш окрашивание в Санкт-Петербурге') {
                $resultStr  = 'Шатуш';
            } elseif ($subService['title'] === 'Окрашивание волос балаяж в Санкт-Петербурге') {
                $resultStr  = 'Окрашивание балаяж';
            } elseif ($subService['title'] === 'Окрашивание волос в один тон в Санкт-Петербурге') {
                $resultStr  = 'Окрашивание в один тон';
            } elseif ($subService['title'] === 'Стрижки на короткие волосы в Санкт-Петербурге') {
                $resultStr  = 'Стрижки на короткие волосы';
            } elseif ($subService['title'] === 'Модельная стрижка в Санкт-Петербурге') {
                $resultStr  = 'Модельная стрижка';
            } elseif ($subService['title'] === 'Женская стрижка в Санкт-Петербурге') {
                $resultStr  = 'Женская стрижка';
            } elseif ($subService['title'] === 'Стрижка каскад в Санкт-Петербурге') {
                $resultStr  = 'Каскад';
            } elseif ($subService['title'] === 'Стрижка маллет в Санкт-Петербурге') {
                $resultStr  = 'Маллет';
            } elseif ($subService['title'] === 'Стрижка каре в Санкт-Петербурге') {
                $resultStr  = 'Каре';
            } elseif ($subService['title'] === 'Oкpaшивaниe омбре в Санкт-Петербурге') {
                $resultStr  = 'Омбре';
            } elseif ($subService['title'] === 'Стрижка кроп в Санкт-Петербурге') {
                $resultStr  = 'Кроп';
            } elseif ($subService['title'] === 'Стрижка пикси в Санкт-Петербурге') {
                $resultStr  = 'Пикси';
            } elseif ($subService['title'] === 'Мужская стрижка полубокс в Санкт-Петербурге') {
                $resultStr  = 'Мужская стрижка полубокс';
            } elseif ($subService['title'] === 'Стрижка челки в Санкт-Петербурге') {
                $resultStr  = 'Стрижка челки';
            } elseif ($subService['title'] === 'Стрижка шегги в Санкт-Петербурге') {
                $resultStr  = 'Стрижка шегги';
            } elseif ($subService['title'] === 'Стрижка кудрявых') {
                $resultStr  = 'Стрижка кудрявых волос';
            } elseif ($subService['title'] === 'Детские стрижки в Санкт-Петербурге') {
                $resultStr  = 'Детские стрижки';
            } elseif ($subService['title'] === 'Вечерние прически в Санкт-Петербурге') {
                $resultStr  = 'Вечерняя';
            } elseif ($subService['title'] === 'Детские прически в Санкт-Петербурге') {
                $resultStr  = 'Детская';
            } elseif ($subService['title'] === 'Свадебные прически в Санкт-Петербурге') {
                $resultStr  = 'Свадебная';
            } elseif ($subService['title'] === 'Кератиновое выпрямление челки в Санкт-Петербурге') {
                $resultStr  = 'Челки';
            } elseif ($subService['title'] === 'Мужское кератиновое выпрямление волос в Санкт-Петербурге') {
                $resultStr  = 'Мужское';
            } elseif ($subService['title'] === 'Афрокудри в Санкт-Петербурге') {
                $resultStr  = 'Афрокудри';
            } elseif ($subService['title'] === 'Ботокс для волос в Санкт-Петербурге') {
                $resultStr  = 'Ботокс';
            } elseif ($subService['title'] === 'Бразильское выпрямление волос в Санкт-Петербурге') {
                $resultStr  = 'Бразильское';
            }
            else {
                $strReplace = 'волос в Санкт-Петербурге';
                $resultStr = str_replace($strReplace,'', $subService['title']);
            }

            ?>

            <a
                    class="button button--secondary service-header__navigation-item"
                    href="<?= $subService["url"] ?>"
            ><?= $resultStr ?></a>
        <? endforeach ?>
    </div>
<? endif ?>
