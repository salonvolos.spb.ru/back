<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {die();}

/** @var Services\Service $service */

$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	[
		"AREA_FILE_SHOW" => "page",
		"AREA_FILE_SUFFIX" => "appointment",
		"USER_PARAM_ONLINE_CODE" => $service->onlineCode,
		"USER_PARAM_SERVICE_LINK" => $service->url,
	]
);