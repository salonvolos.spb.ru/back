<section class="container page-section page-section--no-padding--top">
	<div class="content">
		<div class="prices-block">
			<div class="prices-block__tables">

				<div class="prices-table">
					<div class="prices-table__row prices-table__row--subheader">
						<div class="prices-table__cell prices-table__cell--title">
							<h2>Услуги и цены</h2>
						</div>
						<div class="prices-table__cell prices-table__cell--wide">До подбородка
							<div class="prices-table__clarification">до 10 см</div>
						</div>
						<div class="prices-table__cell">До плеч
							<div class="prices-table__clarification">до 20 см</div>
						</div>
						<div class="prices-table__cell">Ниже плеч
							<div class="prices-table__clarification">от 20 см</div>
						</div>
					</div>
					<div class="prices-table__row">
						<div class="prices-table__cell prices-table__cell--row-header">Стрижка женская с укладкой
							<div class="prices-table__clarification">В услугу входит мытье головы, укладка феном - придание формы без использования стайлинговых средств. По желанию клиента может быть выбрана любая укладка с 50% скидкой по действующему прейскуранту.</div>
						</div>
						<div class="prices-table__cell">1000-1400 ₽</div>
						<div class="prices-table__cell">1600 ₽</div>
						<div class="prices-table__cell">2000 ₽</div>
					</div>
					<div class="prices-table__row">
						<div class="prices-table__cell prices-table__cell--row-header">Стрижка горячими ножницами (придание формы)
							<div class="prices-table__clarification">В услугу входит мытье головы и укладка феном. Данная услуга предоставляется после первичной (лечебной) стрижки горячими ножницами.</div>
						</div>
						<div class="prices-table__cell"> </div>
						<div class="prices-table__cell">2200 ₽</div>
						<div class="prices-table__cell">2400 ₽</div>
					</div>
				</div>

				<div class="prices-table">
					<div class="prices-table__row prices-table__row--subheader">
						<div class="prices-table__cell prices-table__cell--title">Стрижка горячими ножницами (лечебная)
							<div class="prices-table__clarification">В услугу входит мытье головы и укладка феном. Первичная стрижка горячими ножницами на любую длину волос. (Услуга с накопительным эффектом)</div>
						</div>
						<div class="prices-table__cell">2600 ₽</div>
					</div>
					<div class="prices-table__row prices-table__row--subheader">
						<div class="prices-table__cell prices-table__cell--title">Стрижка мужская</div>
						<div class="prices-table__cell">1000-1400 ₽</div>
					</div>
					<div class="prices-table__row prices-table__row--subheader">
						<div class="prices-table__cell prices-table__cell--title">Стрижка бороды / усов
							<div class="prices-table__clarification">Коррекция формы</div>
						</div>
						<div class="prices-table__cell">500 ₽</div>
					</div>
				</div>

			</div>

			<div class="prices-block__bottom"> <a class="button" href="#">Записаться online</a>
				<div class="prices-block__clarification">При записи вы выбираете день, <br> время и мастера.</div>
				<div class="prices-block__phone phone"> +7 (812) 385-01-51</div><a href="#">Все цены</a>
			</div>

		</div>
	</div>
</section>
