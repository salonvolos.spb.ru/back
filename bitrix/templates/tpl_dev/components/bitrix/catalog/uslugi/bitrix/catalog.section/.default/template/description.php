<?php
/** @var Services\Service $service */

$pages = [
    '/uslugi/narashchivanie-volos/kapsuljatsija/',
    '/uslugi/narashchivanie-volos/korrekciya/',
    '/uslugi/narashchivanie-volos/kapsulnoe/',
    '/uslugi/narashchivanie-volos/gollivudskoe/',
    '/uslugi/narashchivanie-volos/snyatie/',
    '/uslugi/narashchivanie-volos/lentochnoe/',
    '/uslugi/narashchivanie-volos/holodnoe/',
    '/uslugi/narashchivanie-volos/na-korotkie/',
    '/uslugi/narashchivanie-volos/muzhskoe/',
    '/uslugi/okrashivanie-volos/dekapirovanie/',
    '/uslugi/okrashivanie-volos/brondirovanie/',
    '/uslugi/okrashivanie-volos/melirovaniye/',
    '/uslugi/okrashivanie-volos/v-odin-ton/',
    '/uslugi/okrashivanie-volos/skrytoe/',
    '/uslugi/okrashivanie-volos/muzhskoe-okrashivanie/',
];
?>
<?php
if (in_array($APPLICATION->GetCurDir(), $pages) === true) { ?>
    <section class="container page-section page-section--no-padding--top page-section--no-padding--bottom">
        <div class="content">
            <?php
            echo $service->description ?>
        </div>
    </section>
    <?php
} else { ?>
    <section class="container page-section page-section--no-padding--top">
        <div class="content">
            <?php
            echo $service->description ?>
        </div>
    </section>
    <?php
} ?>

