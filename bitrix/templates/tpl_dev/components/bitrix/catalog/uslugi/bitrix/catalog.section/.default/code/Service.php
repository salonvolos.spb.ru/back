<? namespace Services;

class Service
{
	public $title;
	public $picture;
	public $breadcrumbs;
	public $breadcrumbsJson;
	public $subServices;
	public $description;
	public $actions;
	public $works;
	public $isEmbeddedActions;
	public $isEmbeddedWorks;
	public $id;
	public $url;
	public $filter;
	public $filterWorks;
	public $onlineCode;
	public $feedbacksText;
	public $feedbacksVideo;

	public function __construct($arResult)
	{
		$this->id = $arResult["ID"];
		$this->url = $arResult["SECTION_PAGE_URL"];
		$this->filter = ["PROPERTY_SERVICE" => $this->id];
		$this->filterWorks = array_merge($this->filter, ["PROPERTY_IS_ON_SERVICE_PAGE" => "Y"]);
        // $this->filterWorks = ["PROPERTY_IS_ON_SERVICE_PAGE" => "Y"];


        $this->actions = $this->getActions();
		$this->works = $this->getWorks();
		$this->isEmbeddedActions = strpos($arResult["DESCRIPTION"], "#INC_AKCII#") !== false;
		$this->isEmbeddedWorks = strpos($arResult["DESCRIPTION"], "#INC_OUR_WORKS#") !== false;

		$this->title = $arResult["NAME"];
		$this->picture = !empty($arResult["PICTURE"]) ? $arResult["PICTURE"]["SRC"] : null;
		$this->breadcrumbs = $this->getBreadcrumbs($arResult["ID"]);
		$this->breadcrumbsJson = $this->getBreadcrumbsJson($this->breadcrumbs);
		$this->subServices = $this->getSubServices(ID_IBLOCK_SERVICES, $arResult["ID"], $arResult["DEPTH_LEVEL"]);
		$this->description = $this->getDescription($arResult["DESCRIPTION"]);
		$this->onlineCode = getSectionPropertyValue("UF_ONLINE_CODE", ID_IBLOCK_SERVICES, $this->id);

		$this->getFeedbacks();
	}

	private function getBreadcrumbs($sectionId)
	{
		$res = \CIBlockSection::GetNavChain(ID_IBLOCK_SERVICES, $sectionId);

		$breadcrumbs = ["Главная" => "/"];

		while($nav = $res->GetNext()){
			$breadcrumbs[$nav["NAME"]] = $nav["SECTION_PAGE_URL"];
		}

		return $breadcrumbs;
	}

	private function getBreadcrumbsJson($breadcrumbs)
	{
		$items = [];

		$position = 1;
		foreach ($breadcrumbs as $title => $url){
			$items[] = [
				"@type" => "ListItem",
				"position" => $position,
				"item" => ["@id" => $url, "name" => $title]
			];
			$position++;
		}

		return json_encode($items);
	}

	private function getSubServices($iBlockId, $serviceId, $serviceDepthLevel)
	{
		$subServices = getSubSectionsBySectionId($iBlockId, $serviceId, $serviceDepthLevel);

		if($subServices){
			foreach ($subServices as $k => $subService){
				$subServices[$k] = [
					"url" => $subService["SECTION_PAGE_URL"],
					"title" => $subService["NAME"],
				];
			}
		}

		return $subServices;
	}

	private function getDescription($description)
	{
		if(strpos($description, "#INC_") === false){
			return $description;
		}

		if(strpos($description, "#INC_AKCII#") !== false){
			$description = str_replace("#INC_AKCII#", $this->actions, $description);
		}

		if (strpos($description, "#INC_EXTENSION_WITHOUT_KNOWLEDGE_FORM#") !== false) {
			$formExtWoKnowledge = file_get_contents(__DIR__ . "/../template/extension/extension_without_knowledge.php");
			$description = str_replace("#INC_EXTENSION_WITHOUT_KNOWLEDGE_FORM#", $formExtWoKnowledge, $description);
		}

		if (strpos($description, "#INC_EXTENSION_WITH_KNOWLEDGE_FORM#") !== false) {
			$formExtWKnowledge = file_get_contents(__DIR__ . "/../template/extension/extension_with_knowledge.php");
			$description = str_replace("#INC_EXTENSION_WITH_KNOWLEDGE_FORM#", $formExtWKnowledge, $description);
		}

		if (strpos($description, "#INC_CALLBACK_FORM#") !== false) {
			$formCallback = file_get_contents(__DIR__ . "/../template/extension/callback.php");
			$description = str_replace("#INC_CALLBACK_FORM#", $formCallback, $description);
		}

		if (strpos($description, "#INC_OUR_WORKS#") !== false) {
			$description = str_replace("#INC_OUR_WORKS#", $this->works, $description);
		}

		if (strpos($description, "#INC_CORRECTION_FORM#") !== false) {
			$formCorrection = file_get_contents(__DIR__ . "/../template/extension/correction.php");
			$description = str_replace("#INC_CORRECTION_FORM#", $formCorrection, $description);
		}

		if (strpos($description, "#INC_REMOVAL_FORM#") !== false) {
			$formRemoval = file_get_contents(__DIR__ . "/../template/extension/removal.php");
			$description = str_replace("#INC_REMOVAL_FORM#", $formRemoval, $description);
		}

		return $description;
	}

	private function getActions()
	{
		ob_start();
		include __DIR__."/actions.php";
		$content = ob_get_contents();
		ob_end_clean();

		return $content;
	}

	private function getWorks()
	{
		ob_start();
		include __DIR__."/our_works.php";
		$content = ob_get_contents();
		ob_end_clean();

		return $content;
	}

	private function getFeedbacks()
	{
		$arOrder = ["CREATED_DATE" => "DESC", "SORT" => "ASC"];
		$arFilter = [
			"IBLOCK_ID" => ID_IBLOCK_FEEDBACKS,
			"PROPERTY_SERVICE" => $this->id,
			"ACTIVE" => "Y",
			"GLOBAL_ACTIVE" => "Y",
		];
		$arSelect = ["ID", "IBLOCK_ID", "NAME", "PREVIEW_TEXT", "PROPERTY_IS_VIDEO"];

		$res = \CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);

		while($obj = $res->GetNextElement()) {
			$props = $obj->GetProperties();
			if($props["IS_VIDEO"]["VALUE"] != "Y"){
				$this->feedbacksText[] = $obj->fields["PREVIEW_TEXT"];
			}else{
				$this->feedbacksVideo[] = $obj->fields["~PREVIEW_TEXT"];
			}

		}

		return false;
	}

}
