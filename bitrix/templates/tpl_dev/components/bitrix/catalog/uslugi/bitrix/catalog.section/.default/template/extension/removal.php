<div class="grid__cell grid__cell--m--6 grid__cell--xs--12">
	<div class="grid"> 
    <div class="grid__cell grid__cell--xs--12">
			<h3>Снятие нарощенных волос</h3>
			<p>Подойдет, если вы хотите дать волосам отдохнуть или поменять нарощенные волоса на новые.</p>
		</div>

    <div class="grid__cell grid__cell--xs--12">
			<form
					class="
							hair-extention-calculator
							hair-extention-calculator--rows
							form-calc-cost-extension
							validated-form
							submitJS
							par
					"
					method="post"
			>
					<div class="hair-extention-calculator__item">
							<label class="hair-extention-calculator__label">Вид наращивания</label>
							<div class="hair-extention-calculator__input">
									<select
											class="hair-extention-calculator__select"
											name="extension_type"
									>
											<option value="kapsulnoe" selected>капсульное</option>
											<option value="lentochnoe">ленточное</option>
									</select>
							</div>
					</div>

					<div class="hair-extention-calculator__item">
							<label class="hair-extention-calculator__label">Кол-во</label>
							<div class="hair-extention-calculator__input">
									<input
											type="text"
											name="ready_qty_from_user"
									>
									<div class="hair-extention-calculator__description">капсул</div>
							</div>
					</div>

					<div class="hair-extention-calculator__item calculator__item_works">
							<label class="hair-extention-calculator__label">Стоимость</label>
							<div class="hair-extention-calculator__output">
									<output>0</output>
									<div class="hair-extention-calculator__description">руб.</div>
							</div>
					</div>

					<input type="hidden" name="name_of_service" value="removal">

			</form>
		</div>
	</div>

</div>
