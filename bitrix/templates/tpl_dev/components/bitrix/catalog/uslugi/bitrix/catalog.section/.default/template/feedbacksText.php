<? /** @var Services\Service $service */ ?>
<? if($service->feedbacksText): ?>
<section class="container page-section page-section--no-padding--top" id="page-feedback">
	<div class="content">

		<h2 class="not-menu-content">Отзывы клиенток</h2>

		<div class="page-subsection">
			<div class="slider feedback-item">
				<div class="slider__slides swiper">
					<div class="slider__wrapper swiper-wrapper">

						<? foreach ($service->feedbacksText as $feedback): ?>
						<div class="slide swiper-slide feedback-item__slide">
							<?= $feedback ?>
						</div>
						<? endforeach ?>

					</div>
					<div class="slider__arrow slider__arrow--prev"></div>
					<div class="slider__arrow slider__arrow--next"></div>
					<div class="slider__pagination"></div>

				</div>
			</div>
		</div>

	</div>
</section>
<? endif ?>
