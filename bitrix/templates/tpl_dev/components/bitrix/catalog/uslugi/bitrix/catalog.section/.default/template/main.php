<?php
use \Bitrix\Main\Localization\Loc;
/** @var Services\Service $service */

Loc::loadLanguageFile(__FILE__);

$titleServiceHeaderBtn = '';

/* Заполняй если надо изменить заголовок в баннере */
$titleServiceHeader = '';

if ($_SERVER['REQUEST_URI'] == '/uslugi/okrashivanie-volos/') {
    $titleServiceHeaderBtn = 'окрашиванию';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/narashchivanie-volos/lentochnoe/') {
    $titleServiceHeaderBtn = 'ленточному наращиванию';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/strizhka/') {
    $titleServiceHeaderBtn = 'стрижке';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/strizhka/zhenskaya-strizhka/') {
    $titleServiceHeaderBtn = 'женской стрижке';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/strizhka/modelnaya/') {
    $titleServiceHeaderBtn = 'модельной стрижке';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/strizhka/muzhskaya-strizhka/') {
    $titleServiceHeaderBtn = 'мужской стрижке';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/strizhka/goryachimi-nozhnicami/') {
    $titleServiceHeaderBtn = 'стрижке горячими ножницами';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/strizhka/kare/') {
    $titleServiceHeaderBtn = 'стрижке каре';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/strizhka/kaskad/') {
    $titleServiceHeaderBtn = 'стрижке каскад';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/strizhka/mallet/') {
    $titleServiceHeaderBtn = 'стрижке маллет';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/strizhka/korotkaya/') {
    $titleServiceHeaderBtn = 'стрижке на короткие волосы';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/pricheska/') {
    $titleServiceHeaderBtn = 'причёске';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/okrashivanie-volos/ombre/') {
    $titleServiceHeaderBtn = 'окрашиванию омбре';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/okrashivanie-volos/blondirovanie/') {
    $titleServiceHeaderBtn = 'блондированию';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/okrashivanie-volos/brondirovanie/') {
    $titleServiceHeaderBtn = 'брондированию';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/okrashivanie-volos/dekapirovanie/') {
    $titleServiceHeaderBtn = 'декапированию';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/okrashivanie-volos/kaliforniyskoe/') {
    $titleServiceHeaderBtn = 'калифорнийскому мелированию';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/okrashivanie-volos/kolorirovanie/') {
    $titleServiceHeaderBtn = 'колорированию';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/okrashivanie-volos/konturing/') {
    $titleServiceHeaderBtn = 'контурингу';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/okrashivanie-volos/melirovaniye/') {
    $titleServiceHeaderBtn = 'мелированию';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/okrashivanie-volos/muzhskoe-okrashivanie/') {
    $titleServiceHeaderBtn = 'мужскому окрашиванию';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/okrashivanie-volos/airtouch/') {
    $titleServiceHeaderBtn = 'окрашиванию Airtouch';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/okrashivanie-volos/balayazh/') {
    $titleServiceHeaderBtn = 'окрашиванию балаяж';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/okrashivanie-volos/v-odin-ton/') {
    $titleServiceHeaderBtn = 'окрашиванию в один тон';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/okrashivanie-volos/skrytoe/') {
    $titleServiceHeaderBtn = 'скрытому окрашиванию';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/okrashivanie-volos/slozhnoe/') {
    $titleServiceHeaderBtn = 'сложному окрашиванию';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/okrashivanie-volos/tonirovanie/') {
    $titleServiceHeaderBtn = 'тонированию';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/okrashivanie-volos/shatush/') {
    $titleServiceHeaderBtn = 'окрашиванию шатуш';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/okrashivanie-volos/czvetnoe/') {
    $titleServiceHeaderBtn = 'цветному окрашиванию';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/narashchivanie-volos/') {
    $titleServiceHeaderBtn = 'наращиванию';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/narashchivanie-volos/gollivudskoe/') {
    $titleServiceHeaderBtn = 'голливудскому наращиванию';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/narashchivanie-volos/kapsulnoe/') {
    $titleServiceHeaderBtn = 'капсульному наращиванию';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/narashchivanie-volos/kapsuljatsija/') {
    $titleServiceHeaderBtn = 'капсуляции';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/narashchivanie-volos/korrekciya/') {
    $titleServiceHeaderBtn = 'коррекции';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/narashchivanie-volos/obuchenie/') {
    $titleServiceHeaderBtn = 'курсам по наращиванию';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/narashchivanie-volos/muzhskoe/') {
    $titleServiceHeaderBtn = 'мужскому наращиванию';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/narashchivanie-volos/na-korotkie/') {
    $titleServiceHeaderBtn = 'наращиванию на короткие волосы';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/narashchivanie-volos/snyatie/') {
    $titleServiceHeaderBtn = 'снятию';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/narashchivanie-volos/holodnoe/') {
    $titleServiceHeaderBtn = 'холодному наращиванию';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/narashchivanie-volos/tsvetnoe/') {
    $titleServiceHeaderBtn = 'цветному наращиванию';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/uhod/botoks-dlya-volos/') {
    $titleServiceHeaderBtn = 'ботоксу';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/uhod/nanoplastika/') {
    $titleServiceHeaderBtn = 'нанопластике';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/ukladka/') {
    $titleServiceHeaderBtn = 'укладке';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/zavivka-volos/') {
    $titleServiceHeaderBtn = 'завивке';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/strizhka/crop/') {
    $titleServiceHeaderBtn = 'стрижке кроп';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/strizhka/konchikov-volos/') {
    $titleServiceHeaderBtn = 'стрижке кончиков';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/strizhka/piksi/') {
    $titleServiceHeaderBtn = 'стрижке пикси';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/strizhka/poluboks/') {
    $titleServiceHeaderBtn = 'мужской стрижке полубокс';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/strizhka/chelki/') {
    $titleServiceHeaderBtn = 'стрижке челки';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/strizhka/sheggi/') {
    $titleServiceHeaderBtn = 'стрижке шегги';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/strizhka/kudryavykh-volos/') {
    $titleServiceHeaderBtn = 'стрижке кудрявых волос';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/strizhka/detskie/') {
    $titleServiceHeaderBtn = 'детской стрижке';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/pricheska/vechernyaya/') {
    $titleServiceHeaderBtn = 'вечерней прическе';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/pricheska/detskaya/') {
    $titleServiceHeaderBtn = 'детской прическе';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/pricheska/svadebnaja/') {
    $titleServiceHeaderBtn = 'свадебной прическе';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/vypryamlenie-volos/chelka/') {
    $titleServiceHeaderBtn = 'кератиновому выпрямлению челки';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/vypryamlenie-volos/muzhskoye/') {
    $titleServiceHeaderBtn = 'мужскому кератиновому выпрямлению';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/pricheska/afrokudri/') {
    $titleServiceHeaderBtn = 'прическе афрокудри';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/uhod/laminirovanie-volos/') {
    $titleServiceHeaderBtn = 'ламинированию волос';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/vypryamlenie-volos/brazilskoe/') {
    $titleServiceHeaderBtn = 'бразильскому выпрямлению волос';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/pricheska/pletenie-kos/') {
    $titleServiceHeaderBtn = 'плетению кос';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/uhod/') {
    $titleServiceHeaderBtn = 'уходу и лечению';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/vypryamlenie-volos/') {
    $titleServiceHeaderBtn = 'кератиновому выпрямлению';
} elseif ($_SERVER['REQUEST_URI'] == '/uslugi/makiyazh/') {
    $titleServiceHeaderBtn = 'макияжу';
}

?>
<section class="container page-section service-header">
    <div class="content">

        <div class="page-subsection service-header__banner">

            <h1 class="service-header__title">
                <?php
                if (!empty($titleServiceHeader)) {
                    echo $titleServiceHeader;
                } else {
                    echo $service->title;
                }
                ?>
            </h1>
            <ul class="service-header__list">
                <li class="service-header__subtitle">
                    <a class="button" href="#" onclick="Comagic.openSitePhonePanel()">
                        <?php
                        if (!empty($titleServiceHeaderBtn)) {
                            echo Loc::getMessage('SIGN_UP_FOR') . mb_strtolower($titleServiceHeaderBtn);
                        } else {
                            echo Loc::getMessage('SIGN_UP_FOR') . mb_strtolower($service->title);
                        } ?>
                    </a>
                </li>
                <li class="service-header__subtitle"><a class="button button--secondary" href="#page-prices"> <?php echo Loc::getMessage('COST_OF_SERVICES')?> </a></li>
            </ul>

            <? /* <ul class="service-header__list">

                <li class="service-header__subtitle">
                    <a href="#page-prices">Цены</a>
                </li>

                <? if($service->feedbacksText): ?>
                    <li class="service-header__subtitle">
                        <a href="#page-feedback">Отзывы</a>
                    </li>
				<? endif ?>

				<? if($service->works): ?>
                    <li class="service-header__subtitle">
                        <a href="#page-works">Примеры работ</a>
                    </li>
				<? endif ?>

            </ul> */?>


            <? include __DIR__."/picture.php" ?>

        </div>

        <? include __DIR__."/breadcrumbs.php" ?>
        <? include __DIR__."/navigation.php" ?>

    </div>
</section>
