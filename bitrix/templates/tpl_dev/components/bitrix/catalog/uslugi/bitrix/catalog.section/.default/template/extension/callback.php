<div class="page-subsection block-marked block-marked--beige text-align-center consultation-item">
  <h2>Сложно рассчитать стоимость самостоятельно?</h2>
  <p>Оставьте номер телефона, наш мастер перезвонит и поможет рассчитать стоимость услуги наращивания волос.</p>
  <a class="button consultation-item__button" href="#" onclick="event.preventDefault(); Comagic.openSitePhonePanel()">Проконсультироваться</a>
</div>
