<?php /** @var Services\Service $service */ ?>
<div class="breadcrumbs page-subsection service-header__breadcrumbs">
    <?php
    $statusBreadcrumbs = false;
    $hairCutDirMan = '/uslugi/strizhka/muzhskaya-strizhka/';
    $hairCutDirWoman = '/uslugi/strizhka/zhenskaya-strizhka/';
    $hairCutDirCare = '/uslugi/strizhka/kare/';
    $hairCutDirModel = '/uslugi/strizhka/modelnaya/';
    $hairCutDirkaskad = '/uslugi/strizhka/kaskad/';
    $hairCutDirNozhnicami = '/uslugi/strizhka/goryachimi-nozhnicami/';
    $hairCutDirMallet = '/uslugi/strizhka/mallet/';
    $hairCutDirKorotkie = '/uslugi/strizhka/korotkaya/';
    $hairCutDirCrop = '/uslugi/strizhka/crop/';
    $hairCutDirKonchikov = '/uslugi/strizhka/konchikov-volos/';
    $hairCutDirPiksi = '/uslugi/strizhka/piksi/';
    $hairCutDirChelki = '/uslugi/strizhka/chelki/';
    $hairCutDirSheggy = '/uslugi/strizhka/sheggi/';
    $hairCutDirKudryavykh = '/uslugi/strizhka/kudryavykh-volos/';
    $hairCutDirDetskie = '/uslugi/strizhka/detskie/';


    $coloringDekapirovanie = '/uslugi/okrashivanie-volos/dekapirovanie/';
    $coloringKolorirovanie = '/uslugi/okrashivanie-volos/kolorirovanie/';
    $coloringKaliforniyskoe = '/uslugi/okrashivanie-volos/kaliforniyskoe/';
    $coloringBrondirovanie = '/uslugi/okrashivanie-volos/brondirovanie/';
    $coloringKonturing = '/uslugi/okrashivanie-volos/konturing/';
    $coloringAirtouch = '/uslugi/okrashivanie-volos/airtouch/';
    $coloringShatush = '/uslugi/okrashivanie-volos/shatush/';
    $coloringBlondirovanie = '/uslugi/okrashivanie-volos/blondirovanie/';
    $coloringMelirovaniye = '/uslugi/okrashivanie-volos/melirovaniye/';
    $coloringBalayazh = '/uslugi/okrashivanie-volos/balayazh/';
    $coloringTon = '/uslugi/okrashivanie-volos/v-odin-ton/';
    $coloringSkrytoe = '/uslugi/okrashivanie-volos/skrytoe/';
    $coloringMuzhskoe = '/uslugi/okrashivanie-volos/muzhskoe-okrashivanie/';
    $coloringTonirovanie = '/uslugi/okrashivanie-volos/tonirovanie/';
    $coloringCzvetnoe = '/uslugi/okrashivanie-volos/czvetnoe/';
    $coloringOmbre = '/uslugi/okrashivanie-volos/ombre/';
    $coloringSlozhnoe = '/uslugi/okrashivanie-volos/slozhnoe/';


    $hairExtensionGollivudskoe = '/uslugi/narashchivanie-volos/gollivudskoe/';
    $hairExtensionKapsulnoe = '/uslugi/narashchivanie-volos/kapsulnoe/';
    $hairExtensionKorrekciya = '/uslugi/narashchivanie-volos/korrekciya/';
    $hairExtensionKapsuljatsija = '/uslugi/narashchivanie-volos/kapsuljatsija/';
    $hairExtensiondSnyatie = '/uslugi/narashchivanie-volos/snyatie/';
    $hairExtensionLentochnoe = '/uslugi/narashchivanie-volos/lentochnoe/';
    $hairExtensionHolodnoe = '/uslugi/narashchivanie-volos/holodnoe/';
    $hairExtensionTsvetnoe = '/uslugi/narashchivanie-volos/tsvetnoe/';
    $hairExtensionqObuchenie = '/uslugi/narashchivanie-volos/obuchenie/';
    $hairExtensionKorotkie = '/uslugi/narashchivanie-volos/na-korotkie/';
    $hairExtensionMuzhskoe = '/uslugi/narashchivanie-volos/muzhskoe/';
    $hairExtensionMuzhskoePoluboks = '/uslugi/strizhka/poluboks/';
    

    $botoxPage = '/uslugi/uhod/botoks-dlya-volos/';
    $hairLamination = '/uslugi/uhod/laminirovanie-volos/';
    $hairNanoplasty = '/uslugi/uhod/nanoplastika/';

    $hairstyleVechernyaya = '/uslugi/pricheska/vechernyaya/';
    $hairstyleDetskaya = '/uslugi/pricheska/detskaya/';
    $hairstyleSvadebnaja = '/uslugi/pricheska/svadebnaja/';
    $hairstyleAfrokudri = '/uslugi/pricheska/afrokudri/';

    $hairStraighteningChelki = '/uslugi/vypryamlenie-volos/chelka/';
    $hairStraighteningMuzhskoe = '/uslugi/vypryamlenie-volos/muzhskoye/';
    $hairStraighteningBrazilskoe = '/uslugi/vypryamlenie-volos/brazilskoe/';

    $curDir = $APPLICATION->GetCurDir();
    if ($curDir === $botoxPage) {
        $statusBreadcrumbs = true;
        $APPLICATION->SetPageProperty('breadcrumbs', 'Ботокс');

    }

    if ($curDir === $hairCutDirMan) {
        $statusBreadcrumbs = true;
        $APPLICATION->SetPageProperty('breadcrumbs', 'Мужская');

    }
    if ($curDir === $hairCutDirNozhnicami) {
        $statusBreadcrumbs = true;
        $APPLICATION->SetPageProperty('breadcrumbs', 'Горячими ножницами');

    }

    if ($curDir === $hairCutDirkaskad) {
        $statusBreadcrumbs = true;
        $APPLICATION->SetPageProperty('breadcrumbs', 'Каскад');

    }
    if ($curDir === $hairCutDirCare) {
        $statusBreadcrumbs = true;
        $APPLICATION->SetPageProperty('breadcrumbs', 'Каре');

    }

    if ($curDir === $hairCutDirModel) {
        $statusBreadcrumbs = true;
        $APPLICATION->SetPageProperty('breadcrumbs', 'Модельная');

    }
    if ($curDir === $hairCutDirKorotkie) {
        $statusBreadcrumbs = true;
        $APPLICATION->SetPageProperty('breadcrumbs', 'На короткие волосы');

    }


    if ($curDir === $hairCutDirWoman) {
        $statusBreadcrumbs = true;
        $APPLICATION->SetPageProperty('breadcrumbs', 'Женская');

    }
    if ($curDir === $hairCutDirMallet) {
        $statusBreadcrumbs = true;
        $APPLICATION->SetPageProperty('breadcrumbs', 'Маллет');

    }


    if ($curDir === $coloringDekapirovanie) {
        $statusBreadcrumbs = true;
        $APPLICATION->SetPageProperty('breadcrumbs', 'Декапирование');

    }

    if ($curDir === $coloringKolorirovanie) {
        $statusBreadcrumbs = true;
        $APPLICATION->SetPageProperty('breadcrumbs', 'Колорирование');

    }

    if ($curDir === $coloringKaliforniyskoe) {
        $statusBreadcrumbs = true;
        $APPLICATION->SetPageProperty('breadcrumbs', 'Калифорнийское мелирование');

    }


    if ($curDir === $coloringBrondirovanie) {
        $statusBreadcrumbs = true;
        $APPLICATION->SetPageProperty('breadcrumbs', 'Бродирование');

    }

    if ($curDir === $coloringKonturing) {
        $statusBreadcrumbs = true;
        $APPLICATION->SetPageProperty('breadcrumbs', 'Контуринг');

    }
    if ($curDir === $coloringAirtouch) {
        $statusBreadcrumbs = true;

        $APPLICATION->SetPageProperty('breadcrumbs', 'Airtouch');

    }

    if ($curDir === $coloringShatush) {
        $statusBreadcrumbs = true;

        $APPLICATION->SetPageProperty('breadcrumbs', 'Шатуш');

    }
    if ($curDir === $coloringBlondirovanie) {
        $statusBreadcrumbs = true;

        $APPLICATION->SetPageProperty('breadcrumbs', 'Блондирование');

    }
    if ($curDir === $coloringMelirovaniye) {
        $statusBreadcrumbs = true;

        $APPLICATION->SetPageProperty('breadcrumbs', 'Мелирование');

    }

    if ($curDir === $coloringBalayazh) {
        $statusBreadcrumbs = true;

        $APPLICATION->SetPageProperty('breadcrumbs', 'Балаяж');

    }

    if ($curDir === $coloringTon) {
        $statusBreadcrumbs = true;

        $APPLICATION->SetPageProperty('breadcrumbs', 'В один тон');

    }
    if ($curDir === $coloringSkrytoe) {
        $statusBreadcrumbs = true;

        $APPLICATION->SetPageProperty('breadcrumbs', 'Скрытое');

    }

    if ($curDir === $coloringMuzhskoe) {
        $statusBreadcrumbs = true;

        $APPLICATION->SetPageProperty('breadcrumbs', 'Мужское');

    }

    if ($curDir === $coloringTonirovanie) {
        $statusBreadcrumbs = true;

        $APPLICATION->SetPageProperty('breadcrumbs', 'Тонирование');

    }

    if ($curDir === $coloringCzvetnoe) {
        $statusBreadcrumbs = true;

        $APPLICATION->SetPageProperty('breadcrumbs', 'Цветное');

    }
    if ($curDir === $coloringOmbre) {
        $statusBreadcrumbs = true;

        $APPLICATION->SetPageProperty('breadcrumbs', 'Омбре');

    }

    if ($curDir === $coloringSlozhnoe) {
        $statusBreadcrumbs = true;

        $APPLICATION->SetPageProperty('breadcrumbs', 'Сложное');

    }


    if ($curDir === $hairExtensionGollivudskoe) {
        $statusBreadcrumbs = true;

        $APPLICATION->SetPageProperty('breadcrumbs', 'Голливудское');

    }

    if ($curDir === $hairExtensionKorrekciya) {
        $statusBreadcrumbs = true;

        $APPLICATION->SetPageProperty('breadcrumbs', 'Коррекция');

    }

    if ($curDir === $hairExtensionKapsulnoe) {
        $statusBreadcrumbs = true;

        $APPLICATION->SetPageProperty('breadcrumbs', 'Капсульное');

    }

    if ($curDir === $hairExtensionKapsuljatsija) {
        $statusBreadcrumbs = true;

        $APPLICATION->SetPageProperty('breadcrumbs', 'Капсуляция');

    }

    if ($curDir === $hairExtensionqObuchenie) {
        $statusBreadcrumbs = true;

        $APPLICATION->SetPageProperty('breadcrumbs', 'Курсы по наращиванию');

    }

    if ($curDir === $hairExtensionHolodnoe) {
        $statusBreadcrumbs = true;

        $APPLICATION->SetPageProperty('breadcrumbs', 'Холодное');

    }

    if ($curDir === $hairExtensionMuzhskoe) {
        $statusBreadcrumbs = true;

        $APPLICATION->SetPageProperty('breadcrumbs', 'Мужское');

    }

    if ($curDir === $hairExtensionTsvetnoe) {
        $statusBreadcrumbs = true;

        $APPLICATION->SetPageProperty('breadcrumbs', 'Цветное');

    }

    if ($curDir === $hairExtensiondSnyatie) {
        $statusBreadcrumbs = true;

        $APPLICATION->SetPageProperty('breadcrumbs', 'Снятие');

    }

    if ($curDir === $hairExtensionLentochnoe) {
        $statusBreadcrumbs = true;

        $APPLICATION->SetPageProperty('breadcrumbs', 'Ленточное');

    }
    if ($curDir === $hairExtensionKorotkie) {
        $statusBreadcrumbs = true;

        $APPLICATION->SetPageProperty('breadcrumbs', 'На короткие');

    }
    if ($curDir === $hairCutDirCrop) {
        $statusBreadcrumbs = true;

        $APPLICATION->SetPageProperty('breadcrumbs', 'Кроп');

    }
    if ($curDir === $hairCutDirKonchikov) {
        $statusBreadcrumbs = true;

        $APPLICATION->SetPageProperty('breadcrumbs', 'Кончиков');

    }
    if ($curDir === $hairCutDirPiksi) {
        $statusBreadcrumbs = true;

        $APPLICATION->SetPageProperty('breadcrumbs', 'Пикси');

    }
    if ($curDir === $hairExtensionMuzhskoePoluboks) {
        $statusBreadcrumbs = true;

        $APPLICATION->SetPageProperty('breadcrumbs', 'Полубокс');

    }
    if ($curDir === $hairCutDirChelki) {
        $statusBreadcrumbs = true;

        $APPLICATION->SetPageProperty('breadcrumbs', 'Челки');

    }
    if ($curDir === $hairCutDirSheggy) {
        $statusBreadcrumbs = true;

        $APPLICATION->SetPageProperty('breadcrumbs', 'Шегги');

    }
    if ($curDir === $hairCutDirKudryavykh) {
        $statusBreadcrumbs = true;

        $APPLICATION->SetPageProperty('breadcrumbs', 'Кудрявых волос');

    }
    if ($curDir === $hairCutDirDetskie) {
        $statusBreadcrumbs = true;

        $APPLICATION->SetPageProperty('breadcrumbs', 'Детские стрижки');

    }
    if ($curDir === $hairstyleVechernyaya) {
        $statusBreadcrumbs = true;

        $APPLICATION->SetPageProperty('breadcrumbs', 'Вечерняя');

    }
    if ($curDir === $hairstyleDetskaya) {
        $statusBreadcrumbs = true;

        $APPLICATION->SetPageProperty('breadcrumbs', 'Детская');

    }
    if ($curDir === $hairstyleSvadebnaja) {
        $statusBreadcrumbs = true;

        $APPLICATION->SetPageProperty('breadcrumbs', 'Свадебная');

    }
    if ($curDir === $hairstyleAfrokudri) {
        $statusBreadcrumbs = true;

        $APPLICATION->SetPageProperty('breadcrumbs', 'Афрокудри');

    }
    if ($curDir === $hairStraighteningChelki) {
        $statusBreadcrumbs = true;

        $APPLICATION->SetPageProperty('breadcrumbs', 'Челки');

    }
    if ($curDir === $hairStraighteningMuzhskoe) {
        $statusBreadcrumbs = true;

        $APPLICATION->SetPageProperty('breadcrumbs', 'Мужское');

    }
    if ($curDir === $hairLamination) {
        $statusBreadcrumbs = true;

        $APPLICATION->SetPageProperty('breadcrumbs', 'Ламинирование');

    }
    if ($curDir === $hairNanoplasty) {
        $statusBreadcrumbs = true;

        $APPLICATION->SetPageProperty('breadcrumbs', 'Нанопластика');

    }
    if ($curDir === $hairStraighteningBrazilskoe) {
        $statusBreadcrumbs = true;

        $APPLICATION->SetPageProperty('breadcrumbs', 'Бразильское');

    }

    foreach ($service->breadcrumbs as $title => $url) {
        if ($statusBreadcrumbs === false) {
            if ($title != $service->title) {
                ?>
                <div class="breadcrumbs__parent">
                    <a class="link-secondary" href="<?php echo $url ?>"><?php echo $title ?></a>
                </div>
                <div class="breadcrumbs__separator"></div>
                <?php
            } else { ?>

                <div class="breadcrumbs__current"><?php echo $title?></div>
                <?php
            }
        }else{
            if ($title != $service->title) {
                ?>
                <div class="breadcrumbs__parent">
                    <a class="link-secondary" href="<?php echo $url ?>"><?php echo $title ?></a>
                </div>
                <div class="breadcrumbs__separator"></div>
                <?php
            } else { ?>

                <div class="breadcrumbs__current"><?php $APPLICATION->ShowProperty('breadcrumbs');?></div>
                <?php
            }
        }

    }
    ?>

    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "BreadcrumbList",
            "itemListElement": <?php echo $service->breadcrumbsJson ?>
        }
    </script>
</div>
