<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {die();} ?>
<div class="page-subsection" id="page-prices">

    <h2>Как рассчитать стоимость?</h2>
    <p>Для расчета стоимости используйте наши калькуляторы:</p>

    <div class="grid par">
        <? include __DIR__."/extension_without_knowledge.php" ?>
        <? include __DIR__."/extension_with_knowledge.php" ?>
    </div>

</div>



<div class="page-block">

    <h2 class="page-subheader">Снятие и коррекция</h2>

    <div class="block-wrap  block-wrap_wrap ">
        <? include __DIR__."/correction.php" ?>
        <? include __DIR__."/removal.php" ?>
    </div>

    <p>Распутывание колтунов - <strong>1000 - 3000 руб.</strong></p>
    <p>В случае если во время носки у вас образовались колтуны и нам необходимо будет их распутать, то эта услуга оплачивается дополнительно в зависимости от сложности ситуации.</p>
</div>



