<? /** @var Services\Service $service */

$APPLICATION->IncludeComponent(
	"kmedia:infoblocktree",
	"service_page",
	array(
		// фильтр: только цены привязанные к этой Услуге + с пометкой вывода на странице
		"SERVICE_ID" => $service->filter,
		"USER_PARAM_ONLINE_CODE" => $service->onlineCode,
	),
	false
);
