<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {die();}

include __DIR__."/code/Service.php";

/** @var array $arResult */
$service = new Services\Service($arResult);

include __DIR__."/template/index.php";
