<? /** @var Services\Service $service */ ?>
<? if($service->picture): ?>
    <picture>
        <source srcset="<?= WebPHelper::getOrCreateWebPUrl($service->picture) ?>" type="image/webp"/>
        <source srcset="<?= $service->picture ?>"/>
        <img
            class="service-header__image"
            src="<?= $service->picture ?>"
            loading="lazy"
            decoding="async"
            alt="<?= $service->title ?>"
            title="<?= $service->title ?>"
        />
    </picture>
<? endif ?>
