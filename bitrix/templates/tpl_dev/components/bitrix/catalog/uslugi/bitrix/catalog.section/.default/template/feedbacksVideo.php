<? /** @var Services\Service $service */ ?>
<? if($service->feedbacksVideo): ?>
	<section class="container page-section page-section--no-padding--top">
		<div class="content">

			<h2>Видеоотзывы</h2>

			<div class="grid page-subsection">
                <? foreach($service->feedbacksVideo as $feedback): ?>
                    <div class="grid__cell grid__cell--s--6 grid__cell--xs--12">
                        <div class="iframe-responsive">
                            <div class="iframe-responsive__iframe">
                                <iframe src="<?= $feedback ?>" allowfullscreen="allowfullscreen"></iframe>
                            </div>
                        </div>
                    </div>
                <? endforeach ?>
			</div>

		</div>
	</section>
<? endif ?>