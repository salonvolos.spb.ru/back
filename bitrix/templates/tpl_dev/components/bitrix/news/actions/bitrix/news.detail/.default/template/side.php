<? /** @var Actions\Action $action */ ?>
<div class="grid__cell grid__cell--l--3 grid__cell--m--4 grid__cell--xs--12">
    <div class="grid par">
        
        <div class="grid__cell grid__cell--xs--12 block-marked block-marked--padding-small text-align-center discount-aside">

            <p><strong>Действие акции</strong></p>

            <? if($action->begin): ?>
                <p><?= $action->begin ?> — <?= $action->expiredAsValue ?></p>
            <? else: ?>
                <p>до <?= $action->expiredAsValue ?></p>
            <? endif ?>

            <p><strong>Вы можете узнать подробности по телефону</strong></p>
            <p><a href="tel:+78123850151">+7 (812) 385-01-51</a></p>
            <p><strong>или</strong></p>
            <p><a class="button ms_booking discount-aside__button" href="#">Записаться в салон</a></p>

        </div>

        <? if($action->price): ?>
        <div class="grid__cell grid__cell--xs--12 block-marked block-marked--padding-small text-align-center">
            <p><strong>Стоимость</strong></p>
            <?= $action->price ?>
        </div>
        <? endif ?>
    </div>

</div>
