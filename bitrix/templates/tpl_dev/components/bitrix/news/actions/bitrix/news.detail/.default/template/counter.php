<? /** @var Actions\Action $action */ ?>

<h2 class="text-align-center">До конца акции осталось</h2>

<div class="countdown block-marked block-marked--padding-small par" data-date="<?= ($action->expiredTimeStamp + 23 * 3600 + 59 * 60 + 59) * 1000 ?>">
    <div class="countdown__item countdown__days">
        <div class="countdown__val">00</div><span class="countdown__text">дней</span>
    </div>
    <div class="countdown__separator">:</div>
    <div class="countdown__item countdown__hours">
        <div class="countdown__val">00</div><span class="countdown__text">часов</span>
    </div>
    <div class="countdown__separator">:</div>
    <div class="countdown__item countdown__minutes"> 
        <div class="countdown__val">00</div><span class="countdown__text">минут</span>
    </div>
    <div class="countdown__separator">:</div>
    <div class="countdown__item countdown__seconds"> 
        <div class="countdown__val">00</div><span class="countdown__text">секунд</span>
    </div>
</div>
