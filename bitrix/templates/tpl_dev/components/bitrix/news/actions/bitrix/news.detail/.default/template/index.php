<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true){die();} ?>
<? /** @var Actions\Action $action */ ?>
<section class="container page-section discount-header">
    <div class="content">
        <? include __DIR__."/image.php" ?>
        <? include __DIR__."/breadcrumbs.php" ?>
    </div>
</section>

        
<section class="container page-section page-section--no-padding--top page-section--no-padding--bottom">
    <div class="content">
        <div class="grid par">
            <? include __DIR__."/description.php" ?>
            <? include __DIR__."/side.php" ?>
            <div class="grid__cell grid__cell--xs--12">
                <? include __DIR__."/how-we-work.php" ?>
                <? include __DIR__."/works.php" ?>
                <? include __DIR__."/more.php" ?>
                <? include __DIR__."/masters.php" ?>
            </div> 
        </div> 
    </div>
</section>

