<? /** @var ActionsList\Action $action */  ?>
<div class="discount-item__image">
	<a href="<?= $action->url ?>">
		<picture>
			<source srcset="<?= WebPHelper::getOrCreateWebPUrl($action->image, [800, 1600]) ?>" type="image/webp"/>
			<source srcset="<?= $action->image ?>"/>
			<img
				src="<?= $action->image ?>"
				loading="lazy"
				decoding="async"
				alt="<?= $action->title ?>"
				title="<?= $action->title ?>"
				width="800"
				height="800"
			/>
		</picture>
	</a>
</div>
