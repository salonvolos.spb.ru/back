<? /** @var Actions\Action $action */ ?>
<? if($action->more): ?>

<h2 class="text-align-center">Другие акции и специальные предложения</h2>

<div class="page-subsection">
    <div class="grid discount-item">

        <? /** @var Actions\Action $moreAction */ ?>
        <? foreach ($action->more as $moreAction): ?>
        <div class="grid__cell grid__cell--l--4 grid__cell--m--6 grid__cell--s--6 grid__cell--xs--12 discount-item__wrapper">

            <? if($moreAction->image): ?>
            <div class="discount-item__image">
                <a href="<?= $moreAction->url ?>">
                    <img src="<?= $moreAction->image ?>"/>
                </a>
            </div>
            <? endif ?>

            <div class="block-marked discount-item__body">

                <h2 class="h3">
                    <a class="discount-item__title" href="<?= $moreAction->url ?>"><?= $moreAction->title ?></a>
                </h2>

                <div><?= $moreAction->text ?></div>
                <div>Акция действует до <?= $moreAction->expiredAsValue ?></div>
                <div>
                    <a class="discount-item__link" href="<?= $moreAction->url ?>">Подробнее</a>
                </div>

            </div>

        </div>
        <? endforeach ?>

    </div>
    <a class="button" style="margin: 2rem auto 0; width: min-content; white-space: nowrap;" href="/aktsii/">Больше акций</a>
</div>


<? endif ?>
