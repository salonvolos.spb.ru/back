<?
namespace Actions;

class Master
{
    public $image;
    public $url;
    public $title;
    public $skills;

    public function __construct($elementId)
    {
        if(!$element = getIbElement($elementId)){
            return null;
        }

        $this->image = getImageSrc($element->fields["PREVIEW_PICTURE"]);
        $this->url = $element->fields["DETAIL_PAGE_URL"];
        $this->title = $element->fields["NAME"];
        $this->skills = $element->props["SKILLS"]["VALUE"];
    }

}