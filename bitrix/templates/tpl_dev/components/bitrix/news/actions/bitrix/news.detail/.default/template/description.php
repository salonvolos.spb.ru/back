<? /** @var Actions\Action $action */ ?>
<div class="grid__cell grid__cell--l--9 grid__cell--m--8 grid__cell--xs--12">

    <h1><?= $action->title ?></h1>

    <p><?= $action->subtitle ?></p>

    <? if($action->isShowCounter): ?>
        <? include __DIR__."/counter.php" ?>
    <? endif ?>

    <? if($action->description): ?>
    <h2 class="text-align-center">Описание акции</h2>
    <div class="par">
        <?= $action->description ?>
    </div>
    <? endif ?>

</div>
