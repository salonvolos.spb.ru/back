<? /** @var Actions\Action $action */ ?>
<? /** @var Actions\Work $work */ ?>
<? if($action->works): ?>

<div class="page-subsection">
    <h2 class="text-align-center">Наши работы</h2>

    <div class="page-subsection grid grid--align-items--flex-start">
        <? foreach ($action->works as $work): ?>

            <a href="<?= $work->image ?>" class="works-item grid__cell grid__cell--l--3 grid__cell--m--4 grid__cell--s--6 grid__cell--xs--6" data-glightbox="data-glightbox">
                <div class="works-item__title"><?= $work->title ?></div>
                <img
                    class="works-item__image"
                    src="<?= $work->image ?>"
                    loading="lazy"
                    decoding="async"
                    alt="<?= $work->title ?>">
            </a>
        <? endforeach ?>
    </div>

    <? /* <div class="block-wrap page-works block-wrap_wrap " id="page-works">
        <? foreach ($action->works as $work): ?>
        <div class="block-wrap__item block-wrap__item_xl-width3 block-wrap__item_l-width3 block-wrap__item_m-width3 block-wrap__item_s-width3 page-works__item">
            <figure>
                <img src="<?= $work->image ?>" alt="<?= $work->title ?>">
                <figcaption>
                    <div class="page-works__title"><?= $work->title ?></div>
                </figcaption>
            </figure>
        </div>
        <? endforeach ?>
    </div> */?>

</div>
<? endif ?>
