<?php /** @var Actions\Action $action */
/** @var Actions\Master $master */
use Bitrix\Main\Localization\Loc;
?>

<?php

?>
<div class="page-subsection">
    <?php if ($APPLICATION->GetCurPage(false) == '/aktsii/velkom-zapis-k-novym-masteram-so-skidkoy/') {?>
        <h2 class="text-align-center"><?php echo Loc::getMessage('NEW_TITLE') ?></h2>
    <?php } else {?>
        <h2 class="text-align-center"><?php echo Loc::getMessage('OLD_TITLE') ?></h2>

    <?php } ?>


    <p>Мастера обучались в ведущих студиях и академиях: <strong>Coiffure, Локон, Londa, Shwarzkopf, Wella.</strong></p>
    <p>Сотрудники салона неоднократно участвовали в международном фестивале красоты<strong>&laquo;Невские берега&raquo;</strong>, крупнейшей в Европе парфюмерно-косметической выставке Intercharm.</p>

</div>

<div class="page-subsection">
    <div class="grid grid--columns--10">

        <? foreach ($action->masters as $master): ?>
        <div class="grid__cell grid__cell--l--2 grid__cell--m--5 grid__cell--s--10 grid__cell--xs--5 master-item">

                <? if($master->image): ?>
                <div class="master-item__image">
                    <? /* <a href="<?= $master->url ?>"> */?>
                        <img class="master-item__image-item" src="<?= $master->image ?>" alt="<?= $master->title ?>" title="<?= $master->title ?>" width='214' height='214'>
                        <a class="button master-item__button ms_booking" href="#" data-url="https://n371027.yclients.com/company:170918?o=m951514">Записаться</a>
                        <? /* </a>  */?>
                </div>
                <? endif ?>

                <h2 class="master-item__title"><?= $master->title ?></h2>
                <? if($master->skills): ?>
                <ul class="master-item__list">
                    <? foreach ($master->skills as $skill): ?>
                        <li class="master-item__list-item"><?= $skill ?></li>
                    <? endforeach ?>
                </ul>
                <? endif ?>
                <a class="master-item__more" href="<?= $master->url ?>">Подробнее</a>

        </div>
        <? endforeach ?>
    </div>

</div>
