<? /** @var Actions\Action $action */ ?>
<? if($action->image): ?>
    <div class="page-subsection">
        <img src="<?= $action->image ?>" alt="<?= $action->title ?>">
    </div>
<? endif ?>
