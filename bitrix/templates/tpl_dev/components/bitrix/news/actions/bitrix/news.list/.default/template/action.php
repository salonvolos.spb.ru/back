<? /** @var ActionsList\Action $action */  ?>
<div class="grid__cell grid__cell--l--4 grid__cell--m--6 grid__cell--s--6 grid__cell--xs--12 discount-item__wrapper">

	<? if($action->image): ?>
        <? include __DIR__."/image.php" ?>
	<? endif ?>

    <div class="block-marked discount-item__body">
        <h2 class="h3">
            <a
                class="discount-item__title"
                href="<?= $action->url ?>"
            ><?= $action->title ?></a>
        </h2>

		<?= $action->text ?>

        <a
            class="discount-item__link"
            href="<?= $action->url ?>"
        >Подробнее</a>

    </div>

</div>
