<div class="page-subsection">
    <h2  class="text-align-center">Как мы работаем?</h2>
    <ol class="grid grid--justify--space-around list-steps list-steps--bottom page-subsection">
        <li class="grid__cell grid__cell--m--6 grid__cell--xs--12 list-steps__item">
        <p><img src="/images/coloring/how-we-work-01.jpg" loading="lazy" decoding="async" alt="Окрашивание: этап 1" title="Окрашивание: этап 1"></p>
        <div class="list-steps__inner par">
            <p><strong>Поможем выбрать цвет</strong> и создать индивидуальный образ, учтем все особенности: стиль, цвет кожи, цвет глаз. Предложим огромную <strong>палитру</strong> красок.</p>
        </div>
        </li>
        <li class="grid__cell grid__cell--m--6 grid__cell--xs--12 list-steps__item">
        <p><img src="/images/coloring/how-we-work-02.jpg" loading="lazy" decoding="async" alt="Окрашивание: этап 2" title="Окрашивание: этап 2"></p>
        <div class="list-steps__inner par">
            <p>Перед началом процедуры внимательно <strong>выслушаем пожелания клиента</strong> и расскажем, какой результат получится в итоге.</p>
        </div>
        <p>В конце консультации мастер <strong>озвучит стоимость</strong> услуги и время обслуживания. Если клиент не согласится со стоимостью услуги, то может отказаться от услуги.</p>
        </li>
        <li class="grid__cell grid__cell--m--6 grid__cell--xs--12 list-steps__item">
        <p><img src="/images/coloring/how-we-work-03.jpg" loading="lazy" decoding="async" alt="Окрашивание: этап 3" title="Окрашивание: этап 3"></p>
        <div class="list-steps__inner par">
            <p>Чтобы вы не волновались в процессе окрашивания, подробно <strong>объясним каждый шаг</strong> в самом начале процедуры и будем разъяснять некоторые детали в процессе.</p>
        </div>
        </li>
        <li class="grid__cell grid__cell--m--6 grid__cell--xs--12 list-steps__item">
        <p><img src="/images/coloring/how-we-work-04.jpg" loading="lazy" decoding="async" alt="Окрашивание: этап 4" title="Окрашивание: этап 4"></p>
        <div class="list-steps__inner par">
            <p>После процедуры <strong>порекомендуем серию косметики</strong> для окрашенных волос, увлажняющие шампуни.</p>
        </div>
        </li>
    </ol>
    </div>
