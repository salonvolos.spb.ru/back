<?
namespace Actions;

class Work
{
    public $title;
    public $image;

    public function __construct($elementId)
    {
        if(!$element = getIbElement($elementId)){
            return null;
        }

        $this->title = $element->fields["NAME"];
        $this->image = getImageSrc($element->props["PHOTO"]["VALUE"][0]);
    }
}