<? /** @var Actions\Action $action */ ?>
<div class="page-subsection discount-header__breadcrumbs">
    <div class="breadcrumbs">

        <div class="breadcrumbs__parent">
            <a href="/">Главная</a>
        </div>
        <div class="breadcrumbs__separator"></div>

        <div class="breadcrumbs__parent">
            <a href="<?= $action->listUrl ?>">Акции</a>
        </div>
        <div class="breadcrumbs__separator"></div>

        <div class="breadcrumbs__current"><?= $action->title ?></div>

    </div>
</div>
