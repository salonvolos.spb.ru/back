<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
include_once __DIR__."/Action.php";
?>
<div class="page-subsection">
	<p class="text-marked text-marked--size--s text-align-center">Скидки применяются при наличной форме оплаты</p>
</div>
<div class="page-subsection">
    <div class="grid discount-item">

		<? foreach($arResult["ITEMS"] as $arItem): ?>
			<? $action = new ActionsList\Action($arItem) ?>
			<? include __DIR__."/template/action.php" ?>
		<? endforeach ?>

		<?= $arResult["NAV_STRING"] ?>

    </div>
</div>
