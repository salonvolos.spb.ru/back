<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if($arResult["DETAIL_TEXT"]): ?>
    <div class="page-subsection">
        <div class="grid">

            <div class="grid__cell grid__cell--xs--12">
                <div class="par">
                    <?= $arResult["~DETAIL_TEXT"] ?>
                </div>
                <p>
                    
                    <strong>Категория:</strong> <a href="<?= $sectionUrl ?>"><?= $sectionName ?></a>
                </p>
            </div>

            <div class="grid__cell grid__cell--xs--12 consultation-author">
                <div class="consultation-author__image">
                    <picture>
                        <source srcset="/images/leytes.webp" type="image/webp"/>
                        <source srcset="/images/leytes.jpg"/><img src="/images/leytes.jpg" loading="lazy" decoding="async" alt="Ольга Лейтес" title="Ольга Лейтес"/>
                    </picture>
                </div>
                <div class="consultation-author__answer">
                    <p>Отвечает:</p>
                    <p class="text-marked text-marked--color--beige"><strong>Ольга Лейтес, директор центра</strong>
                    </p>
                </div>
            </div>

        </div>


    </div>
<? endif ?>
