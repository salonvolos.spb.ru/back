<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
include __DIR__."/Question.php";
?>
<div class="page-subsection">

	<ul class="consultation-list">
		<? foreach($arResult["ITEMS"] as $arItem): ?>
			<? $question = new ConsultationList\Question($arItem) ?>
			<? include __DIR__."/template/question.php" ?>
		<? endforeach ?>
	</ul>

</div>

<?= $arResult["NAV_STRING"] ?>


