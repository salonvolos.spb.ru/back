<?
namespace ConsultationList;

class Question
{
	public $title;
	public $text;
	public $name;
	public $date;
	public $url;

	public function __construct($arItem)
	{
		$this->title = $arItem["NAME"];
		$this->text = $arItem["PREVIEW_TEXT"];
		$this->name = $arItem["DISPLAY_PROPERTIES"]["FIO"]["DISPLAY_VALUE"];
		$this->date = $arItem["DISPLAY_ACTIVE_FROM"];
		$this->url = $arItem["DETAIL_PAGE_URL"];
	}
}
