<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<div class="page-subsection">
    <div class="consultation-question block-marked">

        <h1 class="h2">
            <?= $arResult["NAME"] ?>
        </h1>

        <div class="grid grid--align-items--center par">

            <div class="grid__cell grid__cell--m--9 grid__cell--s--12 grid__cell--xs--12">
                <?= $arResult["PREVIEW_TEXT"] ?>
            </div>

            <div class="grid__cell grid__cell--m--3 grid__cell--s--12 grid__cell--xs--12">

                <? if($authorInfo): ?>
                <p>
                    <?= implode(", <br>", $authorInfo) ?>
                </p>
                <? endif ?>

                <span class="text-marked text-marked--size--xs text-marked--color--grey">
                    <?= $arResult["DISPLAY_ACTIVE_FROM"] ?>
                </span>

            </div>

        </div>

    </div>
</div>

