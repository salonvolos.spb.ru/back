<? /** @var ConsultationList\Question $question */ ?>
<li class="consultation-list__item">

	<h2 class="h3 consultation-list__title">
		<a
			class="consultation-list__link"
			href="<?= $question->url ?>"
		><?= $question->title ?></a>
	</h2>

	<div class="grid par">

		<div class="grid__cell grid__cell--m--9 grid__cell--s--12 grid__cell--xs--12">
			<p><?= $question->text ?></p>
		</div>

		<div class="grid__cell grid__cell--m--3 grid__cell--s--12 grid__cell--xs--12">
			<p><?= $question->name ?></p>
			<span class="consultation-list__date"><?= $question->date ?></span>
		</div>

	</div>

	<p>
        <a href="<?= $question->url ?>">Читать ответ</a>
    </p>

</li>
