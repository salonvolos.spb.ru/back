<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$similarQuestions = getSimilarQuestions($sectionId, $arResult["ID"], ID_IBLOCK_CONSULTATION);

if($similarQuestions): ?>
    <div class="page-subsection">

        <h2>Похожие вопросы</h2>
        <?
        foreach($similarQuestions as $question):
            $authorInfo = [];

            $name = $question["PROPERTY_FIO_VALUE"];
            if($name){
                $authorInfo[] = $name;
            }

            $city = $question["PROPERTY_CITY_VALUE"];
            if($city){
                $authorInfo[] = $city;
            }
        ?>
        <div class="consultation-similar par">
            <a href="<?= $question["DETAIL_PAGE_URL"] ?>">
                <?= $question["NAME"] ?>
            </a>
            <div class="consultation-similar__info">
                <? if($authorInfo): ?>
                    <span class="text-marked text-marked--size--xs text-marked--color--grey">
                        <?= implode(", ", $authorInfo) ?>
                    </span>
                <? endif ?>
                <span class="text-marked text-marked--size--xs text-marked--color--grey">
                    <?= $question["CREATED_DATE"] ?>
                </span>
            </div>
        </div>
        <?
        endforeach ?>

    </div>
<? endif ?>
