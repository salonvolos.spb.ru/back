<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$pagination = "";
$curTitle = $APPLICATION->GetPageProperty("title");

$isPagination = false;
if(!empty($_GET["PAGEN_1"])){
    $isPagination = true;
    $pagination = " / Страница ".(int)$_GET["PAGEN_1"];
}

$isInSection = false;
if(!empty($arResult["SECTION"])){
    $isInSection = true;
}

$sectionName = "";
if($isInSection){
    $sectionName = $arResult["SECTION"]["PATH"][0]["NAME"];
    $APPLICATION->SetPageProperty("title", $sectionName." / ".$curTitle);
    $curTitle = $APPLICATION->GetPageProperty("title");
}

// Страница пагинации + корневой раздел
if($isPagination && !$isInSection){

	$APPLICATION->SetPageProperty("title", $curTitle.$pagination);

	$description = "Вопросы и ответы по различным вопросам, связанным с услугами салона красоты. Задать вопрос парикмахеру.";
	$APPLICATION->SetPageProperty("description", $description);

	$keywords = "консультация парикмахер вопросы ответы";
	$APPLICATION->SetPageProperty("keywords", $keywords);

}

// Страница пагинации + НЕ корневой раздел
if($isPagination && $isInSection){
	$APPLICATION->SetPageProperty("title", $curTitle.$pagination);
}
