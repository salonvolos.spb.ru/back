<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>
<? $this->setFrameMode(true) ?>
<div class="header-search header__search">

    <button class="header-search__switcher"></button>

    <form
        action="<?= $arResult["FORM_ACTION"] ?>"
        method="post"
        class="header-search__form"
    >
        <label class="form-input form-input--search header-search__input">
            <input class="form-input__field" type="search" name="q" value="" required>
        </label>
        <button type="submit" class="header-search__submit" name="s"></button>
    </form>

</div>
