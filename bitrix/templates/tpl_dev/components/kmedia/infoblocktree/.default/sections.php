<? // Цикл по Разделам Цен ?>
<? foreach ($arResult["ITEMS"] as $key => $arItem): ?>

    <? if($key == "ROOT"){continue;} ?>

    <div class="grid prices-block">

        <? if(empty($arResult["ROOT_SECTION_NAME"])): // выводим заголовок Раздела только в корневом Разделе ?>
            <h2 class="grid__cell grid__cell--l--5 grid__cell--m--4 grid__cell--xs--12 prices-block__title"><?= $arItem["NAME"] ?></h2>
		<? endif ?>

        <? if($arItem["ITEMS"]): ?>
            <div class="grid__cell grid__cell--l--7 grid__cell--m--8 grid__cell--xs--12 prices-block__tables">
                <? foreach($arItem["ITEMS"] as $service): ?>
                    <? include __DIR__."/item.php" ?>
                <? endforeach ?>
            </div>
		<? else: ?>
            <p>В разделе нет цен</p>
        <? endif ?>

    </div>

<? endforeach ?>

<? if(!empty($arResult["ITEMS"]["ROOT"])): ?>
    <div class="prices-table__row" style="margin-top: 30px;"></div>
    <? $arItem = $arResult["ITEMS"]["ROOT"] ?>
    <? foreach($arItem["ITEMS"] as $service): ?>
        <? include __DIR__."/item.php" ?>
    <? endforeach ?>
<? endif ?>
