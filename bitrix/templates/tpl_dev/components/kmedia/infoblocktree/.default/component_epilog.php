<?php
$seoTitle = false;

if($arResult["ROOT_SECTION_NAME"] == 'Стрижка'){
	$seoTitle = 'Цены на стрижки';
}

if($arResult["ROOT_SECTION_NAME"] == 'Прическа'){
	$seoTitle = 'Цены на прически';
}

if($arResult["ROOT_SECTION_NAME"] == 'Окрашивание'){
	$seoTitle = 'Цены на окрашивание';
}

if($arResult["ROOT_SECTION_NAME"] == 'Укладка'){
	$seoTitle = 'Цены на укладки';
}

if($arResult["ROOT_SECTION_NAME"] == 'Выпрямление волос'){
	$seoTitle = 'Цены на выпрямление волос';
}

if($arResult["ROOT_SECTION_NAME"] == 'Завивка волос'){
	$seoTitle = 'Цены на завивку волос';
}

if($arResult["ROOT_SECTION_NAME"] == 'Наращивание волос'){
	$seoTitle = 'Цены на наращивание волос';
}

if($arResult["ROOT_SECTION_NAME"] == 'Уход и лечение'){
	$seoTitle = 'Цены на уход и лечение';
}

if($arResult["ROOT_SECTION_NAME"] == 'Визаж'){
	$seoTitle = 'Цены на визаж';
}

if($seoTitle){
	$APPLICATION->SetTitle($seoTitle);
	$APPLICATION->SetPageProperty('title', $seoTitle.' в Санкт-Петербурге | Hair Care Center');
	$APPLICATION->SetPageProperty('description', $seoTitle.' в Санкт-Петербурге в салоне красоты "Hair Care Center". Профессионализм, отдельные кабинеты. Среди наших клиентов актрисы театра и кино, звезды реалити-шоу ★ Записывайтесь: +7 (812) 385-01-51');
}











