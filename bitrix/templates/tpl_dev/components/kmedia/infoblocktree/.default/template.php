<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
?>
<div class="page-subsection">
    <? if(!empty($arResult["ERROR"])): ?>
        <p class="error">Ошибка: <?= $arResult["ERROR"] ?></p>
    <? else: ?>
        <? if ($arResult["ITEMS"]): ?>
            <? include __DIR__."/sections.php" ?>
        <? else: ?>
            <p>Нет разделов цен</p>
        <? endif ?>
    <? endif ?>

    <? if(!empty($arResult["ROOT_SECTION_DESCRIPTION"])): ?>
        <h2>Описание</h2>
        <?= $arResult["ROOT_SECTION_DESCRIPTION"] ?>
    <? endif ?>
</div>
