<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<section class="container page-section page-section--no-padding--top" id="page-prices">
    <div class="content">
        <div class="prices-block">

            <?php if(!empty($arResult["ERROR"])): ?>
                <p class="error">Ошибка: <?= $arResult["ERROR"] ?></p>
            <?php else: ?>
                <?php if($arResult["LAYOUT"] == "with_materials"): ?>
                    <?php include __DIR__."/layouts/with-material/index.php" ?>
                <?php elseif($arResult["LAYOUT"] == "without_materials"): ?>
                    <?php include __DIR__."/layouts/without-material/index.php" ?>
                <?php else: ?>
                    <?php include __DIR__."/layouts/only-base-price/index.php" ?>
                <?php endif ?>
            <?php endif ?>

        </div>
    </div>
</section>

<?php if (
    preg_match('#^/uslugi/okrashivanie-volos/(?!muzhskoe-okrashivanie).*#',
        $APPLICATION->GetCurPage(false))
) {?>
    <section class="container page-section page-section--background-color" id="coloring-calculator">
        <div class="content">
            <h2>Рассчитайте стоимость окрашивания</h2>
            <div class="multi-step-calculator page-subsection">
                <div class="multi-step-calculator__inner">
                    <div class="multi-step-calculator__slide multi-step-calculator__slide--service">
                        <h3 class="multi-step-calculator__title">Вид окрашивания </h3>
                        <div class="grid grid--justify--center par">

                            <?php if ($APPLICATION->GetCurPage(false) === '/uslugi/okrashivanie-volos/' || $APPLICATION->GetCurPage(false) === '/uslugi/okrashivanie-volos/v-odin-ton/')
                            { ?>
                                <label class="form-input form-input--select grid__cell multi-step-calculator__select">
                                    <select class="form-input__field">
                                        <option value="Окраска волос в один тон">Окраска волос в один тон</option>
                                        <option value="Сложное окрашивание">Сложное окрашивание (3D/4D эффект (Шатуш / Балаяж / AirTouch)
                                        </option>
                                        <option value="Снятие краски">Снятие краски / смывка / обесцвечивание
                                        </option>
                                        <option value="Мелирование классическое">Мелирование классическое
                                        </option>
                                        <option value="Мелирование классическое с тонированием">Мелирование классическое с тонированием
                                        </option>
                                    </select>
                                </label>
                                <img class="grid__cell grid__cell--l--5 grid__cell--m--6 grid__cell--s--8 border border--beige-light border-radius border-radius--big" width="600" height="600" src="/images/uslugi/one-tone.webp" loading="lazy" decoding="async">
                            <?php } elseif (
                                $APPLICATION->GetCurPage(false) === '/uslugi/okrashivanie-volos/dekapirovanie/')
                            { ?>
                                <label class="form-input form-input--select grid__cell multi-step-calculator__select">
                                    <select class="form-input__field">
                                        <option value="Снятие краски">Снятие краски / смывка / обесцвечивание</option>
                                        <option value="Окраска волос в один тон">Окраска волос в один тон
                                        </option>
                                        <option value="Сложное окрашивание">Сложное окрашивание (3D/4D эффект (Шатуш / Балаяж / AirTouch)
                                        </option>
                                        <option value="Мелирование классическое">Мелирование классическое
                                        </option>
                                        <option value="Мелирование классическое с тонированием">Мелирование классическое с тонированием
                                        </option>
                                    </select>
                                </label>
                                <img class="grid__cell grid__cell--l--5 grid__cell--m--6 grid__cell--s--8 border border--beige-light border-radius border-radius--big" width="600" height="600" src="/images/uslugi/removal.webp" loading="lazy" decoding="async">
                            <?php }elseif ( $APPLICATION->GetCurPage(false) === '/uslugi/okrashivanie-volos/tonirovanie/'){?>
                                <label class="form-input form-input--select grid__cell multi-step-calculator__select">
                                    <select class="form-input__field">
                                        <option value="Окраска волос в один тон">Окраска волос в один тон</option>
                                        <option value="Сложное окрашивание">Сложное окрашивание (3D/4D эффект (Шатуш / Балаяж / AirTouch)
                                        </option>
                                        <option value="Снятие краски">Снятие краски / смывка / обесцвечивание
                                        </option>
                                        <option value="Мелирование классическое">Мелирование классическое
                                        </option>
                                        <option value="Мелирование классическое с тонированием">Мелирование классическое с тонированием
                                        </option>
                                    </select>
                                </label>
                                <img class="grid__cell grid__cell--l--5 grid__cell--m--6 grid__cell--s--8 border border--beige-light border-radius border-radius--big" width="600" height="600" src="/images/uslugi/one-tone.webp" loading="lazy" decoding="async">
                            <?php } elseif (
                                $APPLICATION->GetCurPage(false) === '/uslugi/okrashivanie-volos/melirovaniye/'
                                || $APPLICATION->GetCurPage(false) === '/uslugi/okrashivanie-volos/kaliforniyskoe/')
                            { ?>
                                <label class="form-input form-input--select grid__cell multi-step-calculator__select">
                                    <select class="form-input__field">
                                        <option value="Мелирование классическое">Мелирование классическое</option>
                                        <option value="Мелирование классическое с тонированием">Мелирование классическое с тонированием
                                        </option>
                                        <option value="Окраска волос в один тон">Окраска волос в один тон
                                        </option>
                                        <option value="Сложное окрашивание">Сложное окрашивание (3D/4D эффект (Шатуш / Балаяж / AirTouch)
                                        </option>
                                        <option value="Снятие краски">Снятие краски / смывка / обесцвечивание
                                        </option>
                                    </select>
                                </label>
                                <img class="grid__cell grid__cell--l--5 grid__cell--m--6 grid__cell--s--8 border border--beige-light border-radius border-radius--big" width="600" height="600" src="/images/uslugi/classic.webp" loading="lazy" decoding="async">
                            <?php } else {?>
                                <label class="form-input form-input--select grid__cell multi-step-calculator__select">
                                    <select class="form-input__field">
                                        <option value="Сложное окрашивание">Сложное окрашивание (3D/4D эффект (Шатуш / Балаяж / AirTouch)</option>
                                        <option value="Окраска волос в один тон">Окраска волос в один тон
                                        </option>
                                        <option value="Снятие краски">Снятие краски / смывка / обесцвечивание
                                        </option>
                                        <option value="Мелирование классическое">Мелирование классическое
                                        </option>
                                        <option value="Мелирование классическое с тонированием">Мелирование классическое с тонированием
                                        </option>
                                    </select>
                                </label>
                                <img class="grid__cell grid__cell--l--5 grid__cell--m--6 grid__cell--s--8 border border--beige-light border-radius border-radius--big" width="600" height="600" src="/images/uslugi/complex.webp" loading="lazy" decoding="async">
                            <?php } ?>
                        </div>
                    </div>
                    <div class="multi-step-calculator__slide multi-step-calculator__slide--length">
                        <h3 class="multi-step-calculator__title">Длина волос</h3>
                        <div class="grid grid--justify--center par">
                            <label class="form-input form-input--select grid__cell multi-step-calculator__select">
                                <select class="form-input__field">
                                    <option value="До плеч">До плеч (до 20 см)
                                    </option>
                                    <option value="Ниже плеч">Ниже плеч (от 20 см)
                                    </option>
                                </select>
                            </label>
                            <div class="grid__cell grid__cell--l--5 grid__cell--m--6 grid__cell--s--8 grid__cell--xs--12">
                                <picture>
                                    <source srcset="/images/uslugi/hair-length.webp" type="image/webp"/>
                                    <source srcset="/images/uslugi/hair-length.jpg"/><img class="border border--pink border-radius border-radius--big" src="/images/uslugi/hair-length.jpg" loading="lazy" decoding="async"/>
                                </picture>
                            </div>
                        </div>
                    </div>
                    <div class="multi-step-calculator__slide multi-step-calculator__slide--density">
                        <h3 class="multi-step-calculator__title">Густота волос</h3>
                        <label class="form-input form-input--radio">
                <span class="form-input__label">Обычная
                </span>
                            <input class="form-input__field" type="radio" name="density" value="Обычная" checked="checked"></input>
                        </label>
                        <label class="form-input form-input--radio">
                <span class="form-input__label">Повышенная
                </span>
                            <input class="form-input__field" type="radio" name="density" value="Повышенная"></input>
                        </label>
                    </div>
                    <div class="multi-step-calculator__slide multi-step-calculator__slide--additional-service">
                        <h3 class="multi-step-calculator__title">Дополнительные услуги</h3>
                        <label class="form-input form-input--radio">
                <span class="form-input__label">Стрижка
                </span>
                            <input class="form-input__field" type="radio" name="service" value="Стрижка" checked="checked"></input>
                        </label>
                        <label class="form-input form-input--radio">
                <span class="form-input__label">Укладка
                </span>
                            <input class="form-input__field" type="radio" name="service" value="Укладка"></input>
                        </label>
                        <label class="form-input form-input--radio">
                <span class="form-input__label">Прическа
                </span>
                            <input class="form-input__field" type="radio" name="service" value="Прическа"></input>
                        </label>
                        <label class="form-input form-input--radio">
                <span class="form-input__label">Не требуются
                </span>
                            <input class="form-input__field" type="radio" name="service" value="Не требуются"></input>
                        </label>
                    </div>
                    <div class="multi-step-calculator__slide multi-step-calculator__slide--colorant">
                        <h3 class="multi-step-calculator__title">Предпочитаемый краситель</h3>
                        <label class="form-input form-input--radio">
                <span class="form-input__label">Estel
                </span>
                            <input class="form-input__field" type="radio" name="colorant" value="Estel" checked="checked"></input>
                        </label>
                        <label class="form-input form-input--radio">
                <span class="form-input__label">Matrix
                </span>
                            <input class="form-input__field" type="radio" name="colorant" value="Matrix"></input>
                        </label>
                        <?php /* <label class="form-input form-input--radio">
                <span class="form-input__label">Loreal
                </span>
                  <input class="form-input__field" type="radio" name="colorant" value="Loreal"></input>
              </label> */?>
                        <label class="form-input form-input--radio">
                <span class="form-input__label">Не имеет значения
                </span>
                            <input class="form-input__field" type="radio" name="colorant" value="Не имеет значения"></input>
                        </label>
                    </div>
                    <div class="multi-step-calculator__slide">
                        <h3>Общая стоимость</h3>
                        <div class="prices-table prices-table--small-padding page-subsection">
                            <div class="prices-table__row" data-total-price>
                                <div class="text-marked text-marked--size--m text-marked--color--beige text-marked--bold prices-table__cell prices-table__cell--row-header">C 10% скидкой на услуги
                                </div>
                                <div class="text-marked text-marked--size--m text-marked--color--beige prices-table__cell prices-table__cell--price-discount">
                                    <div class="prices-table__old-price"> </div>
                                    <div class="prices-table__new-price"><strong></strong></div>
                                </div>
                            </div>
                        </div>
                        <div class="prices-table prices-table--small-padding">
                            <div class="prices-table__row" data-service>
                                <div class="prices-table__cell prices-table__cell--row-header">Название услуги</div>
                                <div class="prices-table__cell prices-table__cell--price-discount">
                                    <div class="prices-table__old-price"> </div>
                                    <div class="prices-table__new-price"> </div>
                                </div>
                            </div>
                        </div>
                        <div class="prices-table prices-table--small-padding">
                            <div class="prices-table__row" data-additional-service>
                                <div class="prices-table__cell prices-table__cell--row-header">Название дополнительной услуги</div>
                                <div class="prices-table__cell prices-table__cell--price-discount">
                                    <div class="prices-table__old-price"> </div>
                                    <div class="prices-table__new-price"> </div>
                                </div>
                            </div>
                        </div>
                        <div class="prices-table prices-table--small-padding">
                            <div class="prices-table__row" data-colorant>
                                <div class="prices-table__cell prices-table__cell--row-header">Краситель</div>
                                <div class="prices-table__cell prices-table__cell--price-discount">
                                    <div class="prices-table__old-price"> </div>
                                    <div class="prices-table__new-price"> </div>
                                </div>
                            </div>
                        </div>
                        <p class="text-marked text-marked--size--xs text-marked--color--grey">*указана ориентировочная стоимость, точная стоимость определяется мастером и объявляется клиенту до процедуры
                        </p>
                    </div>
                </div>
                <div class="multi-step-calculator__footer">
                    <a class="button button--secondary multi-step-calculator__button multi-step-calculator__button--prev" href="#" data-nav="prev">Назад</a>
                    <a class="button multi-step-calculator__button multi-step-calculator__button--next" href="#" data-nav="next">Дальше</a>
                    <a class="button multi-step-calculator__button multi-step-calculator__button--yclients" href="#multi-step-calculator" data-modal="data-modal">Записаться со скидкой</a>
                    <div class="multi-step-calculator__pagination"></div>
                </div>
            </div>
            <div class="modal" id="multi-step-calculator">
                <div class="modal__inner">
                    <h3 class="text-align-center multi-step-calculator-form-title">Записаться в салон</h3>
                    <form class="validated-form multi-step-calculator-form par submitJS" method="post">
                        <p class="multi-step-calculator-form__header">Вы также можете записаться по телефону <a class="multi-step-calculator-form__phone" href="tel:+78123850151">+7 (812) 385-01-51</a></p>
                        <div class="multi-step-calculator-form__inner">
                            <label class="form-input form-input--tel"> <span class="form-input__label"><strong>Телефон для связи *</strong></span><input class="form-input__field validated required" type="tel" name="phone" placeholder="+7 (___) ___-__-__">
                                <div class="validation-msg validation-msg-required">Укажите телефон</div>
                            </label>
                            <label class="form-input form-input--textarea multi-step-calculator-form__comment par">
                      <span class="form-input__label"><strong>Комментарий</strong>
                      </span>
                                <textarea class="form-input__field" name="comment"></textarea>
                            </label>
                            <button class="button par" type="submit">Записаться</button>
                        </div>
                    </form>
                    <div class="multi-step-calculator-form-success">
                        <h3 class="text-align-center">Спасибо за обращение!</h3>
                        <p class="text-align-center">Мы свяжемся с вами в ближайшее время.</p>
                    </div>
                    <button class="modal__close" type="button"></button>
                </div>
            </div>
        </div>
    </section>


<?php } ?>
