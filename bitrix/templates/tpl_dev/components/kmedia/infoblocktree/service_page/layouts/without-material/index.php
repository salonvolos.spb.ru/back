<?php
$pages = [
    '/uslugi/narashchivanie-volos/gollivudskoe/',
    '/uslugi/narashchivanie-volos/lentochnoe/',
    '/uslugi/narashchivanie-volos/holodnoe/',
    '/uslugi/narashchivanie-volos/na-korotkie/',
    '/uslugi/narashchivanie-volos/muzhskoe/',
    '/uslugi/okrashivanie-volos/dekapirovanie/',
    '/uslugi/okrashivanie-volos/brondirovanie/',
    '/uslugi/okrashivanie-volos/melirovaniye/',
    '/uslugi/okrashivanie-volos/v-odin-ton/',
    '/uslugi/okrashivanie-volos/skrytoe/',
    '/uslugi/okrashivanie-volos/muzhskoe-okrashivanie/',
    '/uslugi/narashchivanie-volos/kapsuljatsija/',
    '/uslugi/narashchivanie-volos/korrekciya/',
];
?>


<div class="prices-block__tables">
    <div class="prices-table">

        <?php if (in_array($APPLICATION->GetCurDir(), $pages) === true) { ?>
            <div class="prices-table__row prices-table__row--header">

                <div class="prices-table__cell prices-table__cell--title">
                </div>

                <?php if($arResult["UNIQUE_LENGTHS"]): ?>
                    <?php foreach ($arResult["UNIQUE_LENGTHS"] as $length): ?>
                        <div class="prices-table__cell"><?= $length["name"] ?><?php if($length["note"]): ?><div class="prices-table__clarification"><?= $length["note"] ?></div><?php endif ?></div>
                    <?php endforeach ?>
                <?php endif ?>
            </div>

        <?php } else { ?>
            <div class="prices-table__row prices-table__row--header">

                <div class="prices-table__cell prices-table__cell--title">
                    <h2>Услуги и цены</h2>
                </div>

                <?php if($arResult["UNIQUE_LENGTHS"]): ?>
                    <?php foreach ($arResult["UNIQUE_LENGTHS"] as $length): ?>
                        <div class="prices-table__cell"><?= $length["name"] ?><?php if($length["note"]): ?><div class="prices-table__clarification"><?= $length["note"] ?></div><?php endif ?></div>
                    <?php endforeach ?>
                <?php endif ?>
            </div>

        <?php } ?>

        <?php
        // Цикл по Элементам Цен
        foreach($arResult["ITEMS"] as $service): ?>
            <?php if($service["SHOW_AS_LIST"]["VALUE"] == "Y"): ?>
                <?php include __DIR__."/show-lengths-as-list.php" ?>
            <?php else: ?>
                <?php include __DIR__."/show-lengths-as-column.php" ?>
            <?php endif ?>
        <?php endforeach ?>

    </div>
</div>

<?php include __DIR__."/../../page_online_record.php" ?>
