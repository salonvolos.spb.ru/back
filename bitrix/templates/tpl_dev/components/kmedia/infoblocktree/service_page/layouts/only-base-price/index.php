<?php
$pages = [
        '/uslugi/narashchivanie-volos/kapsuljatsija/',
        '/uslugi/narashchivanie-volos/korrekciya/',
        '/uslugi/narashchivanie-volos/kapsulnoe/',
        '/uslugi/narashchivanie-volos/gollivudskoe/',
        '/uslugi/narashchivanie-volos/snyatie/',
        '/uslugi/narashchivanie-volos/lentochnoe/',
        '/uslugi/narashchivanie-volos/holodnoe/',
        '/uslugi/narashchivanie-volos/na-korotkie/',
        '/uslugi/narashchivanie-volos/muzhskoe/',

    ];
?>
<div class="prices-block__tables">
    <div class="prices-table">

        <div class="prices-table__row prices-table__row--header">
            <?php
            if (in_array($APPLICATION->GetCurDir(), $pages) === false) { ?>
                <div class="prices-table__cell prices-table__cell--title">
                    <h2>Услуги и цены</h2>
                </div>

            <?php
            } ?>
        </div>

        <?php
        foreach($arResult["ITEMS"] as $service): ?>
                <div class="prices-table__row prices-table__row--subheader prices-table__row--big-price">

                    <div class="prices-table__cell prices-table__cell--title">
                        <?= $service["NAME"] ?><?php
                        if($service["NAME_NOTE"]): ?><div class="prices-table__clarification"><?= $service["NAME_NOTE"] ?></div><?php
                        endif ?>
                    </div>

                    <?php
                    if($service["BASE_PRICE"]["VALUE"] != ""): ?>
                        <div class="prices-table__cell"><?= $service["BASE_PRICE"]["VALUE"] ?> руб.</div>
                    <?php
                    endif ?>

                </div>
        <?php
        endforeach ?>


    </div>
</div>

<?php
include __DIR__."/../../page_online_record.php"; ?>
