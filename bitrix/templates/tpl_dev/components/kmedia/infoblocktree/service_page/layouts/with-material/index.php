<div class="prices-block__tables">
    <div class="prices-table">

        <div class="prices-table__row prices-table__row--header">
            <div class="prices-table__cell prices-table__cell--title">
                <h2>Процедура</h2>
            </div>
            <? if($arResult["UNIQUE_MATERIALS"]): ?>
                <? foreach ($arResult["UNIQUE_MATERIALS"] as $material): ?>
                    <div class="prices-table__cell"><?= $material["NAME"] ?><? if($material["NOTE"]): ?><div class="prices-table__clarification"><?= $material["NOTE"] ?></div><? endif ?></div>
                <? endforeach ?>
            <? endif ?>
        </div>

        <?
        // Цикл по Элементам Цен
        foreach($arResult["ITEMS"] as $service): ?>

             <div class="
                prices-table__row
                prices-table__row--subheader
                <?= ($service["BASE_PRICE"]["VALUE"] != "") ? "prices-table__row--big-price" : "" ?>
                <?= (!empty($service["isPricesHaveDiscount"])) ? "prices-table__row--discount" : "" ?>
                <?= ($service["HOR_DIVIDER"]["VALUE"] == "Y") ? "prices-table__row--sep" : "" ?>
                "
            >
                <div class="prices-table__cell prices-table__cell--title">
                    <?= $service["NAME"] ?><? if($service["NAME_NOTE"]): ?><div class="prices-table__clarification"><?= $service["NAME_NOTE"] ?></div><? endif ?>
                </div>

                <? if($service["BASE_PRICE"]): ?>
                    <? foreach($service["BASE_PRICE"] as $basePrice): ?>
                        <div class="prices-table__cell"><?= $basePrice ?></div>
                    <? endforeach ?>
                <? endif ?>
            </div>

             <? if($service["PRICE"]): ?>
                 <?
                 // Цикл по свойству "Цена" Элементов Цен
                 foreach($service["PRICE"] as $price): ?>
                    <div class="prices-table__row">

                        <div class="prices-table__cell prices-table__cell--row-header">
                            <?= $price["name"] ?><? if($price["note"]): ?><div class="prices-table__clarification"><?= $price["note"] ?></div><? endif ?>
                        </div>

                        <? foreach ($price["values"] as $indexPrice => $value): ?>

                            <? if($value): ?>
                                <? if(!$value["discount"]): ?>
                                    <div class="prices-table__cell"><?= $value["value"] ?></div>
                                <? else: ?>
                                    <div class="prices-table__cell prices-table__cell--price-discount">
                                        <div class="prices-table__new-price"><?= $value["value"] ?></div>
                                        <div class="prices-table__old-price"><?= $value["discount"] ?></div>
                                    </div>
                                <? endif ?>
                            <? else: ?>
                                <div class="prices-table__cell"></div>
                            <? endif ?>

                        <? endforeach ?>
                    </div>
                 <? endforeach ?>
             <? endif ?>

        <? endforeach ?>
    </div>
</div>

<? include __DIR__."/../../page_online_record.php" ?>


