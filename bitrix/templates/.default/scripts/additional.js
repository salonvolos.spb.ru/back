if(typeof endOfActionJS !== "undefined"){
    $('#countdown-1').countdown({
        timestamp: endOfActionJS
    });
}

$.maskfn.definitions['~']='[+-]';
$('body').delegate('input[type="tel"], input.phone', 'focus', function(){
    $(this).maskfn('+7 (999) 999-99-99');
});

$(".master-certificates__more").on("click", function (e) {
    e.preventDefault();
    $("img", $(this).parent().parent()).show();
    $(this).detach();
});

$(".master-works__more").on("click", function (e) {
    e.preventDefault();
    $("figure", $(this).parent().parent()).show();
    $(this).detach();
});

$('form.consultation-form-submitJS').on('submitJS', function(event){
    ajaxFormSubmit({
        event:    event,
        handler:  'addFormDataConsultation',
        redirect: '/kontakty/kons/answer_ask-question.html',
        successAction: false
    });
});

$('form.hair-extention-sign-form').on('submitJS', function(event){
    ajaxFormSubmit({
        event: event,
        handler: 'addFormDataHairExtentionSignForm',
        successAction: '$(thisForm).detach(); $(".hair-extention-sign-form__success").show()',
    });
});

$('form.zapisatsya-v-list-ozhidaniya').on('submitJS', function(event){
    ajaxFormSubmit({
        event: event,
        handler: 'addFormDataZapisatsyaVListOzhidaniya',
        popupTitle: "Спасибо за вашу заявку",
        popupText: "Перезвоним вам, как только получим разрешение принимать клиентов и согласуем дату визита"
    });
});

/* Страницы Услуг - показываем ссылки на блоки "Отзывы" и "Примеры работ" только если эти блоки есть на странице */
if($(".page-block#page-feedback").length){
    $(".page-header__links #anchor_feedback").show();
}
if($(".page-block #page-works").length){
    $(".page-header__links #anchor_works").show();
}
/* /Страницы Услуг ... */

$(document).ready(function() {
    $("body").activity({
        "achieveTime": 60,
        "testPeriod": 10,
        useMultiMode: 1,
        callBack: function (e) {
            ym(38802285, "reachGoal", "60seconds");
            console.log("60 sec activity goal achieved!");
        }
    });
});