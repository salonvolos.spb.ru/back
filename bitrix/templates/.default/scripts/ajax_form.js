function ajaxFormSubmit(params)
{
    var event           = params['event'];
    var thisForm        = event.target;
    var dataHandler     = params['handler'];
    var successRedirect = params['redirect'];
    var successAction   = params['successAction'];
    var popupTitle      = params['popupTitle'];
    var popupText       = params['popupText'];
    var btnCloseText    = params['btnCloseText'];
    var message         = params['message'];

    var data = new FormData(thisForm);
    var url  = '/bitrix/templates/.default/include_areas/code/';
        url += dataHandler + '.php';

    jQuery.ajax({
        url: url,
        data: data,
        type: 'POST',  cache: false, dataType: 'json', processData: false, contentType: false,
        success: function(data){
            if (data.result == "success")
            {
                console.log(data.message);

                if(successRedirect){
                    document.location.href = successRedirect;
                }

                if(message){
                    var modalBody = $(thisForm).parents('.modal_body');
                    if(modalBody.length){
                        var messageText  = '<div class="modal__header">' + message.title + '</div>';
                            messageText += message.text;
                        var closeBtn = '<a class="btn" href="" rel="modal:close">' + message.close + '</a>';
                        modalBody.html(messageText).after(closeBtn);
                    }
                }

                if(popupText)
                {
                    var objButtonClose = false;
                    if(btnCloseText != undefined){
                    console.log(btnCloseText);
                        objButtonClose = new BX.PopupWindowButton({
                            text     : btnCloseText,
                            className: '',
                            events   : {
                                click : function(){this.popupWindow.close()}
                            }
                        })
                    }

                    var oPopup = new BX.PopupWindow('call-offer-form-wrapper', window.body, {
                        autoHide : true,
                        offsetTop : 1,
                        offsetLeft : 0,
                        lightShadow : true,
                        closeIcon : true,
                        closeByEsc : true,
                        overlay: {
                            backgroundColor: 'black', opacity: '80'
                        },
                        buttons: [objButtonClose],
                        titleBar: popupTitle
                    });

                    oPopup.setContent(popupText);
                    oPopup.show();
                    thisForm.reset();
                }

                if(successAction){
                    console.log("Evaluated Success Action");
                    eval(successAction);
                }

            }
        },
        error: function(data, textStatus, errorThrown){},
        complete: function(data, textStatus){}

    });
}






