/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/scripts/script.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/scripts/script.js":
/*!*******************************!*\
  !*** ./src/scripts/script.js ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\n$(\"[data-fancybox]\").fancybox({\n  infobar : false,\n\tarrows : false,\n  touch:false,\n  toolbar: false,\n  buttons: [\n    \"close\",\n  ],\n  beforeShow: function(){\n    $(\"body\").css({'overflow':'hidden'});\n  },\n  afterClose: function(){\n      $(\"body\").css({'overflow':'visible'});\n  }\n});\n$('.home-services__label').on('click',function () {\n  if (! $(this).parent().parent().hasClass('home-services__item_blurred')) {\n    $(this).parent().parent().addClass('home-services__item_blurred')\n  } else {\n    $(this).parent().parent().removeClass('home-services__item_blurred')\n  }\n\n})\n\nconst menu = document.getElementById('header-menu');\n// const contactsFixed = document.getElementById('header-contacts-fixed');\nconst menuNav = menu.querySelector('.header-menu__list')\nconst header = document.getElementById('header');\nconst headerMobileSwitcher = document.getElementById('header-mobile-switcher');\n\nlet fixedPoint = menu.offsetTop;\n\nwindow.addEventListener('scroll', () => {\n  let scrolled = window.pageYOffset;\n  if (document.documentElement.clientWidth > 768) {\n    if (scrolled >= fixedPoint) {\n      header.style.marginBottom = `${menu.offsetHeight}px`;\n      menu.classList.add('header-menu--fixed')\n      // contactsFixed.classList.add('header-contacts-fixed--active')\n    } else {\n      menu.classList.remove('header-menu--fixed')\n      header.style.marginBottom = 0;\n      // contactsFixed.classList.remove('header-contacts-fixed--active')\n    }\n  }\n})\n\nheaderMobileSwitcher.addEventListener('click', e => {\n  if (document.documentElement.clientWidth <= 768) {\n    menuNav.classList.toggle('header-menu__list--mobile-visible');\n    menuNav.style.width = `${document.documentElement.clientWidth}px`;\n    menuNav.style.minHeight = `${document.documentElement.clientHeight - header.offsetHeight}px`;\n    e.currentTarget.classList.toggle('header-mobile-switcher--active');\n  }\n})\n\nconst callbackButton = document.querySelectorAll('.header-contacts__callback a');\n\nfor (let item of callbackButton) {\n  item.addEventListener('click', e => {\n    e.preventDefault();\n    Comagic.openSitePhonePanel();\n  } )\n}\n\nconst onlineRecord = document.querySelectorAll('.prices-block__record-online');\n\nfor (let item of onlineRecord) {\n  item.addEventListener('click', e => {\n    e.preventDefault();\n    show_online_record('248'); return false;\n  } )\n}\n\n\n\nconst hairExtentionHeaderButtons = document.getElementsByClassName( 'page-services__button' );\nconst hairExtentionSignSubheader = document.querySelector( '.hair-extention-sign__header span' );\nconst hairExtentionSignTypeInput = document.querySelector( '.hair-extention-sign-form__input-type input' );\nconst hairExtentionSignBottomButton = document.querySelector( '.page-appointment__btn a.btn' );\nconst hairExtentionSignOnlineButton = document.querySelector( 'a.prices-block__record-online' );\n\nconsole.log(hairExtentionHeaderButtons, hairExtentionSignSubheader, hairExtentionSignTypeInput, hairExtentionSignBottomButton);\n\nconst hairExtentionButtons = Array.from(hairExtentionHeaderButtons);\n\nif(hairExtentionSignBottomButton){\n    hairExtentionButtons.push(hairExtentionSignBottomButton);\n}\n\nif(hairExtentionSignOnlineButton){\n    hairExtentionButtons.push(hairExtentionSignOnlineButton);\n}\n\nfor ( var item of hairExtentionButtons ) {\n  item.addEventListener('click', function( event ) {\n    var signWindowTypeText = this.dataset.name;\n    hairExtentionSignSubheader.innerText = signWindowTypeText;\n    if (signWindowTypeText !== 'наращивание') {\n      hairExtentionSignTypeInput.setAttribute('value', signWindowTypeText);\n    } else {\n      hairExtentionSignTypeInput.setAttribute('value', 'тип не указан');\n    }\n  });\n}\n\n\n//# sourceURL=webpack:///./src/scripts/script.js?");

/***/ })

/******/ });