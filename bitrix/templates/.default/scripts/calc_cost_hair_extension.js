$(
function()
{
    var formCalcCostExt = $(".form-calc-cost-extension");

    formCalcCostExt.on('submitJS', getCalcPriceAjax);

    function getCalcPriceAjax(event)
    {
        var url = '/bitrix/templates/.default/include_areas/code/';
        url += 'getCalcCostHairExtension.php';

        if(event.target.nodeName == "FORM"){
            var form = event.target;
        }else{
            var form = $(event.target).parents("form");
        }

        var priceMaterialsDomWrapper = $(".calculator__item_materials .hair-extention-calculator__output", form);
        if(priceMaterialsDomWrapper.length){
            var priceMaterialsDom = $("output", priceMaterialsDomWrapper);
            priceMaterialsDomWrapper.css("opacity", 0);
        }

        var priceWorksDomWrapper = $(".calculator__item_works .hair-extention-calculator__output", form);
        if(priceWorksDomWrapper.length){
            var priceWorksDom = $("output", priceWorksDomWrapper);
            priceWorksDomWrapper.css("opacity", 0);
        }

        var priceTotalDomWrapper = $(".calculator__item_total .hair-extention-calculator__output", form);
        if(priceTotalDomWrapper.length){
            var priceTotalDom = $("output", priceTotalDomWrapper);
            priceTotalDomWrapper.css("opacity", 0);
        }

        var data = $(form).serialize();
        var posting = $.post(url, data, false, "json");
        posting.done(function(data) {
            if(data.result == "success"){
                $(".hair-extention-calculator__items", form).show();

                var price = data.message;

                if(priceMaterialsDomWrapper.length){
                    priceMaterialsDom.text(price.priceMaterials.toLocaleString());
                    priceMaterialsDomWrapper.css("opacity", 1);
                }

                if(priceWorksDomWrapper.length){
                    priceWorksDom.text(price.priceWorks.toLocaleString());
                    priceWorksDomWrapper.css("opacity", 1);
                }

                if(priceTotalDomWrapper.length){
                    priceTotalDom.text(price.priceTotal.toLocaleString());
                    priceTotalDomWrapper.css("opacity", 1);
                }
            }
            console.log(data.message);
        });
    }

    // Поле: Вид наращивания
    var inputExtType = $("select[name='extension_type']", formCalcCostExt);

    // Поле: Ваша длина волос
    var inputHairLength = $("select[name='hair_length']", formCalcCostExt);

    // Поле: Стрижка
    var inputHaircut = $("select[name='haircut']", formCalcCostExt);

    // Поле: Объем (густота) своих волос
    var inputOwnHairVolume = $("select[name='own_hair_volume']", formCalcCostExt);

    // Поле: Желаемая длина
    var inputDesiredLength = $("select[name='desired_length']", formCalcCostExt);

    // Поле: Кол-во от пользователя
    var inputReadyQtyFromUser = $("input[name='ready_qty_from_user']", formCalcCostExt);

    inputExtType.on("change", function (e)
    {
        var parentForm = $(this).parents("form");
        var inputDesiredLengthLoc = $("select[name='desired_length']", parentForm);
        var desiredLengthOption80 = $("option[value='80']", inputDesiredLengthLoc);

        if(desiredLengthOption80.length){
            var extTypeVal = $(this).val();
            if(extTypeVal == "lentochnoe"){
                desiredLengthOption80.hide();
                if(inputDesiredLengthLoc.val() == "80"){
                    inputDesiredLengthLoc.val("70");
                }
            }else{
                desiredLengthOption80.show();
            }
        }
    });

    $(inputExtType).on("change", getCalcPriceAjax);
    $(inputHairLength).on("change", getCalcPriceAjax);
    $(inputHaircut).on("change", getCalcPriceAjax);
    $(inputOwnHairVolume).on("change", getCalcPriceAjax);
    $(inputDesiredLength).on("change", getCalcPriceAjax);
    $(inputDesiredLength).on("change", getCalcPriceAjax);
    $(inputReadyQtyFromUser).on("input", getCalcPriceAjax);

}
);