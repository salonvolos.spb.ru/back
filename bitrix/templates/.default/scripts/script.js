/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/scripts/script.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/scripts/script.js":
/*!*******************************!*\
  !*** ./src/scripts/script.js ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\r\n\r\n$(\"[data-fancybox]\").fancybox({\r\n  infobar : false,\r\n\tarrows : false,\r\n  touch:false,\r\n  beforeShow: function(){\r\n    $(\"body\").css({'overflow':'hidden'});\r\n  },\r\n  afterClose: function(){\r\n      $(\"body\").css({'overflow':'visible'});\r\n  }\r\n});\r\n$('.home-services__label').on('click',function () {\r\n  if (! $(this).parent().parent().hasClass('home-services__item_blurred')) {\r\n    $(this).parent().parent().addClass('home-services__item_blurred')\r\n  } else {\r\n    $(this).parent().parent().removeClass('home-services__item_blurred')\r\n  }\r\n\r\n})\r\n\r\nconst menu = document.getElementById('header-menu');\r\n// const contactsFixed = document.getElementById('header-contacts-fixed');\r\nconst menuNav = menu.querySelector('.header-menu__list')\r\nconst header = document.getElementById('header');\r\nconst headerMobileSwitcher = document.getElementById('header-mobile-switcher');\r\n\r\nlet fixedPoint = menu.offsetTop;\r\n\r\nwindow.addEventListener('scroll', () => {\r\n  let scrolled = window.pageYOffset;\r\n  if (document.documentElement.clientWidth > 768) {\r\n    if (scrolled >= fixedPoint) {\r\n      header.style.marginBottom = `${menu.offsetHeight}px`;\r\n      menu.classList.add('header-menu--fixed')\r\n      // contactsFixed.classList.add('header-contacts-fixed--active')\r\n    } else {\r\n      menu.classList.remove('header-menu--fixed')\r\n      header.style.marginBottom = 0;\r\n      // contactsFixed.classList.remove('header-contacts-fixed--active')\r\n    }\r\n  }\r\n})\r\n\r\nheaderMobileSwitcher.addEventListener('click', e => {\r\n  if (document.documentElement.clientWidth <= 768) {\r\n    menuNav.classList.toggle('header-menu__list--mobile-visible');\r\n    menuNav.style.width = `${document.documentElement.clientWidth}px`;\r\n    menuNav.style.minHeight = `${document.documentElement.clientHeight - header.offsetHeight}px`;\r\n    e.currentTarget.classList.toggle('header-mobile-switcher--active');\r\n  }\r\n})\r\n\r\nconst callbackButton = document.querySelectorAll('.header-contacts__callback a');\r\n\r\nfor (let item of callbackButton) {\r\n  item.addEventListener('click', e => {\r\n    e.preventDefault();\r\n    Comagic.openSitePhonePanel();\r\n  } )\r\n}\r\n\r\nconst onlineRecord = document.querySelectorAll('.prices-block__record-online');\r\n\r\nfor (let item of onlineRecord) {\r\n  item.addEventListener('click', e => {\r\n    e.preventDefault();\r\n    show_online_record('248'); return false;\r\n  } )\r\n}\r\n\r\n\r\n\r\nconst hairExtentionHeaderButtons = document.getElementsByClassName( 'page-services__button' );\r\nconst hairExtentionSignSubheader = document.querySelector( '.hair-extention-sign__header span' );\r\nconst hairExtentionSignTypeInput = document.querySelector( '.hair-extention-sign-form__input-type input' );\r\nconst hairExtentionSignBottomButton = document.querySelector( '.page-appointment__btn a.btn' );\r\nconst hairExtentionSignOnlineButton = document.querySelector( 'a.prices-block__record-online' );\r\n\r\nconst hairExtentionButtons = Array.from(hairExtentionHeaderButtons);\r\n\r\nif(hairExtentionSignBottomButton){\r\n    hairExtentionButtons.push(hairExtentionSignBottomButton);\r\n}\r\n\r\nif(hairExtentionSignOnlineButton){\r\n    hairExtentionButtons.push(hairExtentionSignOnlineButton);\r\n}\r\n\r\nfor ( var item of hairExtentionButtons ) {\r\n  item.addEventListener('click', function( event ) {\r\n    var signWindowTypeText = this.dataset.name;\r\n    hairExtentionSignSubheader.innerText = signWindowTypeText;\r\n    if (signWindowTypeText !== 'наращивание') {\r\n      hairExtentionSignTypeInput.setAttribute('value', signWindowTypeText);\r\n    } else {\r\n      hairExtentionSignTypeInput.setAttribute('value', 'тип не указан');\r\n    }\r\n  });\r\n}\r\n\n\n//# sourceURL=webpack:///./src/scripts/script.js?");

/***/ })

/******/ });