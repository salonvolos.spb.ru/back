 <div class="
    prices-table__row
    <?= (!$service["PARENT_PRICE"]["VALUE"]) ? "prices-table__row--subheader" : "" ?>
    <?= ($service["BASE_PRICE"]["VALUE"]) ? "prices-table__row--big-price" : "" ?>
    <?= (!empty($service["isPricesHaveDiscount"])) ? "prices-table__row--discount" : "" ?>
    <?= ($service["HOR_DIVIDER"]["VALUE"] == "Y") ? "prices-table__row--sep" : "" ?>
    "
>

    <div
        class="
            prices-table__cell
            <?= (!$service["PARENT_PRICE"]["VALUE"]) ? "prices-table__cell--title" : "prices-table__cell--row-header" ?>
        "
    >
        <? if(!$service["SERVICE_LINK"]): ?>
            <?= $service["NAME"] ?><? if($service["NAME_NOTE"]): ?><div class="prices-table__clarification"><?= $service["NAME_NOTE"] ?></div><? endif ?>
        <? else: ?>
            <a href="<?= $service["SERVICE_LINK"] ?>"><?= $service["NAME"] ?></a><? if($service["NAME_NOTE"]): ?><div class="prices-table__clarification"><?= $service["NAME_NOTE"] ?></div><? endif ?>
        <? endif ?>
    </div>

    <? if($service["BASE_PRICE"]["VALUE"] != ""): ?>
        <div class="prices-table__cell"><?= $service["BASE_PRICE"]["VALUE"] ?> руб.</div>
    <? endif ?>

    <? if($service["MATERIAL"]): ?>
        <? foreach ($service["MATERIAL"] as $material): ?>
            <div class="prices-table__cell"><?= $material["NAME"] ?><? if($material["NOTE"]): ?><div class="prices-table__clarification"><?= $material["NOTE"] ?></div><? endif ?></div>
        <? endforeach ?>
    <? endif ?>

 </div>

 <? if($service["PRICE"]): ?>
     <? // Цикл по свойству "Цена" Элементов Цен ?>
     <? foreach($service["PRICE"] as $price): ?>
        <div class="prices-table__row">

            <div class="prices-table__cell prices-table__cell--row-header">
                <?= $price["name"] ?><? if($price["note"]): ?><div class="prices-table__clarification"><?= $price["note"] ?></div><? endif ?>
            </div>

            <? foreach ($price["values"] as $value): ?>
                <? if(!$value["discount"]): ?>
                    <div class="prices-table__cell"><?= $value["value"] ?></div>
                <? else: ?>
                    <div class="prices-table__cell prices-table__cell--price-discount">
                        <div class="prices-table__new-price"><?= $value["value"] ?></div>
                        <div class="prices-table__old-price"><?= $value["discount"] ?></div>
                    </div>
                <? endif ?>
            <? endforeach ?>

        </div>
     <? endforeach ?>
 <? endif ?>
