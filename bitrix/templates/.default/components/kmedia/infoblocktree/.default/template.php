<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!empty($arResult["ERROR"])): ?>
    <p class="error">Ошибка: <?= $arResult["ERROR"] ?></p>
<? else: ?>
    <? if ($arResult["ITEMS"]): ?>
        <div class="prices-table">
            <? include __DIR__."/sections.php" ?>
        </div>
    <? else: ?>
        <p>Нет разделов цен</p>
    <? endif ?>
<? endif ?>

<? if(!empty($arResult["ROOT_SECTION_DESCRIPTION"])): ?>
    <h2>Описание</h2>
    <?= $arResult["ROOT_SECTION_DESCRIPTION"] ?>
<? endif ?>