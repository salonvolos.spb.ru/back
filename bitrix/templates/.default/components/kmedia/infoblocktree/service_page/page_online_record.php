<?
    $serviceLink = $service["SERVICE_LINK"];
    $serviceOnlineCode = $arParams["USER_PARAM_ONLINE_CODE"];
?>
<div class="prices-block__bottom" id="page_online_record">
    <a
        class="btn prices-block__record-online<? if($serviceLink != "/uslugi/narashchivanie-volos/"): ?> ms_booking<? endif ?>"
        <? if($serviceLink == "/uslugi/narashchivanie-volos/"): ?>
            href="#hair-extention-sign" data-fancybox data-name="наращивание"
        <? else: ?>
            href="#"
            <? if($serviceOnlineCode): ?>
                data-url="https://n<?= $serviceOnlineCode ?>.yclients.com/company:170918"
            <? else: ?>
               data-url="https://n371027.yclients.com/company:170918"
            <? endif ?>
        <? endif ?>
    >Записаться online</a>
    <div class="prices-block__clarification">
        При записи вы выбираете день, <br>время и мастера.
    </div>

    <div class="prices-block__phone phone_switch">+7 (812) 385-01-51</div>

    <? if($arResult["URL_TO_PRICE_SECTION"]): ?>
        <div class="prices-block__all-prices">
            <a href="<?= $arResult["URL_TO_PRICE_SECTION"] ?>">Все цены</a>
        </div>
    <? endif ?>

</div>
