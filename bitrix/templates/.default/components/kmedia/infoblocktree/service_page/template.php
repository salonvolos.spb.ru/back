<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!empty($arResult["ERROR"])): ?>
    <p class="error">Ошибка: <?= $arResult["ERROR"] ?></p>
<? else: ?>
    <? if($arResult["LAYOUT"] == "with_materials"): ?>
        <? include __DIR__."/with-material-layout/index.php" ?>
    <? elseif($arResult["LAYOUT"] == "without_materials"): ?>
        <? include __DIR__."/without-material-layout/index.php" ?>
    <? else: ?>
        <? include __DIR__."/only-base-price-layout/index.php" ?>
    <? endif ?>
<? endif ?>

