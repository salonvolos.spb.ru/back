<div class="prices-table">

    <div class="prices-table__row prices-table__row--header">
        <div class="prices-table__cell prices-table__cell--title">Услуги и цены</div>
        <? if($arResult["UNIQUE_LENGTHS"]): ?>
            <? foreach ($arResult["UNIQUE_LENGTHS"] as $length): ?>
                <div class="prices-table__cell"><?= $length["name"] ?><? if($length["note"]): ?><div class="prices-table__clarification"><?= $length["note"] ?></div><? endif ?></div>
            <? endforeach ?>
        <? endif ?>
    </div>

    <?
    // Цикл по Элементам Цен
    foreach($arResult["ITEMS"] as $service): ?>
        <? if($service["SHOW_AS_LIST"]["VALUE"] == "Y"): ?>
            <? include __DIR__."/show-lengths-as-list.php" ?>
        <? else: ?>
             <? include __DIR__."/show-lengths-as-column.php" ?>
        <? endif ?>
    <? endforeach ?>

</div>

<? include __DIR__."/../page_online_record.php" ?>