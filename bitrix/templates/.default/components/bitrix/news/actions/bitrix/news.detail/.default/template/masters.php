<? /** @var Actions\Action $action */ ?>
<? /** @var Actions\Master $master */ ?>
<div class="discount__masters">

    <h2 class="discount__subheader">Средний стаж мастеров салона — 12 лет</h2>

    <p>Мастера обучались в ведущих студиях и академиях:<strong>Coiffure, Локон, Londa, Shwarzkopf, Wella.</strong></p>
    <p>Сотрудники салона неоднократно участвовали в международном фестивале красоты<strong>&laquo;Невские берега&raquo;</strong>, крупнейшей в Европе парфюмерно-косметической выставке Intercharm.</p>

    <div class="block-wrap home-masters block-wrap_wrap ">

        <? foreach ($action->masters as $master): ?>
        <div class="block-wrap__item block-wrap__item_xl-width3 block-wrap__item_l-width3 block-wrap__item_m-width3 block-wrap__item_s-width3 home-masters__item">
            <figure>

                <? if($master->image): ?>
                <div class="home-masters__image">
                    <a href="<?= $master->url ?>">
                        <img src="<?= $master->image ?>" alt="<?= $master->title ?>" title="<?= $master->title ?>">
                    </a>
                </div>
                <? endif ?>

                <figcaption>
                    <div class="home-masters__title"><?= $master->title ?></div>
                    <? if($master->skills): ?>
                    <ul class="home-masters__activities">
                        <? foreach ($master->skills as $skill): ?>
                            <li><?= $skill ?></li>
                        <? endforeach ?>
                    </ul>
                    <? endif ?>
                    <a class="home-masters__link" href="<?= $master->url ?>">Подробнее</a>
                </figcaption>

            </figure>
        </div>
        <? endforeach ?>

    </div>
</div>
