<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$description = "";

if(!empty($arResult["PROPERTIES"]["SUBTITLE"]["~VALUE"])){
    $description = getTruncatedDataForSeo($arResult["PROPERTIES"]["SUBTITLE"]["~VALUE"]);
}

if(!empty($description)){
    $APPLICATION->SetPageProperty("description", $description);
}
