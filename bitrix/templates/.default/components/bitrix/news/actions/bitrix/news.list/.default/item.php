<? /** @var ActionsList\Action $action */  ?>
<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width6 discounts-list__item">

    <? if($action->image): ?>
    <div class="discounts-list__item-image s-hidden">
        <a href="<?= $action->url ?>">
            <img src="<?= $action->image ?>"/>
        </a>
    </div>
    <? endif ?>

    <div class="discounts-list__item-body">

        <h2 class="discounts-list__title">
            <a href="<?= $action->url ?>"><?= $action->title ?></a>
        </h2>

        <div class="discounts-list__info"><?= $action->text ?></div>

        <? /* <div class="discounts-list__expire">Акция действует до <?= $action->expired ?></div> */ ?>

        <div class="discounts-list__more">
            <a href="<?= $action->url ?>">Подробнее</a>
        </div>

    </div>

</div>
