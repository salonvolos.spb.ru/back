<? /** @var Actions\Action $action */ ?>
<div class="discount__counter">

    <h2 class="discount__subheader">До конца акции осталось</h2>

    <div class="block-wrap discount-counter block-wrap_nopadding block-wrap_wrap block-wrap_justify-center ">

        <script>
            let endOfActionJS = <?= $action->expiredTimeStamp ?> * 1000;
        </script>

        <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6 discount-counter__body">
            <div class="countdownHolder" id="countdown-1"></div>
        </div>

    </div>

</div>