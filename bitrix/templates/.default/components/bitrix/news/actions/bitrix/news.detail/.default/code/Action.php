<?
namespace Actions;

class Action
{
	public $id;
	public $image;
	public $title;
	public $subtitle;
	public $url;
	public $listUrl;

    public $begin;
    public $expiredTimeStamp;
    public $expiredAsText;
    public $expiredAsValue;

    public $description;
    public $text;
    public $isShowCounter;

    public $price;
    public $more;
    public $masters;
    public $works;

	public function __construct(array $arResult)
	{
		$this->id = $arResult["ID"];
		$this->image = $arResult["DETAIL_PICTURE"]["SRC"];
		$this->title = $arResult["PROPERTIES"]["TITLE"]["~VALUE"];
		$this->subtitle = $arResult["PROPERTIES"]["SUBTITLE"]["~VALUE"];
		$this->listUrl = $arResult["LIST_PAGE_URL"];
		$this->description = $arResult["~PREVIEW_TEXT"];
		$this->text = $arResult["~DETAIL_TEXT"];

		$this->begin = $this->getBegin($arResult["PROPERTIES"]["BEGIN"]["VALUE"]);
		$this->expiredTimeStamp = $this->getExpiredTimeStamp($arResult["PROPERTIES"]["EXPIRED"]["VALUE"]);
		$this->expiredAsText = $this->getExpiredAsText($this->expiredTimeStamp);
		$this->expiredAsValue = $this->getExpiredAsValue($this->expiredTimeStamp);
		$this->isShowCounter = $this->isShowCounter($arResult["PROPERTIES"]["HIDE_COUNTER"]["VALUE"]);

		$this->price = $arResult["PROPERTIES"]["PRICE"]["~VALUE"];
		$this->more = $this->getMore($arResult["PROPERTIES"]["MORE"]["VALUE"]);
		$this->masters = $this->getMasters($arResult["PROPERTIES"]["MASTERS"]["VALUE"]);
		$this->works = $this->getWorks($arResult["PROPERTIES"]["WORKS"]["VALUE"]);
	}

    private function getBegin($date)
    {
        return $date ? date("d.m.Y", MakeTimeStamp($date)) : null;
	}

    private function getExpiredTimeStamp($date)
    {
        return $date ? MakeTimeStamp($date) : strtotime(date("Y-m-t", time()));
	}

	private function getExpiredAsText($expiredTimeStamp)
	{
		if(!$expiredTimeStamp){
			return false;
		}

		return mb_strtolower(FormatDate("d F", $expiredTimeStamp), "utf8");
	}

	private function getExpiredAsValue($expiredTimeStamp)
	{
		if(!$expiredTimeStamp){
			return false;
		}

		return date("d.m.Y", $expiredTimeStamp);
	}

    private function isShowCounter($property)
    {
        return $property != "Y" && (strtotime($this->begin) < time());
	}

    public function getMore($elementsId)
    {
        $actions = [];

        foreach ($elementsId as $elementId){
            if(!$element = getIbElement($elementId)){
                continue;
            }

            $oReflectionClass = new \ReflectionClass(self::class);
            $action = $oReflectionClass->newInstanceWithoutConstructor();

            $action->image = getImageSrc($element->fields["PREVIEW_PICTURE"]);
            $action->url = $element->fields["DETAIL_PAGE_URL"];
            $action->title = $element->props["TITLE"]["~VALUE"];
            $action->text = $element->props["SUBTITLE"]["~VALUE"];
            $expired = $element->props["EXPIRED"]["VALUE"];
            $action->expiredAsValue = $expired ? $expired : date("t.m.Y", time()); // date("t.m.Y", time()) - конец текущего месаца

            $actions[] = $action;
        }

        return $actions;
    }

    public function getMasters($elementsId)
    {
        return array_map(function ($elementId){
            return new Master($elementId);
        }, $elementsId);
    }

    public function getWorks($elementsId)
    {
        return array_map(function ($elementId){
            return new Work($elementId);
        }, $elementsId);
    }

}