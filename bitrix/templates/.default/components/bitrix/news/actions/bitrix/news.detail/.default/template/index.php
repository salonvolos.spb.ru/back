<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true){die();} ?>
<? /** @var Actions\Action $action */ ?>
<div class="container">
    <div class="content">
        <div class="discount">

            <? include __DIR__."/image.php" ?>
            <? include __DIR__."/breadcrumbs.php" ?>

            <div class="block-wrap  block-wrap_wrap ">
                <? include __DIR__."/description.php" ?>
                <? include __DIR__."/side.php" ?>
                <div class="block-wrap__item block-wrap__item_xl-width12 block-wrap__item_l-width12 block-wrap__item_m-width12 block-wrap__item_s-width6">
                    <? include __DIR__."/works.php" ?>
                    <? include __DIR__."/more.php" ?>
                    <? include __DIR__."/masters.php" ?>
                </div>
            </div>

        </div>
    </div>
</div>