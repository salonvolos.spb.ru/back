<? /** @var Actions\Action $action */ ?>
<div class="block-wrap__item block-wrap__item_xl-width9 block-wrap__item_l-width8 block-wrap__item_m-width12 block-wrap__item_s-width6">

    <h1 class="discount__title"><?= $action->title ?></h1>

    <div class="discount__subtitle"><?= $action->subtitle ?></div>

    <? if($action->isShowCounter): ?>
        <? include __DIR__."/counter.php" ?>
    <? endif ?>

    <? if($action->description): ?>
    <div class="discount__body">
        <h2 class="discount__subheader">Описание акции</h2>
        <?= $action->description ?>
    </div>
    <? endif ?>

</div>