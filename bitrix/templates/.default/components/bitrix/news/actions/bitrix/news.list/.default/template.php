<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
include_once __DIR__."/code.php";
?>

<div class="container">
    <div class="content">

        <h1 class="margin-big">Акции и специальные предложения</h1>

        <div class="block-wrap discounts-list block-wrap_wrap ">
            <? foreach($arResult["ITEMS"] as $arItem): ?>
                <? $action = new ActionsList\Action($arItem) ?>
                <? include __DIR__."/item.php" ?>
            <? endforeach ?>

            <?= $arResult["NAV_STRING"] ?>
        </div>

    </div>
</div>
