<?
namespace ActionsList;

class Action
{
	public $id;
	public $title;
	public $image;
	public $expired;
	public $text;
	public $url;

	public function __construct($arItem)
	{
		$this->id = $arItem["ID"];
		$this->title = $arItem["PROPERTIES"]["TITLE"]["~VALUE"];
		$this->image = $this->getImage($arItem["PREVIEW_PICTURE"]);
		$this->expired = $this->getExpired($arItem["PROPERTIES"]["EXPIRED"]["VALUE"]);
		$this->text = $arItem["PROPERTIES"]["SUBTITLE"]["~VALUE"];
		$this->url = $arItem["DETAIL_PAGE_URL"];
	}

	private function getImage($picture)
	{
		return !empty($picture["SRC"]) ? $picture["SRC"] : null;
	}

	private function getExpired($date)
	{
		return $date ? $date : date("t.m.Y", time());
	}

}