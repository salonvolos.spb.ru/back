<? /** @var Actions\Action $action */ ?>
<? if($action->more): ?>
<div class="discount__more">

    <h2 class="discount__subheader">Другие акции и специальные предложения</h2>

    <div class="block-wrap discounts-list block-wrap_wrap ">

        <? /** @var Actions\Action $moreAction */ ?>
        <? foreach ($action->more as $moreAction): ?>
        <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width6 discounts-list__item">

            <? if($moreAction->image): ?>
            <div class="discounts-list__item-image s-hidden">
                <a href="<?= $moreAction->url ?>">
                    <img src="<?= $moreAction->image ?>"/>
                </a>
            </div>
            <? endif ?>

            <div class="discounts-list__item-body">

                <h2 class="discounts-list__title">
                    <a href="<?= $moreAction->url ?>"><?= $moreAction->title ?></a>
                </h2>

                <div class="discounts-list__info"><?= $moreAction->text ?></div>
                <div class="discounts-list__expire">Акция действует до <?= $moreAction->expiredAsValue ?></div>
                <div class="discounts-list__more">
                    <a href="<?= $moreAction->url ?>">Подробнее</a>
                </div>

            </div>

        </div>
        <? endforeach ?>

    </div>

    <a class="btn" href="/aktsii/">Больше акций</a>

</div>
<? endif ?>