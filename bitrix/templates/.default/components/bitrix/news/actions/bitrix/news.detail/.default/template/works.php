<? /** @var Actions\Action $action */ ?>
<? /** @var Actions\Work $work */ ?>
<? if($action->works): ?>
<div class="discount__works">

    <h2 class="discount__subheader">Наши работы</h2>

    <div class="block-wrap page-works block-wrap_wrap " id="page-works">

        <? foreach ($action->works as $work): ?>
        <div class="block-wrap__item block-wrap__item_xl-width3 block-wrap__item_l-width3 block-wrap__item_m-width3 block-wrap__item_s-width3 page-works__item">
            <figure>
                <img src="<?= $work->image ?>" alt="<?= $work->title ?>">
                <figcaption>
                    <div class="page-works__title"><?= $work->title ?></div>
                </figcaption>
            </figure>
        </div>
        <? endforeach ?>

    </div>
</div>
<? endif ?>