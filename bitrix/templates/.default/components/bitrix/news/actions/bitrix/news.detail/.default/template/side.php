<? /** @var Actions\Action $action */ ?>
<div class="block-wrap__item block-wrap__item_xl-width3 block-wrap__item_l-width4 block-wrap__item_m-width12 block-wrap__item_s-width6">

    <div class="block-marked block-marked--padding-small text-align-center" style="height: auto;">

        <p><strong>Действие акции</strong></p>

        <? if($action->begin): ?>
            <p><?= $action->begin ?> — <?= $action->expiredAsValue ?></p>
        <? else: ?>
            <p>до <?= $action->expiredAsValue ?></p>
        <? endif ?>

        <p><strong>Вы можете узнать подробности по телефону</strong></p>
        <p><a href="tel:+78123850151">+7 (812) 385-01-51</a></p>
        <p><strong>или</strong></p>
        <p><a class="btn ms_booking" href="#">Записаться в салон</a></p>

    </div>

    <? if($action->price): ?>
    <div class="block-marked block-marked--padding-small text-align-center" style="height: auto;">
        <p><strong>Стоимость</strong></p>
        <?= $action->price ?>
    </div>
    <? endif ?>

</div>