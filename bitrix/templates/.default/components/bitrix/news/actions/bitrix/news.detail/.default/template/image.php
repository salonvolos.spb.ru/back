<? /** @var Actions\Action $action */ ?>
<? if($action->image): ?>
    <div class="discount-banner">
        <img src="<?= $action->image ?>" alt="<?= $action->title ?>">
    </div>
<? endif ?>