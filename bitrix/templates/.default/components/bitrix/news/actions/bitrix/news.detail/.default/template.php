<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true){die();}
include_once __DIR__."/code/Work.php";
include_once __DIR__."/code/Master.php";
include_once __DIR__."/code/Action.php";
$action = new Actions\Action($arResult);
include_once __DIR__."/template/index.php";