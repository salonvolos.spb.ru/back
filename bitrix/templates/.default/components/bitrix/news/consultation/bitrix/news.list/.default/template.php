<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>


<div
    class="
        block-wrap__item
        block-wrap__item_xl-width9
        block-wrap__item_l-width9
        block-wrap__item_m-width9
        block-wrap__item_s-width6
    "
>

    <?
    $APPLICATION->IncludeComponent(
      "bitrix:main.include",
      "",
      Array(
            "AREA_FILE_SHOW" => "page",
            "AREA_FILE_SUFFIX" => "ask-block",
            "EDIT_TEMPLATE" => ""
      )
    );
    ?>

    <div class="consultation-list">

        <?
        foreach($arResult["ITEMS"] as $arItem):

            $properties = $arItem["DISPLAY_PROPERTIES"];

            $authorInfo = [];

            $name = $properties["FIO"]["DISPLAY_VALUE"];
            if($name){
                $authorInfo[] = $name;
            }

            $city = $properties["CITY"]["DISPLAY_VALUE"];
            if($city){
                $authorInfo[] = $city;
            }

            $age = $properties["AGE"]["DISPLAY_VALUE"];
            if($age){
                $age = $age.' '.num2word($age);
                $authorInfo[] = trim($age);
            }

            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>

            <div class="consultation-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">

                <h2 class="consultation-item__title">
                    <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
                        <?= $arItem["NAME"] ?>
                    </a>
                </h2>

                <div class="consultation-item__body">

                    <? if(!empty($arItem["PREVIEW_TEXT"])): ?>
                        <div class="consultation-item__question">
                            <?= $arItem["PREVIEW_TEXT"] ?>
                        </div>
                    <? endif ?>

                    <div class="consultation-item__info">

                        <? if($authorInfo): ?>
                            <div class="consultation-item__author">
                                <?= implode(", <br>", $authorInfo) ?>
                            </div>
                        <? endif ?>

                        <div class="consultation-item__date">
                            <?= $arItem["DISPLAY_ACTIVE_FROM"] ?>
                        </div>

                    </div>

                </div>

                <div class="consultation-item__more">
                    <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
                        Читать ответ
                    </a>
                </div>

            </div>
        <? endforeach ?>

    </div>

    <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
        <br /><?=$arResult["NAV_STRING"]?>
    <?endif;?>

</div>