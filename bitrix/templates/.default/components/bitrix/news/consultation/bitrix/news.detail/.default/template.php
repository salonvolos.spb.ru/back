<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$section = $arResult["SECTION"]["PATH"][0];
$sectionId = $section["ID"];
$sectionName = strtolower($section["NAME"]);
$sectionUrl  = $section["SECTION_PAGE_URL"];

$properties = $arResult["PROPERTIES"];

$authorInfo = [];

$name = $properties["FIO"]["VALUE"];
if($name){
    $authorInfo[] = $name;
}

$city = $properties["CITY"]["VALUE"];
if($city){
    $authorInfo[] = $city;
}

$age = $properties["AGE"]["VALUE"];
if($age){
    $age = $age.' '.num2word($age);
    $authorInfo[] = trim($age);
}
?>
<div
    class="
        block-wrap__item
        block-wrap__item_xl-width9
        block-wrap__item_l-width9
        block-wrap__item_m-width9
        block-wrap__item_s-width6
    "
>
    <? include __DIR__."/includes/question.php" ?>
    <? include __DIR__."/includes/answer.php" ?>
    <? include __DIR__."/includes/similar_questions.php" ?>
    <? include __DIR__."/includes/ask_block.php" ?>
</div>