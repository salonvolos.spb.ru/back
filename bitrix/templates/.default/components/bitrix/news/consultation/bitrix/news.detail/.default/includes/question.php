<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<div class="consultation-question">
    <div class="consultation-item">

        <h1 class="consultation-item__header">
            <?= $arResult["NAME"] ?>
        </h1>

        <div class="consultation-item__body">

            <div class="consultation-item__question">
                <?= $arResult["PREVIEW_TEXT"] ?>
            </div>

            <div class="consultation-item__info">

                <? if($authorInfo): ?>
                <div class="consultation-item__author">
                    <?= implode(", <br>", $authorInfo) ?>
                </div>
                <? endif ?>

                <div class="consultation-item__date">
                    <?= $arResult["DISPLAY_ACTIVE_FROM"] ?>
                </div>

            </div>

        </div>

    </div>
</div>

