<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if($arResult["DETAIL_TEXT"]): ?>
    <div class="consultation-answer">

        <div class="consultation-answer__author">
            <div class="consultation-answer__photo">
                <img
                    src="/images/leytes.jpg"
                    alt="Ольга, директор центра"
                >
            </div>
            <div class="consultation-answer__name">
                <span>Отвечает:</span>
                <a href="#">Ольга, директор центра</a>
            </div>
        </div>

        <div class="consultation-answer__body">
            <?= $arResult["~DETAIL_TEXT"] ?>
        </div>

        <div class="consultation-item__category">
            Категория: <a href="<?= $sectionUrl ?>"><?= $sectionName ?></a>
        </div>

    </div>
<? endif ?>
