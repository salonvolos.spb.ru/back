<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$similarQuestions = getSimilarQuestions($sectionId, $arResult["ID"], ID_IBLOCK_CONSULTATION);

if($similarQuestions): ?>
    <div class="consultation-similar">

        <h2 class="consultation-similar__header">Похожие вопросы</h2>
        <?
        foreach($similarQuestions as $question):
            $authorInfo = [];

            $name = $question["PROPERTY_FIO_VALUE"];
            if($name){
                $authorInfo[] = $name;
            }

            $city = $question["PROPERTY_CITY_VALUE"];
            if($city){
                $authorInfo[] = $city;
            }
        ?>
        <div class="consultation-similar__item">
            <div class="consultation-similar__title">
                <a href="<?= $question["DETAIL_PAGE_URL"] ?>">
                    <?= $question["NAME"] ?>
                </a>
            </div>
            <div class="consultation-similar__info">
                <? if($authorInfo): ?>
                    <div class="consultation-similar__author">
                        <?= implode(", ", $authorInfo) ?>
                    </div>
                <? endif ?>
                <div class="consultation-similar__date">
                    <?= $question["CREATED_DATE"] ?>
                </div>
            </div>
        </div>
        <?
        endforeach ?>

    </div>
<? endif ?>