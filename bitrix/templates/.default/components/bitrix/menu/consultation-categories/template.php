<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!empty($arResult)):
?>
<ul class="aside-menu">
    <?
    foreach($arResult as $arItem):
        if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) {
            continue;
        }
        ?>
        <li
            class="
                aside-menu__item
                aside-menu__item--parent
                <? if($arItem["SELECTED"]): ?>
                    aside-menu__item--active
                <? endif ?>
            "
        >
            <span class="aside-menu__item-body">
                <a href="<?=$arItem["LINK"]?>">
                    <?=$arItem["TEXT"]?>
                </a>
                <span class="aside-menu__count">
                    (<?= $arItem["PARAMS"]["ELEMENT_CNT"] ?>)
                </span>
            </span>
        </li>
    <? endforeach ?>
</ul>
<?
endif?>

