<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!empty($arResult)):
?>    <ul class="submenu">
    <? foreach($arResult as $arItem):

        if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1){
            continue;
        }
    ?>

        <? if($arItem["SELECTED"]): ?>
            <li class="submenu__item submenu__item--active">
                <a href="<?=$arItem["LINK"]?>">
                    <?=$arItem["TEXT"]?>
                </a>
            </li>
        <? else: ?>
            <li class="submenu__item">
                <a href="<?=$arItem["LINK"]?>">
                    <?=$arItem["TEXT"]?>
                </a>
            </li>
        <? endif ?>

    <? endforeach ?>
</ul>
<? endif ?>