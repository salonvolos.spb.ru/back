<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
if (!empty($arResult)): ?>

<ul class="header-menu__list">

	<? $previousLevel = 0 ?>

    <? foreach ($arResult as $arItem): ?>

		<? $isLink = false; ?>
		<? if (empty($arItem['PARAMS']['not_clickable'])): ?>
			<? $isLink = true; ?>
		<? endif ?>

		<? $isActive = "" ?>
		<? if($arItem["SELECTED"]): ?>
			<? $isActive = "header-menu__item--active" ?>
		<? endif ?>

		 <? if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel): ?>
			  <?= str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"])); ?>
		 <? endif ?>


		<? if ($arItem["IS_PARENT"]): // Родительский пункт меню ?>

			<? if ($arItem["DEPTH_LEVEL"] == 1):  //первого уровня ?>

				<li
					class="
						header-menu__item
						header-menu__item--parent
						<?= $isActive ?>
					"
				>

					<? $link = "" ?>
					<? if ($isLink) : ?>
						<? $link	= 'href="'.$arItem["LINK"].'"' ?>
					<? endif ?>

					<a <?= $link ?>>
						<?= $arItem["TEXT"] ?>
					</a>

					<ul class="header-menu__sublist">

				<? else: // // Родительский пункт меню второго и более нижн. уровней ?>

					<li
						class="
							header-menu__item
							header-menu__item--parent
							<?= $isActive ?>
						"
					>
						<a href="<?=$arItem["LINK"]?>">
							<?=$arItem["TEXT"]?>
						</a>
						<ul class="header-menu__sublist">
				 <? endif ?>

			<? else: ?>

				<? if ($arItem["DEPTH_LEVEL"] == 1):  // Простой пункт меню первого уровня ?>
					<li
						class="
							header-menu__item
							<?= $isActive ?>
						"
					>
						<a href="<?= $arItem["LINK"] ?>">
							<?= $arItem["TEXT"] ?>
						</a>
					</li>
				<? else :  // Простой пункт меню второго и более нижн. уровней ?>
					<li
						class="
							header-menu__item
							<?= $isActive ?>
						"
					>
						<a href="<?= $arItem["LINK"] ?>">
							<?= $arItem["TEXT"] ?>
						</a>
					</li>
				<? endif ?>

			<? endif ?>

			<? $previousLevel = $arItem["DEPTH_LEVEL"]; ?>

		<? endforeach ?>

		<? if ($previousLevel > 1)://close last item tags ?>
			<?= str_repeat("</ul></li>", ($previousLevel - 1)); ?>
		<?endif ?>

		<li class="header-menu__contact">
			<div class="header-contacts s-hidden m-hidden">
				<div class="header-contacts__phone header-phone phone_switch">+7 (812) 385-01-51</div>
				<div class="header-contacts__schedule">с 10 до 21 без выходных</div>
				<div class="header-contacts__callback">
					<a href="#" onclick="Comagic.openSitePhonePanel()">Заказать звонок</a>
				</div>
			</div>
		</li>

	</ul>
<? endif ?>
