<?
if(strpos($description, "#INC_AKCII#") !== false){
    $isEmbedActions = true;
//    $contentAction = file_get_contents(__DIR__."/extension/action.php");
    ob_start();
    include __DIR__."/actions.php";
    $contentAction = ob_get_contents();
    ob_end_clean();
    $description = str_replace("#INC_AKCII#", $contentAction, $description);
}

if(strpos($description, "#INC_EXTENSION_WITHOUT_KNOWLEDGE_FORM#") !== false){
    $formExtWoKnowledge = file_get_contents(__DIR__."/extension/extension_without_knowledge.php");
    $description = str_replace("#INC_EXTENSION_WITHOUT_KNOWLEDGE_FORM#", $formExtWoKnowledge, $description);
}

if(strpos($description, "#INC_EXTENSION_WITH_KNOWLEDGE_FORM#") !== false){
    $formExtWKnowledge = file_get_contents(__DIR__."/extension/extension_with_knowledge.php");
    $description = str_replace("#INC_EXTENSION_WITH_KNOWLEDGE_FORM#", $formExtWKnowledge, $description);
}

if(strpos($description, "#INC_CALLBACK_FORM#") !== false){
    $formCallback = file_get_contents(__DIR__."/extension/callback.php");
    $description = str_replace("#INC_CALLBACK_FORM#", $formCallback, $description);
}

if(strpos($description, "#INC_OUR_WORKS#") !== false){
    $isEmbedOurWorks = true;
    ob_start();
    include __DIR__."/our_works.php";
    $contentOurWorks = ob_get_contents();
    ob_end_clean();
    $description = str_replace("#INC_OUR_WORKS#", $contentOurWorks, $description);
}

if(strpos($description, "#INC_CORRECTION_FORM#") !== false){
    $formCorrection = file_get_contents(__DIR__."/extension/correction.php");
    $description = str_replace("#INC_CORRECTION_FORM#", $formCorrection, $description);
}

if(strpos($description, "#INC_REMOVAL_FORM#") !== false){
    $formRemoval = file_get_contents(__DIR__."/extension/removal.php");
    $description = str_replace("#INC_REMOVAL_FORM#", $formRemoval, $description);
}

return $description;
