<div class="grid__cell grid__cell--m--6 grid__cell--xs--12">
    <h3>Коррекция нарощенных волос</h3>
    <p>Нужна, когда вы хотите снять и снова нарастить те же самые волосы.</p>

    <form
        class="
            hair-extention-calculator
            hair-extention-calculator--rows
            form-calc-cost-extension
            validated-form
            submitJS
        "
        method="post"
    >
        <div class="hair-extention-calculator__item">
            <label class="hair-extention-calculator__label">Вид наращивания</label>
            <div class="hair-extention-calculator__input">
                <select
                    class="hair-extention-calculator__select"
                    name="extension_type"
                >
                    <option value="kapsulnoe" selected>капсульное</option>
                    <option value="lentochnoe">ленточное</option>
                </select>
            </div>
        </div>

        <div class="hair-extention-calculator__item">
            <label class="hair-extention-calculator__label">Кол-во</label>
            <div class="hair-extention-calculator__input">
                <input
                    type="text"
                    name="ready_qty_from_user"
                >
                <div class="hair-extention-calculator__description">капсул</div>
            </div>
        </div>

        <div class="hair-extention-calculator__item calculator__item_works">
            <label class="hair-extention-calculator__label">Стоимость</label>
            <div class="hair-extention-calculator__output">
                <output>0</output>
                <div class="hair-extention-calculator__description">руб.</div>
            </div>
        </div>

        <input type="hidden" name="name_of_service" value="correction">

    </form>

    <p><strong>Быстро и без вреда</strong> для волос снимем пряди с помощью специальной жидкости и щипцов.</p>
    <p>Снятые пряди распутаем, при необходимости, разберем по длине и перекапсулируем для дальнейшего наращивания.</p>

</div>
