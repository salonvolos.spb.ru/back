<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

$serviceId = $arResult["ID"];
$serviceCode = $arResult["CODE"];
$serviceDepthLevel = $arResult["DEPTH_LEVEL"];
$serviceOnlineCode = getSectionPropertyValue("UF_ONLINE_CODE", ID_IBLOCK_SERVICES, $serviceId);

global $arrFilterService;
$arrFilterService["PROPERTY_SERVICE"] = $serviceId;

$serviceBg = "";
if(!empty($arResult["PICTURE"]["SRC"])){
    $serviceBg = $arResult["PICTURE"]["SRC"];
}

$serviceName = $arResult["NAME"];
if(!empty($arResult["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"])){
    $serviceName = $arResult["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"];
}

$curPage = $APPLICATION->GetCurPage();
?><div class="inner-page container">
    <div class="content">
        <? include __DIR__."/includes/header.php" ?>
        <? include __DIR__."/includes/breadcrumbs.php" ?>
        <? include __DIR__."/includes/inner_services_menu.php" ?>
        <? include __DIR__."/includes/description.php" ?>

        <? if(!$isEmbedActions): ?>
            <? include __DIR__."/includes/actions.php" ?>
        <? endif ?>

        <? include __DIR__."/includes/prices.php" ?>

        <? if(!$isEmbedOurWorks): ?>
            <? include __DIR__."/includes/our_works.php" ?>
        <? endif ?>

        <? include __DIR__."/includes/trust_from_celebrities.php" ?>

        <? include __DIR__."/includes/masters.php" ?>
        <? include __DIR__."/includes/feedback.php" ?>
        <? include __DIR__."/includes/individual_cabinet.php" ?>
        <? include __DIR__."/includes/3d-tour.php" ?>
        <? include __DIR__."/includes/appointment.php" ?>
    </div>
</div>
