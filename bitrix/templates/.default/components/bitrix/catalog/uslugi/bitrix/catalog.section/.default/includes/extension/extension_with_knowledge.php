<div
    class="
        block-wrap__item
        block-wrap__item_xl-width6
        block-wrap__item_l-width6
        block-wrap__item_m-width6
        block-wrap__item_s-width6
    "
>
    <form
        class="
            hair-extention-calculator
            form-calc-cost-extension
            validated-form
            submitJS
        "
        method="post"
    >
        <h3 class="hair-extention-calculator__header">
            Я знаю необходимое количество материалов
        </h3>

        <div class="hair-extention-calculator__item">
            <label class="hair-extention-calculator__label">Вид наращивания</label>
            <div class="hair-extention-calculator__input">
                <select
                    class="hair-extention-calculator__select"
                    name="extension_type"
                >
                    <option value="kapsulnoe" selected>капсульное</option>
                    <option value="lentochnoe">ленточное</option>
                </select>
            </div>
        </div>

        <div class="hair-extention-calculator__item">
            <label class="hair-extention-calculator__label">Желаемая длина волос</label>
            <div class="hair-extention-calculator__input">
                <select
                    class="hair-extention-calculator__select"
                    name="desired_length"
                >
                    <option value="40">40 см</option>
                    <option value="50">50 см</option>
                    <option value="60">60 см</option>
                    <option value="70">70 см</option>
                    <option value="80">80 см</option>
                </select>
            </div>
        </div>

        <div class="hair-extention-calculator__item">
            <label class="hair-extention-calculator__label">Кол-во</label>
            <div class="hair-extention-calculator__input">
                <input
                    type="text"
                    name="ready_qty_from_user"
                    class="validated required"
                >
                <div class="validation-msg validation-msg-required">Обязательное поле</div>
            </div>
        </div>

        <div class="hair-extention-calculator__submit">
            <button class="btn" type="submit">Рассчитать стоимость</button>
        </div>

        <br>
        <div class="hair-extention-calculator__items" style="display: none;">
            <div class="hair-extention-calculator__item calculator__item_materials">
                <label class="hair-extention-calculator__label">Стоимость материалов</label>
                <div class="hair-extention-calculator__output">
                    <output>0</output>
                    <div class="hair-extention-calculator__description">руб.</div>
                </div>
            </div>
            <div class="hair-extention-calculator__item calculator__item_works">
                <label class="hair-extention-calculator__label">Стоимость работ</label>
                <div class="hair-extention-calculator__output">
                    <output>0</output>
                    <div class="hair-extention-calculator__description">руб.</div>
                </div>
            </div>
            <div class="hair-extention-calculator__item calculator__item_total">
                <label class="hair-extention-calculator__label">Общая стоимость наращивания</label>
                <div class="hair-extention-calculator__output">
                    <output>0</output>
                    <div class="hair-extention-calculator__description">руб.</div>
                </div>
            </div>
        </div>


        <input type="hidden" name="name_of_service" value="extension">

    </form>
</div>
