<div class="page-block">
    <div class="page-appointment">
        <div class="page-appointment__inner">

          <h2 class="page-appointment__header">Сложно рассчитать стоимость самостоятельно?</h2>

          <p style="margin-bottom: 1.5rem">Оставьте номер телефона, наш мастер перезвонит и поможет рассчитать стоимость услуги наращивания волос.</p>

          <a
              href="#"
              onclick="event.preventDefault(); Comagic.openSitePhonePanel()"
              class="btn"
          >Проконсультироваться</a>

        </div>
    </div>
</div>
