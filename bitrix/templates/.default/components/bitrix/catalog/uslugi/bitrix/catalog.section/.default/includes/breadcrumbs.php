<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {die();}

$SECTION_ID = $arResult["ID"];

$res = CIBlockSection::GetNavChain(
    ID_IBLOCK_SERVICES,
    $SECTION_ID
);

$parentSections = ["Главная" => "/"];

while($nav = $res->GetNext()){
    $parentSections[$nav["NAME"]] = $nav["SECTION_PAGE_URL"];
}

$curPageUrl = $APPLICATION->GetCurPage();
?>
<div class="breadcrumbs" vocab="http://schema.org/" typeof="BreadcrumbList">
    <? $index = 1; ?>
    <? foreach ($parentSections as $sectionName => $sectionUrl): ?>
    
        <? if($sectionUrl !== $curPageUrl): ?>
            <div class="breadcrumbs__parent" property="itemListElement" typeof="ListItem">
                <a href="<?= $sectionUrl ?>" title="<?= $sectionName ?>">
                    <span property="name"><?= $sectionName ?></span>
                </a>
                <meta property="position" content="<?= $index ?>">
            </div>
            <div class="breadcrumbs__separator"></div>
        <? else: ?>
            <div class="breadcrumbs__current" property="itemListElement" typeof="ListItem">
                <span property="name"><?= $sectionName ?></span>
                <meta property="position" content="<?= $index ?>">
            </div>
        <? endif ?>

        <? $index++; ?>

    <? endforeach ?>
</div>

