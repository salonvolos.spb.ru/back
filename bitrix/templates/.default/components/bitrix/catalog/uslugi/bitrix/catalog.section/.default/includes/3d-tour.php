<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	[
		"AREA_FILE_SHOW" => "page",
		"AREA_FILE_SUFFIX" => "3d-tour",
	]
);
