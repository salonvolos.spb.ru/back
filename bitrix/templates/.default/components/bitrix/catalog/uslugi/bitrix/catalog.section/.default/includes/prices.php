<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>
<div class="page-block" id="page-prices">
    <div class="prices-block">

        <?
        $APPLICATION->IncludeComponent(
            "kmedia:infoblocktree",
            "service_page",
            array(
                // фильтр: только цены привязанные к этой Услуге + с пометкой вывода на странице
                "SERVICE_ID" => $arrFilterService,
                "USER_PARAM_ONLINE_CODE" => $serviceOnlineCode,
            ),
            false
        );
        ?>
    </div>
</div>
