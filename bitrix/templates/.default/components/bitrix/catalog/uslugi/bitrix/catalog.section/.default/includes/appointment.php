<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	[
		"AREA_FILE_SHOW" => "page",
		"AREA_FILE_SUFFIX" => "appointment",
		"USER_PARAM_ONLINE_CODE" => $serviceOnlineCode,
		"USER_PARAM_SERVICE_LINK" => $sectionUrl,
	]
);
