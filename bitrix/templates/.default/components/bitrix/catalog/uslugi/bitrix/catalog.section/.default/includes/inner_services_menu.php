<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

$subSections = getSubSectionsBySectionId(ID_IBLOCK_SERVICES, $serviceId, $serviceDepthLevel);

if($subSections): ?>    <div class="page-block">
        <ul class="submenu">
            <? foreach($subSections as $subSection): ?>
                <li class="submenu__item">
                    <a href="<?= $subSection["SECTION_PAGE_URL"] ?>">
                        <?= $subSection["NAME"] ?>
                    </a>
                </li>
            <? endforeach ?>
        </ul>
    </div>
<? endif ?>