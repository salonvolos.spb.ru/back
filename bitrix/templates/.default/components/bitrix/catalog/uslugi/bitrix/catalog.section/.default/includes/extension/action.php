<?
$date = "31.08.2018";
$endOfActionTS = strtotime($date);
$endOfAction = date("d.m.Y", $endOfActionTS);
$endOfActionJS = date("Y, m, d", $endOfActionTS);
?>
<div class="page-block">
    <div class="hair-extention-banner">
        <div class="hair-extention-banner__left">
            <div class="hair-extention-banner__title">Акция</div>
            <div class="hair-extention-banner__info hair-extention-banner__info_big">
                Скидка на волосы <strong>20%</strong><br>при наращивании в нашем салоне
            </div>
            <div class="hair-extention-banner__gift">+ подарок</div>

            <? if($endOfActionJS): ?>
                <script>
                    var endOfActionJS = new Date(<?= $endOfActionJS ?>);
                    endOfActionJS.setMonth(endOfActionJS.getMonth() - 1);
                </script>
                <div class="hair-extention-banner__counter">
                    <p>до конца акции осталось:</p>
                    <div class="countdownHolder"></div>
                </div>
            <? endif ?>

        </div>

        <div class="hair-extention-banner__right">
            <p class="hair-extention-banner__byphone">Вы можете получить<br>консультацию по телефону</p>
            <p class="hair-extention-banner__phone">+7 (812) 385-01-51</p>
            <p>или закажите обратный звонок</p>
            <a
                href="#"
                onclick="event.preventDefault(); Comagic.openSitePhonePanel()"
                class="btn"
            >Заказать звонок</a>
            <? /* <form class="hair-extention-banner__form">
                <div class="hair-extention-banner__input form-input">
                    <input type="tel">
                </div>
                <div class="hair-extention-banner__submit form-submit">
                    <button type="submit">Заказать обратный звонок</button>
                </div>
            </form> */ ?>
        </div>

    </div>
</div>
