<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<div
   class="page-header"
   <? if ($serviceBg): ?>
       style="background-image: url('<?= $serviceBg ?>');"
   <? endif ?>
>

    <div class="page-header__inner">

        <h1 class="page-header__name">
            <?= $serviceName ?>
        </h1>

        <ul class="page-header__links">
            <li class="page-header__link" id="anchor_prices">
                <a href="#page-prices">Цены</a>
            </li>
            <li class="page-header__link" id="anchor_feedback">
                <a href="#page-feedback">Отзывы</a>
            </li>
            <li class="page-header__link" id="anchor_works">
                <a href="#page-works">Примеры работ</a>
            </li>
        </ul>

    </div>

</div>

