<div
    class="
        block-wrap__item
        block-wrap__item_xl-width6
        block-wrap__item_l-width6
        block-wrap__item_m-width6
        block-wrap__item_s-width6
    "
>
    <form
        class="
            hair-extention-calculator
            form-calc-cost-extension
            validated-form
            submitJS
        "
        method="post"
    >

        <h3 class="hair-extention-calculator__header">
            Я не знаю необходимое количество материалов
        </h3>

        <div class="hair-extention-calculator__item">
            <label class="hair-extention-calculator__label">Вид наращивания</label>
            <div class="hair-extention-calculator__input">
                <select
                    class="hair-extention-calculator__select"
                    name="extension_type"
                >
                    <option value="kapsulnoe" selected>капсульное</option>
                    <option value="lentochnoe">ленточное</option>
                </select>
            </div>
        </div>

        <div class="hair-extention-calculator__item">
            <label class="hair-extention-calculator__label">Ваша длина волос</label>
            <div class="hair-extention-calculator__input">
                <select
                    class="hair-extention-calculator__select"
                    name="hair_length"
                >
                    <option value="do_podborodka">до подбородка</option>
                    <option value="do_plech">до плеч</option>
                    <option value="nije_lopatok">ниже лопаток</option>
                </select>
            </div>
        </div>

        <div class="hair-extention-calculator__item">
            <label class="hair-extention-calculator__label">Стрижка</label>
            <div class="hair-extention-calculator__input">
                <select
                    class="hair-extention-calculator__select"
                    name="haircut"
                >
                    <option value="kaskadnaya">Каскадная</option>
                    <option value="gustye_konchiki">Густые кончики (одной длины)</option>
                </select>
            </div>
        </div>

        <div class="hair-extention-calculator__item">
            <label class="hair-extention-calculator__label">Объем (густота) своих волос</label>
            <div class="hair-extention-calculator__input">
                <select
                    class="hair-extention-calculator__select"
                    name="own_hair_volume"
                >
                    <option value="malyi">малый</option>
                    <option value="srednii">средний</option>
                    <option value="bolshoi">большой</option>
                </select>
            </div>
        </div>

        <div class="hair-extention-calculator__item">
            <label class="hair-extention-calculator__label">Желаемая длина волос</label>
            <div class="hair-extention-calculator__input">
                <select
                    class="hair-extention-calculator__select"
                    name="desired_length"
                >
                    <option value="40">40 см</option>
                    <option value="50">50 см</option>
                    <option value="60">60 см</option>
                    <option value="70">70 см</option>
                    <option value="80">80 см</option>
                </select>
            </div>
        </div>

        <div class="hair-extention-calculator__submit">
            <button class="btn" type="submit">Рассчитать стоимость</button>
        </div>

        <br>
        <div class="hair-extention-calculator__items" style="display: none;">
            <div class="hair-extention-calculator__item calculator__item_materials">
                <label class="hair-extention-calculator__label">Стоимость материалов</label>
                <div class="hair-extention-calculator__output">
                    <output>0</output>
                    <div class="hair-extention-calculator__description">руб.</div>
                </div>
            </div>
            <div class="hair-extention-calculator__item calculator__item_works">
                <label class="hair-extention-calculator__label">Стоимость работ</label>
                <div class="hair-extention-calculator__output">
                    <output>0</output>
                    <div class="hair-extention-calculator__description">руб.</div>
                </div>
            </div>
            <div class="hair-extention-calculator__item calculator__item_total">
                <label class="hair-extention-calculator__label">Общая стоимость наращивания</label>
                <div class="hair-extention-calculator__output">
                    <output>0</output>
                    <div class="hair-extention-calculator__description">руб.</div>
                </div>
            </div>
        </div>

        <input type="hidden" name="name_of_service" value="extension">

    </form>
</div>
