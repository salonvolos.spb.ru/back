<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {die();}

$description = "";
$isEmbedActions = false;
$isEmbedOurWorks = false;

if(!empty($arResult["DESCRIPTION"])){

    $description = $arResult["DESCRIPTION"];

    if(strpos($description, "#INC_") !== false){
        $description = include __DIR__."/description_placeholder_replace.php";
    }

    echo $description;

}