<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {die();}

$diplomasHor = false;
if(!empty($arResult["DISPLAY_PROPERTIES"]["DIPLOMA_HOR_IB"]["LINK_ELEMENT_VALUE"])){
    $diplomasHor = LinkedElements::getLinkedElements($arResult["DISPLAY_PROPERTIES"]["DIPLOMA_HOR_IB"]["LINK_ELEMENT_VALUE"]);
}

$maxDiplomasToShow = 6;

if($diplomasHor): ?>
    <h2 class="small">Сертификаты</h2>

    <div class="block-wrap master-certificates block-wrap_wrap ">
        <? foreach ($diplomasHor as $key => $diplomaHor): ?>
            <div class="block-wrap__item block-wrap__item_xl-width2 block-wrap__item_l-width2 block-wrap__item_m-width4 block-wrap__item_s-width3 master-certificates__item">

                <? if($key <= ($maxDiplomasToShow - 1)): ?>
                    <a href="<?= $diplomaHor["PICTURE_SRC"] ?>" data-fancybox="gallery-diplomas">
                        <img
                            src="<?= $diplomaHor["PICTURE_SRC"] ?>"
                            alt="<?= $diplomaHor["PREVIEW_TEXT"] ?>"
                            title="<?= $diplomaHor["DETAIL_TEXT"] ?>"
                        >
                    </a>
                <? else: ?>
                    <a href="<?= $diplomaHor["PICTURE_SRC"] ?>" data-fancybox="gallery-diplomas" style="display: none;">
                        <img
                            src="<?= $diplomaHor["PICTURE_SRC"] ?>"
                            alt="<?= $diplomaHor["PREVIEW_TEXT"] ?>"
                            title="<?= $diplomaHor["DETAIL_TEXT"] ?>"
                        >
                    </a>
                <? endif ?>

            </div>
        <? endforeach ?>
    </div>

    <div class="master-certificates__more">
        <a href="#">Больше сертификатов</a>
    </div>

<? endif ?>