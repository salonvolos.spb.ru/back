<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

// Выводятся работы, привязанные к мастеру

$linkedMasterId = $arResult["ID"];

$arOrder = ["SORT" => "ASC", "ID" => "ASC"];
$arFilter = [
    "IBLOCK_ID" => ID_IBLOCK_WORKS,
    "PROPERTY_MASTER" => $linkedMasterId,
    "ACTIVE" => "Y",
    "GLOBAL_ACTIVE" => "Y",
];
$arGroupBy = false;
$arNavStartParams = false;
$arSelect = ["ID", "IBLOCK_ID", "NAME", "PROPERTY_PHOTO", "PROPERTY_PRICE"];

$res = CIBlockElement::GetList($arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelect);

$works = [];
$photosId = [];
$photosSrc = [];

while($obj = $res->GetNext()) {
    if(!empty($works[$obj["ID"]])){ // проблема с множ. свойством PHOTO
        continue;
    }
    $works[$obj["ID"]] = $obj;
	$photosId[] = $obj["PROPERTY_PHOTO_VALUE"];
}

if($photosId){
    $res = CFile::GetList([], ["@ID" => implode(",", $photosId)]);
    while($photoArray = $res->GetNext()){
        $photosSrc[$photoArray["ID"]] = "/upload/".$photoArray["SUBDIR"]."/".$photoArray["FILE_NAME"];
    }
}

$maxMasterWorksToShow = 4;

if($works):
?>    <h2 class="small">Работы</h2>

    <div class="block-wrap master-works block-wrap_wrap ">
        <? $index = 1; ?>
        <? foreach($works as $work): ?>

            <?
            $photoSrc = false;
            if(!empty($work["PROPERTY_PHOTO_VALUE"])){
                $photoId = $work["PROPERTY_PHOTO_VALUE"];
                if(!empty($photosSrc[$photoId])){
                    $photoSrc = $photosSrc[$photoId];
                }
            }
            ?>

            <div class="block-wrap__item block-wrap__item_xl-width3 block-wrap__item_l-width3 block-wrap__item_m-width3 block-wrap__item_s-width3 master-works__item">
                <? if($index <= $maxMasterWorksToShow): ?>
                    <figure itemscope itemtype="http://schema.org/ImageObject">
                <? else: ?>
                    <figure style="display: none;" itemscope itemtype="http://schema.org/ImageObject">
                <? endif ?>

                        <? if($photoSrc): ?>
                        <a href="<?= $photoSrc ?>" data-fancybox="gallery" itemprop="contentUrl">
                            <img
                                src="<?= $photoSrc ?>"
                                alt="<?= $work["NAME"] ?>"
                                title="<?= $work["NAME"] ?>"
                                itemprop="thumbnail"
                            >
                        </a>
                        <? endif ?>

                        <figcaption  itemprop="caption">
                            <div class="master-works__title" itemprop="name">
                                <?= $work["NAME"] ?>
                            </div>
                        </figcaption>

                    </figure>
            </div>
            <? $index++; ?>
        <? endforeach ?>




        <div class="block-wrap__item block-wrap__item_xl-width12 block-wrap__item_l-width12 block-wrap__item_m-width12 block-wrap__item_s-width6">
            <a href="#" class="master-works__more">Больше работ</a>
        </div>

    </div>
<? endif ?>