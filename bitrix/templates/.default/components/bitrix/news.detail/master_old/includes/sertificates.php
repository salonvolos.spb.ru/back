<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {die();}

$diplomasHorId = false;
if(!empty($arResult["DISPLAY_PROPERTIES"]["DIPLOMA_HOR"]["VALUE"])){
    $diplomasHorId = $arResult["DISPLAY_PROPERTIES"]["DIPLOMA_HOR"]["VALUE"];
}

$diplomasHorSrc = false;
if($diplomasHorId){
    $res = CFile::GetList([], ["@ID" => implode(",", $diplomasHorId)]);
    while($photoArray = $res->GetNext()){
        $diplomasHorSrc[] = "/upload/".$photoArray["SUBDIR"]."/".$photoArray["FILE_NAME"];
    }
}

$maxDiplomasToShow = 6;

if($diplomasHorSrc): ?>
    <h2 class="small">Сертификаты</h2>

    <div class="block-wrap master-certificates block-wrap_wrap ">
        <? foreach ($diplomasHorSrc as $key => $diplomaHorSrc): ?>
        <div class="block-wrap__item block-wrap__item_xl-width2 block-wrap__item_l-width2 block-wrap__item_m-width4 block-wrap__item_s-width3 master-certificates__item">

            <? if($key <= ($maxDiplomasToShow - 1)): ?>
                <a href="<?= $diplomaHorSrc ?>" data-fancybox="gallery-diplomas">
                    <img src="<?= $diplomaHorSrc ?>">
                </a>
                <? else: ?>
                <a href="<?= $diplomaHorSrc ?>" data-fancybox="gallery-diplomas" style="display: none;">
                    <img src="<?= $diplomaHorSrc ?>">
                </a>
            <? endif ?>

        </div>
        <? endforeach ?>
    </div>

    <div class="master-certificates__more">
        <a href="#">Больше сертификатов</a>
    </div>

<? endif ?>