<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$photoSrc = false;
if(!empty($arResult["PREVIEW_PICTURE"]["SRC"])){
    $photoSrc = $arResult["PREVIEW_PICTURE"]["SRC"];
}

$skills = false;
if(!empty($arResult["DISPLAY_PROPERTIES"]["SKILLS"]["VALUE"])){
    $skills = $arResult["DISPLAY_PROPERTIES"]["SKILLS"]["VALUE"];
}

$experienceInYears = false;
if(!empty($arResult["DISPLAY_PROPERTIES"]["EMPLOYMENT_BEGIN"]["VALUE"])){
    $employmentBegin = $arResult["DISPLAY_PROPERTIES"]["EMPLOYMENT_BEGIN"]["VALUE"];
    $experienceInYears = FormatDate("Ydiff", strtotime($employmentBegin));
    $experienceInYears = explode(" ", $experienceInYears);
}

$educations = false;
if(!empty($arResult["DISPLAY_PROPERTIES"]["EDUCATION"]["VALUE"])){
    $educations = $arResult["DISPLAY_PROPERTIES"]["EDUCATION"]["VALUE"];
}

$experiences = false;
if(!empty($arResult["DISPLAY_PROPERTIES"]["EXPERIENCE"]["VALUE"])){
    $experiences = $arResult["DISPLAY_PROPERTIES"]["EXPERIENCE"]["VALUE"];
}

$additionalInfo = false;
if(!empty($arResult["DETAIL_TEXT"])){
    $additionalInfo = $arResult["DETAIL_TEXT"];
}
?>
<div class="block-wrap  block-wrap_wrap ">

    <div
        class="
            block-wrap__item
            block-wrap__item_xl-width4
            block-wrap__item_l-width4
            block-wrap__item_m-width4
            block-wrap__item_s-width6
        "
    >

        <? if($photoSrc): ?>
            <div class="master-photo">
                <img
                    src="<?= $photoSrc ?>"
                    alt="<?= $arResult["NAME"] ?>"
                    title="<?= $arResult["NAME"] ?>"
                >
                <a
                    class="btn master-photo__btn"
                    href="#"
                    onclick="show_online_record(248); return false;"
                >
                    Записаться
                </a>
            </div>
        <? endif ?>

        <div class="block-wrap  block-wrap_align-center">

            <? if($skills): ?>
                <div
                    class="
                        block-wrap__item
                        block-wrap__item_xl-width7
                        block-wrap__item_l-width7
                        block-wrap__item_m-width7
                        block-wrap__item_s-width6
                    "
                >
                    <?= implode("<br>", $skills) ?>
                </div>
            <? endif ?>

            <? if($experienceInYears): ?>
                <div
                    class="
                        block-wrap__item
                        block-wrap__item_xl-width5
                        block-wrap__item_l-width5
                        block-wrap__item_m-width5
                        block-wrap__item_s-width6
                    "
                >
                    <div class="master-experience">
                        Опыт работы
                        <div>
                            <strong><?= $experienceInYears[0] ?></strong>
                            <?= $experienceInYears[1] ?>
                        </div>
                    </div>
                </div>
            <? endif ?>

        </div>

    </div>

    <div
        class="
            block-wrap__item
            block-wrap__item_xl-width8
            block-wrap__item_l-width8
            block-wrap__item_m-width8
            block-wrap__item_s-width6
        "
    >

        <? if($educations): ?>
            <h2 class="small">Образование</h2>
            <ul class="list-marked">
                <? foreach ($educations as $education): ?>
                    <li><?= $education ?></li>
                <? endforeach ?>
            </ul>
        <? endif ?>

        <? if($experiences): ?>
            <h2 class="small">Опыт работы</h2>
            <ul class="list-marked">
                <? foreach ($experiences as $experience): ?>
                    <li><?= $experience ?></li>
                <? endforeach ?>
            </ul>
        <? endif ?>

        <? if($additionalInfo): ?>
            <h2 class="small">Дополнительная информация</h2>
            <?= $additionalInfo ?>
        <? endif ?>
    </div>

</div>