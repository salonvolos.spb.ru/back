<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>
<div class="article__body">
    <?= $arResult["PREVIEW_TEXT"] ?>
    <div class="article__date">
        <?= $arResult["DISPLAY_ACTIVE_FROM"] ?>
    </div>
</div>