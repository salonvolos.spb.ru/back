<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<div
    class="
        block-wrap
        home-masters
        block-wrap_wrap
    "
>
    <? foreach ($arResult["ITEMS"] as $arItem): ?>

        <?
        $tagId = "home-master-description--".$arItem["CODE"];

        $photoSrc = false;
        if(!empty($arItem["PREVIEW_PICTURE"]["SRC"])){
            $photoSrc = $arItem["PREVIEW_PICTURE"]["SRC"];
        }

        $skills = false;
        if(!empty($arItem["DISPLAY_PROPERTIES"]["SKILLS"]["VALUE"])){
            $skills = $arItem["DISPLAY_PROPERTIES"]["SKILLS"]["VALUE"];
        }
        ?>

        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
            <div
                class="
                    block-wrap__item
                    block-wrap__item_xl-width3
                    block-wrap__item_l-width3
                    block-wrap__item_m-width3
                    block-wrap__item_s-width3
                    home-masters__item
                "
                id="<?=$this->GetEditAreaId($arItem['ID']);?>"
            >
                <figure itemscope itemtype="http://schema.org/ImageObject">
 
                    <? if($photoSrc): ?>
                        <div class="home-masters__image">
                            <? if(!empty($arItem["PREVIEW_TEXT"])): ?>
                                <a href="#<?= $tagId ?>" data-fancybox>
                                    <img
                                        src="<?= $photoSrc ?>"
                                        alt="<?= $arItem["NAME"] ?>"
                                        title="<?= $arItem["NAME"] ?>"
                                        itemprop="contentUrl"
                                    >
                                </a>
                            <? else: ?>
                                <img
                                    src="<?= $photoSrc ?>"
                                    alt="<?= $arItem["NAME"] ?>"
                                    title="<?= $arItem["NAME"] ?>"
                                    itemprop="contentUrl"
                                >
                            <? endif ?>
                        </div>
                    <? endif ?>

                    <figcaption itemprop="caption">

                        <div class="home-masters__title" itemprop="name">
                            <?= $arItem["NAME"] ?>
                        </div>

                        <? if($skills): ?>
                            <ul class="home-masters__activities" itemprop="description">
                                <? foreach ($skills as $skill): ?>
                                <li><?= $skill ?></li>
                                <? endforeach ?>
                            </ul>
                        <? endif ?>

                        <? if(!empty($arItem["PREVIEW_TEXT"])): ?>
                            <a class="home-masters__link" href="#<?= $tagId ?>" data-fancybox>
                                Подробнее
                            </a>
                        <? endif ?>

                    </figcaption>

                </figure>
            </div>
    <? endforeach ?>
</div>

<?
// Описания для pop-up
foreach ($arResult["ITEMS"] as $arItem): ?>
    <? if(!empty($arItem["PREVIEW_TEXT"])): ?>
        <? $tagId = "home-master-description--".$arItem["CODE"] ?>
        <div class="home-master-description" id="<?= $tagId ?>">
            <div class="home-master-description__inner">
                <?= $arItem["PREVIEW_TEXT"] ?>
            </div>
        </div>
    <? endif ?>
<? endforeach ?>
