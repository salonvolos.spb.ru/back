<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {die();}

if($arResult["ITEMS"]): ?>
<div class="page-block">
    <div
        class="block-wrap page-works block-wrap_wrap"
        id="page-works"
    >

        <? if(!$arParams["USER_PARAM_IS_EMBED_OUR_WORKS"]): ?>
            <div class="block-wrap__item block-wrap__item_xl-width12 block-wrap__item_l-width12 block-wrap__item_m-width12 block-wrap__item_s-width6">
                <h2 class="page-subheader">Наши работы</h2>
            </div>
        <? endif ?>

        <?
        foreach ($arResult["ITEMS"] as $arItem):

            $photoFirstSrc = false;
            if(!empty($arItem["DISPLAY_PROPERTIES"]["PHOTO"])){
                $photoProp = $arItem["DISPLAY_PROPERTIES"]["PHOTO"];
                if(count($photoProp["VALUE"]) == 1){
                    $photoFirstSrc = $photoProp["FILE_VALUE"]["SRC"];
                }else{
                    $photoFirstSrc = $photoProp["FILE_VALUE"][0]["SRC"];
                }
            }

            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

            ?>
            <div
                class="block-wrap__item block-wrap__item_xl-width3 block-wrap__item_l-width3 block-wrap__item_m-width3 block-wrap__item_s-width3 page-works__item"
                id="<?=$this->GetEditAreaId($arItem['ID']);?>"
            >

                <figure itemscope itemtype="http://schema.org/ImageObject">

                    <? if ($photoFirstSrc): ?>
                    <a href="<?= $photoFirstSrc ?>" data-fancybox="service-works" data-caption="<?= $arItem["NAME"] ?>" itemprop="contentUrl">
                        <img src="<?= $photoFirstSrc ?>" alt="<?= $arItem["NAME"] ?>" title="<?= $arItem["NAME"] ?>" itemprop="thumbnail"/>
                    </a>
                    <? endif ?>

                    <figcaption itemprop="caption">
                        <div class="page-works__title" itemprop="name"><?= $arItem["NAME"] ?></div>
                        <div class="page-works__price" itemprop="description"><?= $arItem["DISPLAY_PROPERTIES"]["PRICE"]["VALUE"] ?></div>
                    </figcaption>

                </figure>

            </div>
        <?
        endforeach;
        ?>

    </div>

    <? if(!empty($arParams["TEXT_OUR_WORKS"])): ?>
        <p class="small"><?= $arParams["TEXT_OUR_WORKS"] ?></p>
    <? endif ?>

</div>
<? endif ?>