<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<? foreach ($arResult["ITEMS"] as $arItem): ?>

    <?
    $tagId = "home-master-description--".$arItem["CODE"];

    $photoSrc = false;
    if(!empty($arItem["PREVIEW_PICTURE"]["SRC"])){
        $photoSrc = $arItem["PREVIEW_PICTURE"]["SRC"];
    }

    $skills = false;
    if(!empty($arItem["DISPLAY_PROPERTIES"]["SKILLS"]["VALUE"])){
        $skills = $arItem["DISPLAY_PROPERTIES"]["SKILLS"]["VALUE"];
    }
    ?>

    <?
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>

    <?
    $masterOnlineCode = $arItem["DISPLAY_PROPERTIES"]["YCLIENTS_CODE"]["VALUE"];
    ?>

    <div
        class="
            block-wrap__item
            block-wrap__item_xl-width3
            block-wrap__item_l-width3
            block-wrap__item_m-width3
            block-wrap__item_s-width3
            home-masters__item
        "
        id="<?=$this->GetEditAreaId($arItem['ID']);?>"
    >
        <figure itemscope itemtype="http://schema.org/ImageObject">

            <? if($photoSrc): ?>
                <div class="home-masters__image">
                    <img
                        src="<?= $photoSrc ?>"
                        alt="<?= $arItem["NAME"] ?>"
                        title="<?= $arItem["NAME"] ?>"
                        itemprop="contentUrl"
                    >
                    <a
                        class="btn home-masters__appointment ms_booking"
                        href="#"
                        data-url="https://n371027.yclients.com/company:170918?o=m<?= $masterOnlineCode ?>"
                    >
                        Записаться
                    </a>
                </div>
            <? endif ?>

            <figcaption itemprop="caption">

                <div class="home-masters__title" itemprop="name">
                    <?= $arItem["NAME"] ?>
                </div>

                <? if($skills): ?>
                    <ul class="home-masters__activities" itemprop="description">
                        <? foreach ($skills as $skill): ?>
                        <li><?= $skill ?></li>
                        <? endforeach ?>
                    </ul>
                <? endif ?>

                <a class="home-masters__link" href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
                    Подробнее
                </a>

            </figcaption>

        </figure>
    </div>

<? endforeach ?>
