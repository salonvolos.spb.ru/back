<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {die();} ?>
<? foreach ($arResult["ITEMS"] as $arItem): ?>

    <?
        $photoSrc = false;
        if(!empty($arItem["PREVIEW_PICTURE"]["SRC"])){
            $photoSrc = $arItem["PREVIEW_PICTURE"]["SRC"];
        }

        $displayProps = $arItem["DISPLAY_PROPERTIES"];

        $title = false;
        if(!empty($displayProps["TITLE"]["VALUE"])){
            $title = $displayProps["TITLE"]["VALUE"];
        }

        $subTitle = false;
        if(!empty($displayProps["SUBTITLE"]["VALUE"])){
            $subTitle = $displayProps["SUBTITLE"]["~VALUE"];
        }

        $endOfAction = false;
        $endOfActionJS = false;
        if(!empty($arItem["ACTIVE_TO"])){
            $endOfActionTS = strtotime($arItem["ACTIVE_TO"]);
            $endOfAction = date("d.m.Y", $endOfActionTS);
            $endOfActionJS = date("Y, m, d", $endOfActionTS);
        }
    ?>

    <?
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>

    <div
        class="
            block-wrap
            discounts-list__item
            block-wrap_nopadding
            block-wrap_wrap
        "
        id="<?=$this->GetEditAreaId($arItem['ID']);?>"
    >

        <? if($photoSrc): ?>
            <div
               class="
                    block-wrap__item
                    block-wrap__item_xl-width4
                    block-wrap__item_l-width4
                    block-wrap__item_m-width4
                    block-wrap__item_s-width6
                    discounts-list__item-image
                    s-hidden
                "
            >
                <img src="<?= $photoSrc ?>"/>
            </div>
        <? endif ?>

        <div
            class="
                block-wrap__item
                block-wrap__item_xl-width8
                block-wrap__item_l-width8
                block-wrap__item_m-width8
                block-wrap__item_s-width6
                discounts-list__item-body
            "
        >
            <? if($title): ?>
                <h2 class="discounts-list__title">
                    <?= $title ?>
                </h2>
            <? endif ?>

            <? if($subTitle): ?>
                <div class="discounts-list__info">
                    <?= $subTitle ?>
                </div>
            <? endif ?>

            <? /* <? if($endOfAction): ?>
            <div class="discounts-list__expire">
                Акция действует до <?= $endOfAction ?>
            </div>
            <? endif ?> */ ?>
<!--test-->
            <? if($endOfActionJS): ?>
                <script>
                    var endOfActionJS = new Date(<?= $endOfActionJS ?>);
                    endOfActionJS.setMonth(endOfActionJS.getMonth() - 1);
                </script>
                <div class="hair-extention-banner__counter" style="margin-bottom: 30px;">
                    <p>до конца акции осталось:</p>
                    <div id="countdown-1"></div>
                </div>
            <? endif ?>

            <div class="discounts-list__more">
                <a class="btn" href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
                    Получить скидку
                </a>
            </div>

        </div>
    </div>

<? endforeach ?>
