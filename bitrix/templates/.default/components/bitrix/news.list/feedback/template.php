<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {die();}

if($arResult["ITEMS"]):
?>
<div class="page-block" id="page-feedback">
    <div class="block-wrap  block-wrap_wrap block-wrap_justify-center ">
        <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width10 block-wrap__item_s-width6">
            <div class="hair-extention-reviews">

                <h2 class="page-subheader">Отзывы клиенток</h2>

                <? foreach ($arResult["ITEMS"] as $arItem): ?>

                    <?
                        $displayProps = $arItem["DISPLAY_PROPERTIES"];

                        $isVideo = false;
                        if($displayProps["IS_VIDEO"]["VALUE"] == "Y"){
                            $isVideo = true;
                        }
                    ?>

                    <?
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                    ?>

                    <div
                        class="hair-extention-review"
                        id="<?= $this->GetEditAreaId($arItem['ID']) ?>"
                    >
                        <div class="hair-extention-review__title">
                            <?= $arItem["NAME"] ?>
                        </div>

                        <? if($isVideo): ?>
                            <div class="iframe-responsive">
                                <iframe
                                    src="<?= $arItem["~PREVIEW_TEXT"] ?>"
                                    allowfullscreen=""
                                ></iframe>
                            </div>
                        <? else: ?>
                            <div class="plain-text">
                                <?= $arItem["PREVIEW_TEXT"] ?>
                            </div>
                        <? endif ?>

                    </div>

                <? endforeach ?>

            </div>
        </div>
    </div>
</div>
<? endif ?>