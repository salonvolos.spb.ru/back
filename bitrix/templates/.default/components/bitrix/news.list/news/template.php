<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<div class="articles-list">
    <?foreach($arResult["ITEMS"] as $arItem):?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <div
            class="articles-list__item"
            id="<?= $this->GetEditAreaId($arItem['ID']) ?>"
        >
            <h2 class="articles-list__title">
                <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
                    <?= $arItem["NAME"] ?>
                </a>
            </h2>
            <div class="articles-list__date">
                <?= $arItem["DISPLAY_ACTIVE_FROM"] ?>
            </div>
        </div>
    <? endforeach ?>

    <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
        <br /><?=$arResult["NAV_STRING"]?>
    <?endif;?>

</div>