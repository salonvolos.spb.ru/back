<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
} ?>
<nav class="top-menu container s-hidden">
	<div class="content">

		<ul class="top-menu__list">
			<li class="top-menu__item">
				<a href="http://www.hairdesign.ru/"><span>Замещение волос</span></a>
			</li>
			<li class="top-menu__item top-menu__item_active">
				<div class="a-repl"><span>Салон красоты</span></div>
			</li>
			<li class="top-menu__item">
				<a href="http://www.vsepariki.com"><span>Парики и<br>головные уборы</span></a>
			</li>
			<li class="top-menu__item">
				<a href="http://triholog.org/"><span>Лечебная косметика<br>для волос</span></a>
			</li>
			<li class="top-menu__item">
				<a href="http://zagustitelvolos.com"><span>Загустители волос</span></a>
			</li>
		</ul>

	</div>
</nav>
