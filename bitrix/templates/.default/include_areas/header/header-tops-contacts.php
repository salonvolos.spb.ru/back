<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
} ?><div class="header-contacts s-hidden m-hidden">
	<div class="header-contacts__phone header-phone phone_switch">+7 (812) 385-01-51</div>
	<div class="header-contacts__schedule">пн-пт: с 10:00 до 21:00 <br> сб-вс: с 10:00 до 19:00</div>
	<div class="header-contacts__callback">
		<a href="#" onclick="Comagic.openSitePhonePanel()">Заказать звонок</a>
	</div>
</div>