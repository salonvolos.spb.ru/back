<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}?>
<div class="header-menu" id="header-menu">
<?
$APPLICATION->IncludeComponent("bitrix:menu", "horizontal_multilevel-desktop", Array(
    "ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
        "MAX_LEVEL" => "2",	// Уровень вложенности меню
        "CHILD_MENU_TYPE" => "top_sub",	// Тип меню для остальных уровней
        "USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
        "DELAY" => "N",	// Откладывать выполнение шаблона меню
        "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
        "MENU_CACHE_TYPE" => "N",	// Тип кеширования
        "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
        "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
        "MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
        "COMPONENT_TEMPLATE" => "horizontal_multilevel-desktop"
    ),
    false
);
?>
</div>

