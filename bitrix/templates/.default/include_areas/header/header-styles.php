<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

$styleFileNames = [
    'style'
];

$templateStylesPath = "/bitrix/templates/.default/styles/";
$mark = "1626290732107";

foreach ($styleFileNames as $stylesFileName): ?>	<? $cssPath = $templateStylesPath.$stylesFileName.".css?".$mark; ?>
	<link href="<?= $cssPath ?>" rel="stylesheet">
<? endforeach ?>
