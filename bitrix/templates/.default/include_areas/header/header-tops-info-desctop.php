<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
} ?>
<div class="header-info s-hidden">
	<div class="header-info__name">
		Салон красоты в Санкт-Петербурге – <b>Hair&nbsp;Сare&nbsp;Center</b>
	</div>
	<div class="header-info__address">
		<div class="header-info__street">набережная Обводного&nbsp;канала&nbsp;108</div>
		<div class="header-metro">
			<div class="header-metro__item header-metro__item_metro-1">Балтийская &mdash; 10 минут</div>
			<div class="header-metro__item header-metro__item_metro-2">Фрунзенская &mdash; 5 минут</div>
		</div>
	</div>
	<div class="header-phone xl-hidden l-hidden phone_switch">+7 (812) 385-01-51</div>
</div>

