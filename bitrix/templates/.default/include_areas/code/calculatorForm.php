<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if(empty($_POST)) {
    die("No data in request");
}

$iblockId = ID_IBLOCK_CONSULTATION; // Инфоблок "Консультация"
$siteCode = "s1";
$emailEventName = "CONSULTATION_FORM_SEND";


//init data

$phone = htmlspecialcharsbx($_POST['phone']);
$comment = htmlspecialcharsbx($_POST['comment']);

$city = htmlspecialcharsbx($_POST['city']);





$returnMsg = "New Iblock Item successfully added ";

if($GLOBALS['prod']) {


    $arEventFields = array(
        'PHONE' => $phone,
        'COMMENTS' => $comment
    );

    $resSend = CEvent::Send($emailEventName, $siteCode, $arEventFields);
    $returnMsg .= "; Message send: " . $resSend;
}

die(json_encode([
    "result" => "success",
    "message" => $returnMsg
]));
