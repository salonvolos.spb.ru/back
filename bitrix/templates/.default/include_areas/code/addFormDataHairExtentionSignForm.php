<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if(empty($_POST)) {
    die("No data in request");
}

$iblockId = ID_IBLOCK_SIGN_IN_HAIR_EXTENSION; // Инфоблок "Запись на наращивание"
$siteCode = "s1";
$emailEventName = "NEW_SIGN_IN_HAIR_EXTENSION";

//init data
$phone = htmlspecialcharsbx($_POST['phone']);
$method = htmlspecialcharsbx($_POST['extention_method']);
$reqName = htmlspecialcharsbx($_POST['req_name']);

// AntiSpam
if($reqName){
    die(json_encode([
        "result" => "error",
        "message" => "Ошибка данных формы"
    ]));
}

$dateForUser = date('Y/m/d H:i:s');
$dateForCode = date('Y-m-d-H-i-s');

CModule::IncludeModule('iblock');

if($phone){
    $topicInit = 'Новый запись от';
    $topicForUser = $topicInit.' '.$dateForUser;
    $topicForCode = $topicInit.'_'.$dateForCode;
}else{
    die(json_encode([
        "result" => "error",
        "message" => "Phone is empty"
    ]));
}

//code generation
$arParamsCode = array(
   "max_len" => 255,
   "change_case" => "L",
   "replace_space" => '-',
   "replace_other" => '-',
   "delete_repeat_replace" => true
);
$code = CUtil::translit($topicForCode, "ru", $arParamsCode);

$element = new CIBlockElement;

$elementProps = [
    "PHONE" => $phone,
    "METHOD" => $method,
];

$arElementFields = Array(
    "IBLOCK_ID" => $iblockId,
    "PROPERTY_VALUES" => $elementProps,
    "NAME" => $topicForUser,
    "CODE" => $code,
    "ACTIVE" => "N",
    "DATE_ACTIVE_FROM" => ConvertTimeStamp(time(), "FULL")
);

$newIblockItemId = $element->Add($arElementFields);

if (!$newIblockItemId) {
    die(json_encode([
        "result" => "error",
        "message" => $element->LAST_ERROR
    ]));
}

$returnMsg = "New Iblock Item successfully added (id: $newIblockItemId)";

if($GLOBALS['prod']) {
    $backend_link = 'http://'.SITE_SERVER_NAME.'/bitrix/admin/iblock_element_edit.php';
    $backend_link .= '?IBLOCK_ID='.$iblockId.'&type=consultation&ID='.$newIblockItemId;

    $arEventFields = array(
        "DATE" => $dateForUser,
        "ID" => $newIblockItemId,
        "PHONE" => $phone,
        "METHOD" => $method,
        "BE_LINK" => $backend_link
    );

    $resSend = CEvent::Send($emailEventName, $siteCode, $arEventFields);
    $returnMsg .= "; Message send: " . $resSend;
}

die(json_encode([
    "result" => "success",
    "message" => $returnMsg
]));