<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if(empty($_POST)) {
    die("No data in request");
}

$iblockId = ID_IBLOCK_CONSULTATION; // Инфоблок "Консультация"
$siteCode = "s1";
$emailEventName = "NEW_QUESTION";

//init data
$question = htmlspecialcharsbx($_POST['question']);
$fio = htmlspecialcharsbx($_POST['fio']);
$email = htmlspecialcharsbx($_POST['email']);
$phone = htmlspecialcharsbx($_POST['phone']);
$city = htmlspecialcharsbx($_POST['city']);

$questionToName = false;

$dateForUser = date('Y/m/d H:i:s');
$dateForCode = date('Y-m-d-H-i-s');

CModule::IncludeModule('iblock');

if($question){
    $topicInit = 'Новый вопрос от ';
    $topicForUser = $topicInit.' '.$dateForUser;
    $topicForCode = $topicInit.'_'.$dateForCode;
}else{
    die(json_encode([
        "result" => "error",
        "message" => "Question is empty"
    ]));
}

//code generation
$arParamsCode = array(
   "max_len" => 255,
   "change_case" => "L",
   "replace_space" => '-',
   "replace_other" => '-',
   "delete_repeat_replace" => true
);
$code = CUtil::translit($topicForCode, "ru", $arParamsCode);

$element = new CIBlockElement;

$elementProps = [
    "FIO" => $fio,
    "PHONE" => $phone,
    "EMAIL" => $email,
    "CITY" => $city,
];

$arElementFields = Array(
    "IBLOCK_ID" => $iblockId,
    "PROPERTY_VALUES" => $elementProps,
    "NAME" => $topicForUser,
    "PREVIEW_TEXT" => $question,
    "CODE" => $code,
    "ACTIVE" => "N",
    "DATE_ACTIVE_FROM" => ConvertTimeStamp(time(), "FULL")
);

$newIblockItemId = $element->Add($arElementFields);

if (!$newIblockItemId) {
    die(json_encode([
        "result" => "error",
        "message" => $element->LAST_ERROR
    ]));
}

$returnMsg = "New Iblock Item successfully added (id: $newIblockItemId)";

if($GLOBALS['prod']) {
    $backend_link = 'http://'.SITE_SERVER_NAME.'/bitrix/admin/iblock_element_edit.php';
    $backend_link .= '?IBLOCK_ID='.$iblockId.'&type=consultation&ID='.$newIblockItemId;

    $arEventFields = array(
        "NAME" => $fio,
        "DATE" => $dateForUser,
        "ID" => $newIblockItemId,
        "PHONE" => $phone,
        "EMAIL" => $email,
        "CITY" => $city,
        "COMMENTS" => $question,
        "BE_LINK" => $backend_link
    );

    $resSend = CEvent::Send($emailEventName, $siteCode, $arEventFields);
    $returnMsg .= "; Message send: " . $resSend;
}

die(json_encode([
    "result" => "success",
    "message" => $returnMsg
]));