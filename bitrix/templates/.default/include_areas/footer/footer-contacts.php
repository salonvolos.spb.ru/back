<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
} ?><div
   class="block-wrap__item block-wrap__item_xl-width3 block-wrap__item_l-width3 block-wrap__item_m-width12 block-wrap__item_s-width6">
    <div class="footer-contacts">
        <div class="footer-phone phone_switch s-hidden xs-hidden" style="margin-bottom: 1rem; line-height: 1.125">+7 (812) 385-01-51</div>
		<div class="footer-phone phone_switch xl-hidden l-hidden m-hidden"><a href="tel:+7-812-385-01-51">+7 (812) 385-01-51</a></div>
        <div class="footer-socials ">
            <div class="footer-socials__subheader" style="font-size: .875rem">Напишите нам</div>
            <div class="link-icons">
              <a class="link-icons__link link-icons__link--whatsapp" href="whatsapp://send?phone=79817245628&text=" target="_blank"></a>
              <a class="link-icons__link link-icons__link--viber" href="viber://chat?number=79817245628" target="_blank"></a>
              <a class="link-icons__link link-icons__link--telegram" href="tg://resolve?domain=HairCareCenter" target="_blank"></a>
              <a class="link-icons__link link-icons__link--vk" href="https://vk.com/club2142527" target="_blank"></a>
            </div>
        </div>
    </div>
</div>
