<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
} ?><div
   class="block-wrap__item block-wrap__item_xl-width3 block-wrap__item_l-width3 block-wrap__item_m-width4 block-wrap__item_s-width6">
   <ul class="footer-menu">
       <li class="footer-menu__item">
           <a href="/uslugi/narashchivanie-volos/">Наращивание волос</a>
       </li>
       <li class="footer-menu__item">
           <a href="/uslugi/uhod/">Уход и лечение</a>
       </li>
       <li class="footer-menu__item">
           <a href="/uslugi/makiyazh/">Макияж</a>
       </li>
   </ul>
</div>
