<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
} ?>
<div
   class="block-wrap__item block-wrap__item_xl-width9 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6">
    <div class="footer-bottom__copy">
        © Салон красоты HairCare Center 2006 - <?= date("Y"); ?>
        <br>
        <a href="/soglasheniya-na-obrabotku-personalnykh-dannykh/">Соглашения на обработку персональных данных</a>
    </div>
</div>
