<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
} ?><div
   class="block-wrap__item block-wrap__item_xl-width3 block-wrap__item_l-width3 block-wrap__item_m-width4 block-wrap__item_s-width6">
    <ul class="footer-menu">
        <li class="footer-menu__item">
            <a href="/uslugi/zavivka-volos/">Завивка волос</a>
        </li>
        <li class="footer-menu__item">
            <a href="/uslugi/vypryamlenie-volos/">Выпрямление волос</a>
        </li>
        <li class="footer-menu__item">
            <a href="/uslugi/okrashivanie-volos/">Окрашивание волос</a>
        </li>
    </ul>
</div>
