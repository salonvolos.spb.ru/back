<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
} ?>
<div
   class="
    block-wrap__item
    block-wrap__item_xl-width3
    block-wrap__item_l-width4
    block-wrap__item_m-width4
    block-wrap__item_s-width6
    "
>
    <div class="footer-bottom__author" style="max-width: fit-content; float: right;">
        <span>
            <a href="https://medreclama.ru" target="_blank">Реклама косметологии</a>
        </span>
        Агентство «Medreclama»
    </div>
</div>
