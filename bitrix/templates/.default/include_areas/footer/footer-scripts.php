<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {die();}

$scriptFileNames = [
    'jquery-3.2.1.min',
    'jquery.fancybox.min',
    'jquery.countdown',
    'jquery.flexslider-min',
    'jquery.maskedinput',
    'jquery.activity.min',
    'form_validation',
    'ajax_form',
    'calc_cost_hair_extension',
    'script',
    'additional',
];

if(isHomePage()){
    $scriptFileNames[] = "lazy_loading";
}

$templateScriptsPath = "/bitrix/templates/.default/scripts/";
$mark = "1511784816";

foreach ($scriptFileNames as $scriptFileName): ?>
	<? $scriptPath = $templateScriptsPath.$scriptFileName.".js?".$mark; ?>
	<script src="<?= $scriptPath ?>"></script>
<? endforeach ?>
<!-- <a class="ms_booking" href="#" data-url="https://n371027.yclients.com/company:170918?o=m951517" style="float:right;">Записаться</a> -->
<script type="text/javascript" src="https://w371027.yclients.com/widgetJS" charset="UTF-8"></script>

<link href="/bitrix/templates/.default/styles/jquery.fancybox.min.css" rel="stylesheet">
<link href="/bitrix/templates/.default/styles/add.css" rel="stylesheet">
