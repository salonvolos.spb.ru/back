<?php
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)die();

if($_SERVER['REMOTE_ADDR'] != "127.0.0.1") {
	$GLOBALS['prod'] = true;
}

define('ID_IBLOCK_SERVICES', 1);
define('ID_IBLOCK_FEEDBACKS', 4);
define('ID_IBLOCK_PRICES', 2);
define('ID_IBLOCK_WORKS', 8);
define('ID_IBLOCK_CONSULTATION', 9);
define('ID_IBLOCK_OVERRIDE_SERVICE_PRICE_PROPS', 11);
define('ID_IBLOCK_SIGN_IN_HAIR_EXTENSION', 12);


$curPage = $APPLICATION->GetCurPage();

require __DIR__."/class_linked_elements.php";
require __DIR__."/event_handlers/BeforeIBlockElementAdd.php";

include __DIR__. "/include/functions.php";
include __DIR__. "/include/bx-functions.php";
include __DIR__. "/include/pages.php";
include __DIR__. "/include/Images/DimensionHelper.php";
include __DIR__. "/include/Images/WebPHelper.php";
include __DIR__. "/include/Images/ResizeHelper.php";

AddEventHandler('main', 'OnEpilog', '_Check404Error', 1);
function _Check404Error()
{
    if (defined('ERROR_404') && ERROR_404 == 'Y') {
        global $APPLICATION;
        $APPLICATION->RestartBuffer();
        include $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/header.php';
        include $_SERVER['DOCUMENT_ROOT'] . '/404.php';
        include $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/footer.php';
    }
}
