<?
AddEventHandler("iblock", "OnBeforeIBlockElementAdd", Array("OnBeforeIBlockElementAddClass", "process"));

class OnBeforeIBlockElementAddClass
{
    public function process(&$arFields)
    {
        if($arFields["IBLOCK_ID"] != ID_IBLOCK_PRICES){
            return;
        }

        if(empty($arFields["IBLOCK_SECTION"])){
            return;
        }

        $sectionId = $arFields["IBLOCK_SECTION"][0];
        $sectionCode = self::getSectionCode($sectionId);

        $arFields["CODE"] = $sectionCode."__".$arFields["CODE"];
    }

    private static function getSectionCode($sectionId)
    {
        $arOrder = ["ID" => "ASC"];
        $arFilter = ["ID" => $sectionId];
        $bIncCnt = false;
        $Select = ["CODE"];
        $NavStartParams = false;

        $rsSect = CIBlockSection::GetList($arOrder, $arFilter, $bIncCnt, $Select, $NavStartParams);

        $arSect = $rsSect->Fetch();

        if(!$arSect){
            return false;
        }

        return $arSect["CODE"];
    }
}
