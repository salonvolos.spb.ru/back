<?
function blockInclude($section, $filename, $simpleInclude = true)
{
	$includePath  = '/bitrix/templates/.default/include_areas/';
	$includePath .= $section.'/'.$filename.'.php';

	global $APPLICATION;

	if($simpleInclude){
		include($_SERVER["DOCUMENT_ROOT"].$includePath);
	}else{
		$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			"",
			array(
				"AREA_FILE_SHOW" => "file",
				"PATH" => $includePath
			),
			false
		);
	}

	return false;
}

function getPageWrapperClass()
{
	global $APPLICATION;
	$result = '';
	if($pageWrapperClass = $APPLICATION->GetPageProperty('page-wrapper-class')){
		$result = ' '.$pageWrapperClass;
	}
	return $result;
}


function getIbElement($elementId, $isOnlyFields = false)
{
	$res = \CIBlockElement::GetList([], ["ID" => $elementId]);

	if(!$bxElement = $res->GetNextElement()){
		return null;
	}

	$element = new stdClass();
	$element->fields = $bxElement->GetFields();

	if($isOnlyFields){
		return $element->fields;
	}

	$element->props = $bxElement->GetProperties();

	return $element;
}

function getIbElementFields($elementId)
{
	return getIbElement($elementId, true);
}

function getImageSrc($imageId)
{
	return !empty($imageId) ? \CFile::GetPath($imageId) : null;
}

function getSectionPropertyValue($fieldCode, $iblockId, $sectionId)
{
	$ufArResult = CIBlockSection::GetList(
		[],
		[
			"IBLOCK_ID" => $iblockId,
			"ID" => $sectionId
		],
		false,
		[$fieldCode]
	);

	if($ufValue = $ufArResult->GetNext()){
		if(!empty($ufValue[$fieldCode])){
			return $ufValue[$fieldCode];
		}
	}

	return false;
}

function generateCode($subject)
{
	$params = [
		"max_len" => 255,
		"change_case" => "L",
		"replace_space" => '-',
		"replace_other" => '-',
		"delete_repeat_replace" => true
	];

	return CUtil::translit($subject, "ru", $params);
}

function isUserDeveloper()
{
	global $USER;

	$allowedAdmins = ["rt-webdesign", "dmitriy.m", "zarya"];
	if(!in_array($USER->GetLogin(), $allowedAdmins)){
		return false;
	}

	return true;
}

function getSubSectionsBySectionId($iblockId, $sectionId, $sectionDepthLevel = false)
{
	$arOrder = ["SORT" => "DESC"];
	$arFilter = [
		"IBLOCK_ID" => $iblockId,
		"SECTION_ID" => $sectionId,
		"ACTIVE" => "Y",
		"GLOBAL_ACTIVE" => "Y",
	];
	if($sectionDepthLevel){
		$arFilter ["DEPTH_LEVEL"] = $sectionDepthLevel + 1;
	}
	$bIncCnt = false;
	$Select = [
		"ID",
		"NAME",
		"IBLOCK_ID",
		"SECTION_PAGE_URL"
	];
	$NavStartParams = false;

	$res = CIBlockSection::GetList($arOrder, $arFilter, $bIncCnt, $Select, $NavStartParams);

	$subSections = [];
	while($sectionObj = $res->GetNext()) {
		$subSections[] = $sectionObj;
	}

	return $subSections;
}

function getSimilarQuestions($sectionId, $questionId, $iblockId, $count = 2)
{
	$arOrder = ["RAND" => "ASC"];
	$arFilter = [
		"IBLOCK_ID" => $iblockId,
		"ACTIVE" => "Y",
		"GLOBAL_ACTIVE" => "Y",
		"SECTION_ID" => $sectionId,
		"!ID" => $questionId,
	];
	$arGroupBy = false;
	$arNavStartParams = ["nTopCount" => $count];
	$arSelectFields = [
		"ID",
		"IBLOCK_ID",
		"NAME",
		"CREATED_DATE",
		"DETAIL_PAGE_URL",
		"PROPERTY_FIO",
		"PROPERTY_CITY",
		"PROPERTY_AGE",
	];

	$res = CIBlockElement::GetList($arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields);

	$questions = [];
	while($question = $res->GetNext()) {
		$questions[] = $question;
	}

	return $questions;
}
