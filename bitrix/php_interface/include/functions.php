<?
function filterCode($code)
{
	$codeExplode = explode('/', $code);
	$code = end($codeExplode);
	$codeExplode = explode('?', $code);
	$code = reset($codeExplode);

	return $code;
}

function getTruncatedDataForSeo($initText, $charsLimit = 250, $addSuspension = true)
{
	$initText = trim($initText);
	$text = $initText;

	$isLonger = false;
	if(mb_strlen($text) > $charsLimit){
		$isLonger = true;
	}

	$text = strip_tags($text);
	$text = str_replace(["\n", "\r", "\n\r", "\r\n", "\t"], '', $text);

	$text = html_entity_decode($text);
	$text = trim($text);
	$text = preg_replace('/[ ]{2,}/', " ", $text);

	if(mb_strlen($text) > $charsLimit){
		$text = mb_substr($text, 0, $charsLimit, 'UTF-8');
		$position = mb_strrpos($text, ' ', 'UTF-8');
		$text = mb_substr($text, 0, $position, 'UTF-8');
	}

	$text = rtrim($text, '.,:-—');
	$text = trim($text);

	if($isLonger && $addSuspension){
		$text .= '&nbsp;&hellip;';
	}

	return $text;
}

function camelize($input)
{
	return str_replace('_', '', ucwords($input, '_'));
}

function num2word($num)
{
	// пришла строка
	if (!preg_match('/^[\d]+$/', $num)){
		return false;
	}

	$words = array('год', 'года', 'лет');

	$num = $num % 100;
	if ($num > 19) {
		$num = $num % 10;
	}
	switch ($num) {
		case 1: {
			return($words[0]);
		}
		case 2: case 3: case 4: {
		return($words[1]);
	}
		default: {
			return($words[2]);
		}
	}
}

function mb_lcfirst($str, $encoding = "UTF-8")
{
	$first_letter = mb_strtolower(mb_substr($str, 0, 1, $encoding), $encoding);
	$str_end = mb_substr($str, 1, mb_strlen($str, $encoding), $encoding);
	$str = $first_letter . $str_end;
	return $str;
}

function L($Message)
{
	if (is_array($Message)) {
		$Message = print_r($Message,1);
	}
	$file_path = $_SERVER['DOCUMENT_ROOT'].'/!log.txt';
	$handle = fopen($file_path, 'a+');
	@flock($handle, LOCK_EX);
	fwrite($handle, '['.date('d.m.Y H:i:s').'] '.$Message."\r\n");
	@flock($handle, LOCK_UN);
	fclose($handle);
}
