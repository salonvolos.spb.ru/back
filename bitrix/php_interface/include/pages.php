<?
function getCurPage()
{
	global $APPLICATION;
	$curPage = explode('?', $APPLICATION->GetCurPage());
	$curPage = reset($curPage);

	return $curPage;
}

function isHomePage()
{
	$curPage = getCurPage();
	return $curPage == "/";
}

function isInnerPage()
{
	return !isHomePage();
}

function isServicesPage()
{
    $curPage = getCurPage();
    return strpos($curPage, "/uslugi/") !== false;
}

function isActionPage()
{
    $curPage = getCurPage();
    return strpos($curPage, "/aktsii/") !== false && count(array_filter(explode('/', $curPage))) > 1;
}

function isMasterPage()
{
    $curPage = getCurPage();
    return strpos($curPage, "/mastera/") !== false && strpos($curPage, "html") !== false;
}

function isConsultationDetailPage()
{
    $curPage = getCurPage();
    return strpos($curPage, "/kons/") !== false && strpos($curPage, "html") !== false;
}

function isShowBreadcrumbs()
{
    if(isHomePage()){return false;}
    if(isServicesPage()){return false;}
    if(isActionPage()){return false;}

    return true;
}

function isShowTitle()
{
	if(isHomePage()){return false;}
	if(isServicesPage()){return false;}
	if(isMasterPage()){return false;}
	if(isActionPage()){return false;}
	if(isConsultationDetailPage()){return false;}
	return true;
}
