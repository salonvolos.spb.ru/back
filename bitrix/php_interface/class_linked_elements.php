<?
class LinkedElements
{
    public static function getLinkedElements($linkedItems)
    {
        $picturesId = self::getLinkedElementsPicturesId($linkedItems);
        $picturesRef = self::getLinkedElementsPicturesRef($picturesId);
        $linkedItems = self::addToLinkedElementsRealPicturesPath($linkedItems, $picturesRef);
        return self::makeLinkedElements($linkedItems);
    }

    private static function getLinkedElementsPicturesId($linkedItems)
    {
        $picturesId = [];
        foreach($linkedItems as $linkedItem){
            if(!empty($linkedItem["PREVIEW_PICTURE"])){
               $picturesId[] = $linkedItem["PREVIEW_PICTURE"];
            }
        }
        return $picturesId;
    }

    private static function getLinkedElementsPicturesRef($picturesId)
    {
        $picturesRef = [];
        $res = CFile::GetList([], ["@ID" => implode(",", $picturesId)]);
        while($photo = $res->GetNext()){
            $picturesRef[$photo["ID"]] = "/upload/".$photo["SUBDIR"]."/".$photo["FILE_NAME"];
        }
        return $picturesRef;
    }

    private static function addToLinkedElementsRealPicturesPath($linkedItems, $picturesRef)
    {
        foreach($linkedItems as $k => $linkedItem){
            $linkedItems[$k]["PICTURE_SRC"] = $picturesRef[$linkedItem["PREVIEW_PICTURE"]];
        }
        return $linkedItems;
    }

    private static function getLinkedItemsId($linkedItems)
    {
        $linkedItemsId = [];
        foreach($linkedItems as $linkedItem){
            $linkedItemsId[] = $linkedItem["ID"];
        }
        return $linkedItemsId;
    }

    private static function makeLinkedElements($linkedItems)
    {
        $linkedItemsId = self::getLinkedItemsId($linkedItems);

        $arOrder  = [];
        $arFilter = [
            "ID" => $linkedItemsId,
        ];
        $arLimit  = [];
        $arSelect = [
            "IBLOCK_ID",
            "ID",
            "NAME",
            "PREVIEW_TEXT",
            "DETAIL_TEXT",
        ];
        $res = CIBlockElement::GetList($arOrder, $arFilter, false, $arLimit, $arSelect);

        $linkedElements = [];
        while($obj = $res->GetNext()) {
            $linkedElements[] = array_merge($obj, $linkedItems[$obj["ID"]]);
        }

        return $linkedElements;
    }

}