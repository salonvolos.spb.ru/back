<?
class ModelItemsOverrideProperties
{
    public static function get($prices, $serviceId)
    {
        $pricesId = array_keys($prices);
        $overrideItems = self::getOverrideItems($pricesId, $serviceId);

        if($overrideItems){
            $prices = self::doOverrideProperties($prices, $overrideItems);
            $prices = self::resortPrices($prices);
        }

        return $prices;
    }

    private static function getOverrideItems($pricesId, $serviceId)
    {
       $arSort = [
            "SORT" => "ASC",
            "ID" => "ASC"
        ];

        $arSelect = [
            "ID",
            "IBLOCK_ID",
            "NAME",
            "IBLOCK_SECTION_ID"
        ];

        $arFilter = [
            "IBLOCK_ID" => ID_IBLOCK_OVERRIDE_SERVICE_PRICE_PROPS,
            "ACTIVE" => "Y",
            "PROPERTY_SERVICE" => $serviceId,
            "PROPERTY_PRICE" => $pricesId,
        ];


        $rs_element = CIBlockElement::GetList($arSort, $arFilter, false, false, $arSelect);

        $overrideItems = [];
        while($ob = $rs_element->GetNextElement())
        {
            $fields = $ob->GetFields();
            $properties = $ob->GetProperties();
            $overrideItems[$fields["ID"]] = array_merge($fields, $properties);
        }

        return $overrideItems;
    }

    private static function doOverrideProperties($prices, $overrideItems)
    {
        foreach($overrideItems as $overrideItem){
            $overridePriceId = $overrideItem["PRICE"]["VALUE"];
            if(!empty($prices[$overridePriceId])){
                $prices[$overridePriceId]["SORT"] = $overrideItem["SORT"];
                $prices[$overridePriceId]["HOR_DIVIDER"]["VALUE"] = $overrideItem["HOR_DIVIDER"]["VALUE"];
                $prices[$overridePriceId]["SHOW_AS_LIST"]["VALUE"] = $overrideItem["SHOW_AS_LIST"]["VALUE"];
                $prices[$overridePriceId]["SHOW_AS_TITLE"]["VALUE"] = $overrideItem["SHOW_AS_TITLE"]["VALUE"];
            }
        }

        return $prices;
    }

    private static function resortPrices($prices)
    {
        $refPriceIdToPriceSort = [];
        foreach ($prices as $priceId => $price){
            $refPriceIdToPriceSort[$priceId] = $price["SORT"];
        }
        asort($refPriceIdToPriceSort);
        $sortedIds = array_keys($refPriceIdToPriceSort);

        $resortedPrices = [];
        foreach($sortedIds as $id){
            $resortedPrices[$id] = $prices[$id];
        }

        return $resortedPrices;
    }

}