<?
class ModelSections
{
    private $sectionCode;
    private $arParentSection;

    public function __construct($sectionCode, $arParentSection)
    {
        $this->sectionCode = $sectionCode;
        $this->arParentSection = $arParentSection;
    }

    public function getSections()
    {
        $arFilter = array(
            "IBLOCK_ID" => ID_IBLOCK_PRICES,
            "CHECK_PERMISSIONS" => "Y",
            "ACTIVE" => "Y",
        );

        if($this->arParentSection){
            $arFilter['>LEFT_MARGIN'] = $this->arParentSection['LEFT_MARGIN'];
            $arFilter['<RIGHT_MARGIN'] = $this->arParentSection['RIGHT_MARGIN'];
        }

        $arSelect = [
            "ID",
            "IBLOCK_SECTION_ID",
            "NAME",
            "DEPTH_LEVEL",
            "DESCRIPTION",
        ];

        $rs_section = CIBlockSection::GetList(
            ["LEFT_MARGIN" => "ASC"],
            $arFilter,
            true,
            $arSelect
        );

        $sections = [];
        while($ar_section = $rs_section->Fetch()) {
           $sections[$ar_section["ID"]] = $ar_section;
        }

        return $sections;
    }

    public function combineSectionsAndItems($sections, $items)
    {
        foreach($items as $item){
            $itemSectionId = $item["IBLOCK_SECTION_ID"];
            if(!$itemSectionId){
                $sections["ROOT"]["ITEMS"][] = $item;
            }else{
                $sections[$itemSectionId]["ITEMS"][] = $item;
            }
        }

        return $sections;
    }

    public static function getUrlToPriceSection($serviceId)
    {
        $priceSectionId = getSectionPropertyValue("UF_PRICE_SECTION", ID_IBLOCK_SERVICES, $serviceId);
        if(!$priceSectionId){
            return false;
        }

        $resource = CIBlockSection::GetByID($priceSectionId);
        if($priceSection = $resource->GetNext()){
            return "/tseny/".$priceSection["CODE"]."/";
        }

        return false;
    }

}
