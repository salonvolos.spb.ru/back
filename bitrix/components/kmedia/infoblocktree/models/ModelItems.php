<?
class ModelItems
{
    public function getPricesForService($serviceId)
    {
        $pricesId = getSectionPropertyValue("UF_PRICE", ID_IBLOCK_SERVICES, $serviceId);

        $filter = ["ID" => $pricesId];
        $prices = $this->getBxItems($filter);

        return $prices;
    }

    public function getPricesForSection($sectionsId)
    {
        $filter = false;
        if($sectionsId){
            $filter = ["SECTION_ID" => $sectionsId];
        }

        $prices = $this->getBxItems($filter);

        return $prices;
    }

    private function getBxItems($filter)
    {
       $arSort = [
            "SORT" => "ASC",
            "ID" => "ASC"
        ];

        $arSelect = [
            "ID",
            "IBLOCK_ID",
            "NAME",
            "IBLOCK_SECTION_ID"
        ];

        // Фильтр по умолчанию
        $arFilter = [
            "IBLOCK_ID" => ID_IBLOCK_PRICES,
            "ACTIVE" => "Y"
        ];

        // Фильтр из параметра (Id цен или Id разделов цен)
        if($filter){
            $arFilter = array_merge($arFilter, $filter);
        }

        $rs_element = CIBlockElement::GetList($arSort, $arFilter, false, false, $arSelect);

        $items = [];
        while($ob = $rs_element->GetNextElement())
        {
            $fields = $ob->GetFields();
            $properties = $ob->GetProperties();
            $items[$fields["ID"]] = array_merge($fields, $properties);
        }

        return $items;
    }

    public function prepareItems($items)
    {
        // В массиве Элементов заменяем ID привязанного Раздела на его Символьный путь
        $items = ModelFunctions::replaceSectionIdByUrl($items, "SERVICE_LINK");

        // В названии Услуги Цены может быть заметка ("[note]") - переносим в отдельное свойство
        $items = $this->prepareNames($items);

        // Формируем правильные Цены из строки типа: короткие волосы (подсказка):1600|1700 (1900)|4800
        $items = ModelPriceHelper::preparePrices($items);

        // Если в Материалах есть подсказка - выделяем ее в отдельное поле
        $items = $this->prepareMaterials($items);

        return $items;
    }

    private function prepareMaterials($items)
    {
        foreach($items as $id => $item){
            $materials = $item["MATERIAL"]["VALUE"];

            if(!$materials){
                $items[$id]["MATERIAL"] = false;
                continue;
            }

            $arMaterials = [];
            foreach($materials as $key => $material){

                // Если в названии Материала есть Заметка - получаю "название Материала без заметки" и саму "Заметку"
                $note = ModelFunctions::getNoteFromBracketsInText($material);

                $arMaterial = [];

                if(!$note){
                    $arMaterial["NAME"] = $material;
                }else{
                    $arMaterial = [
                        "NAME" => $note["text"],
                        "NOTE" => $note["note"],
                    ];
                }

                $arMaterials[] = $arMaterial;
            }

            $items[$id]["MATERIAL"] = $arMaterials;

        }

        return $items;
    }

    private function prepareNames($items)
    {
        foreach($items as $id => $item){

            // Если есть Заметка из отдельного поля
            if(!empty($item["NAME_NOTE"]["VALUE"])){
                $items[$id]["NAME_NOTE"] = $items[$id]["NAME_NOTE"]["VALUE"];
            }else{
                $items[$id]["NAME_NOTE"] = false;

                $name = $item["~NAME"];

                // Если в Названии есть Заметка - получаю "Название без заметки" и саму "Заметку"
                $note = ModelFunctions::getNoteFromBracketsInText($name);

                if(!$note){
                    continue;
                }else{
                    $items[$id]["NAME"] = $note["text"];
                    $items[$id]["NAME_NOTE"] = $note["note"];
                }
            }

        }

        return $items;
    }

    public function flipMaterialsToLengths($bxItems)
    {
        foreach($bxItems as $k => $bxItem){
            if(!empty($bxItem["MATERIAL"]["VALUE"]) && empty($bxItem["PRICE"]["VALUE"])){
                $bxItems[$k]["PRICE"]["VALUE"] = $bxItem["MATERIAL"]["VALUE"];
                $bxItems[$k]["MATERIAL"]["VALUE"] = false;
            }
        }

        return $bxItems;
    }

    public function flipMaterialsToBasePrice($bxItems)
    {
        foreach($bxItems as $k => $bxItem){
            if(!empty($bxItem["MATERIAL"]["VALUE"]) && empty($bxItem["PRICE"]["VALUE"])){
                $bxItems[$k]["BASE_PRICE"]["VALUE"] = [];
                foreach ($bxItem["MATERIAL"]["VALUE"] as $keyMaterial => $material){
                    $explodeMaterialBySemicolon = explode(":", $material);
                    $explodeMaterialBySemicolon = array_map("trim", $explodeMaterialBySemicolon);
                    $bxItems[$k]["MATERIAL"]["VALUE"][$keyMaterial] = $explodeMaterialBySemicolon[0]; // Название Материала
                    $bxItems[$k]["BASE_PRICE"]["VALUE"][] = $explodeMaterialBySemicolon[1]; // Добавить базовую цену
                }
            }
        }

        return $bxItems;
    }

    public function deleteMaterialPrices($bxItems)
    {
        foreach($bxItems as $bxKey => $bxItem){
            if(!empty($bxItem["PRICE"]["VALUE"])){
                if(!empty($bxItem["MATERIAL"]["VALUE"])){
                    foreach($bxItem["MATERIAL"]["VALUE"] as $matKey => $matVal){
                        if(strpos($matVal, ":") !== false){
                            $matValWoPrice = explode(":", $matVal);
                            $bxItems[$bxKey]["MATERIAL"]["VALUE"][$matKey] = $matValWoPrice[0];
                        }
                    }
                }
            }
        }

        return $bxItems;
    }

    public function sortChildPricesRightUnderParent($prices)
    {
        // Дочерние цены (ключи = Id родительской цены)
        $pricesChild = [];
        foreach($prices as $priceId => $price){
            if(!empty($price["PARENT_PRICE"]["VALUE"])){
                $priceParentId = $price["PARENT_PRICE"]["VALUE"];
                $pricesChild[$priceParentId] = $price;
            }
        }

        if(!$pricesChild){
            return $prices;
        }

        $pricesSorted = [];
        foreach($prices as $priceId => $price){

            // Если у цены нет родителя -> просто добавляем в сортированный массив
            if(empty($price["PARENT_PRICE"]["VALUE"])){
                $pricesSorted[$priceId] = $price;
            }

            // Если у этой цены есть дети -> добавляем в сортированный массив сразу после этого элемента
            if(!empty($pricesChild[$priceId])){
                $priceChild = $pricesChild[$priceId];
                $priceChildId = $priceChild["ID"];
                $pricesSorted[$priceChildId] = $priceChild;
            }

        }

        return $pricesSorted;
    }

    public function setShortNameAsMainNameByCondition($items, $isOnPricesSection = false)
    {
        foreach($items as $id => $item){

            if(empty($item["SHORT_NAME"]["VALUE"])){
                continue;
            }

            if(!empty($item["PARENT_PRICE"]["VALUE"])){

                // На странице Цен: Короткое название Дочерней цены всегда становится основным
                if($isOnPricesSection){
                    $items[$id]["NAME"] = $item["SHORT_NAME"]["VALUE"];
                }else{
                    // На странице Услуг: Короткое название Дочерней цены становится основным только если
                    // у Родильской цены установлено свойство "SHOW_AS_TITLE"
                    $parentPriceId = $item["PARENT_PRICE"]["VALUE"];
                    $parentPrice = $items[$parentPriceId];
                    if($parentPrice["SHOW_AS_TITLE"]["VALUE"] == "Y"){
                        $items[$id]["NAME"] = $item["SHORT_NAME"]["VALUE"];
                    }
                }
            }
        }

        return $items;
    }


}
