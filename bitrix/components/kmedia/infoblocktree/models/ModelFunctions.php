<?
class ModelFunctions
{
    public static function replaceSectionIdByUrl($items, $linkedServiceCode)
    {
        $linkedServicesId = self::getLinkedServicesId($items, $linkedServiceCode);

        // Получаем массив связок ID раздела - символьный путь Раздела
        $rs_sections = CIBlockSection::GetList(
            [],
            ['=ID' => $linkedServicesId],
            false,
            ['SECTION_PAGE_URL']
        );

        $linkedServicesArr = [];
        while($ar_section = $rs_sections->GetNext()) {
            $linkedServicesArr[$ar_section["ID"]] = $ar_section["SECTION_PAGE_URL"];
        }

        // В массиве Элементов заменяем ID привязанного Раздела на его Символьный путь
        foreach($items as $key => $ar_element){
            if($ar_element[$linkedServiceCode]["VALUE"]){
                $serviceId = (int)$ar_element[$linkedServiceCode]["VALUE"];
                $items[$key][$linkedServiceCode] = $linkedServicesArr[$serviceId];
            }else{
                $items[$key][$linkedServiceCode] = false;
            }
        }

        return $items;
    }

    private static function getLinkedServicesId($items, $linkedServiceCode)
    {
        $linkedServicesId = [];
        foreach($items as $item){
            if($item[$linkedServiceCode]["VALUE"]){
                $linkedServicesId[] = $item[$linkedServiceCode]["VALUE"];
            }
        }

        return $linkedServicesId;
    }

    public static function getNoteFromBracketsInText($text)
    {
        if(!preg_match('/\[.*\]/', $text, $matches)){
            return false;
        }

        $note = $matches[0];

        $text = str_replace($note, "", $text);
        $text = trim($text);

        $note = str_replace(["[", "]"], "", $note);
        $note = trim($note);

        $result = [
            "text" => $text,
            "note" => $note
        ];

        return $result;
    }

    public static function getDiscountFromParenthesesInText($text)
    {
        if(!preg_match('/\(.*\)/', $text, $matches)){
            return false;
        }

        $discount = $matches[0];

        $text = str_replace($discount, "", $text);
        $text = trim($text);

        $discount = str_replace(["(", ")"], "", $discount);
        $discount = trim($discount);

        $result = [
            "text" => $text,
            "discount" => $discount
        ];

        return $result;
    }


}