<?
class ModelMaterial
{
    public static function isPricestWithMaterials($items)
    {
        foreach($items as $item){
            if(!empty($item["MATERIAL"])){
                return true;
            }
        }

        return false;
    }

    public static function getUniqueMaterials($items)
    {
        $materials = [];
        foreach($items as $item){
            if(!empty($item["MATERIAL"])){
                foreach ($item["MATERIAL"] as $material){
                    // array_unique не работает с двумерными массивами, поэтому serialize
                    $materials[] = serialize($material);
                }
            }
        }

        $materials = array_unique($materials);

        $uniqueMaterials = [];
        foreach($materials as $material){
            $uniqueMaterials[] = unserialize($material);
        }

        return $uniqueMaterials;
    }

    // Задача сделать кол-во и порядок цен в массиве цен соотв-им порядку имен столбцов (названиям Материалов).
    // Индекс Цены равен индексу Материала в Элементе Цена.
    // Если в Элементе Цены не найден уникальный Материал - будет пустая ячейка ("")
    public static function mixPricesWithMaterials($items, $uniqueMaterials)
    {
        foreach($items as $id => $item){

            foreach($item["PRICE"] as $kPrice => $prices){
                $items[$id]["PRICE"][$kPrice]["values"] = self::getMixedPricesWithMaterials(
                    $prices["values"],
                    $item["MATERIAL"],
                    $uniqueMaterials
                );
            }

            $items[$id]["BASE_PRICE"] = self::getMixedPricesWithMaterials(
                $item["BASE_PRICE"]["VALUE"],
                $item["MATERIAL"],
                $uniqueMaterials
            );

        }

        return $items;
    }

    private static function getMixedPricesWithMaterials($servicePrices, $serviceMaterials, $uniqueMaterials)
    {
        $result = [];
        foreach($uniqueMaterials as $uniqueMaterial){
            $index = self::searchUniqueMaterialInServiceMaterials($uniqueMaterial, $serviceMaterials);
            if($index === false){
                $result[] = ""; // пустая ячейка
            }else{
                $result[] = $servicePrices[$index];
            }
        }

        return $result;
    }

    // Возвращает индекс Материала (он же индекс Цены) если уник. Материал есть в текущей Цена Услуги
    private static function searchUniqueMaterialInServiceMaterials($uniqueMaterial, $serviceMaterials)
    {
        $uniqueMaterial = serialize($uniqueMaterial);
        foreach($serviceMaterials as $index => $serviceMaterial){
            if(serialize($serviceMaterial) == $uniqueMaterial){
                return $index;
            }
        }

        return false;
    }
}
