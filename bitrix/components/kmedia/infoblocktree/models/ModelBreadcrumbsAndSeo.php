<?
class ModelBreadcrumbsAndSeo
{
    public $rootSectionName;
    public $rootSectionDescription;
    public $arParentSection;
    private $sectionCode;

    public function __construct($sectionCode)
    {
        $this->sectionCode = $sectionCode;
    }

    public function add()
    {
        if(!$this->sectionCode){
            return;
        }

        $rsSections = CIBlockSection::GetList(
            [],
            [
                'IBLOCK_ID' => ID_IBLOCK_PRICES,
                '=CODE' => $this->sectionCode
            ]
        );

        $this->arParentSection = $rsSections->Fetch();

        global $APPLICATION;
        $APPLICATION->AddChainItem($this->arParentSection["NAME"]);

        $ipropValues = new \Bitrix\Iblock\InheritedProperty\SectionValues($this->iBlockId, $this->arParentSection["ID"]);
        $sectionSeoProperties = $ipropValues->getValues();

        $this->rootSectionName = $this->arParentSection["NAME"];

        $sectionSeoTitle = $sectionSeoProperties["SECTION_PAGE_TITLE"];
        if($sectionSeoTitle){
            $APPLICATION->SetTitle($sectionSeoTitle);
            $this->rootSectionName = $sectionSeoTitle;
        }

        if($this->arParentSection["DESCRIPTION"]){
            $this->rootSectionDescription = $this->arParentSection["DESCRIPTION"];
        }

        $sectionSeoMetaTitle = $sectionSeoProperties["SECTION_META_TITLE"];
        if($sectionSeoMetaTitle){
            $APPLICATION->SetPageProperty("title", $sectionSeoMetaTitle);
        }

        $sectionSeoMetaKeywords = $sectionSeoProperties["SECTION_META_KEYWORDS"];
        if($sectionSeoMetaKeywords){
            $APPLICATION->SetPageProperty("keywords", $sectionSeoMetaKeywords);
        }

        $sectionSeoMetaDescription = $sectionSeoProperties["SECTION_META_DESCRIPTION"];
        if($sectionSeoMetaDescription){
            $APPLICATION->SetPageProperty("description", $sectionSeoMetaDescription);
        }
    }

}
