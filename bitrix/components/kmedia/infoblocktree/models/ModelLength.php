<?
class ModelLength
{
    public static function isPricestWithLengths($items)
    {
        foreach($items as $item){
            if(!empty($item["PRICE"])){
                return true;
            }
        }

        return false;
    }

    public static function getUniqueLengths($items)
    {
        $lengths = [];
        foreach($items as $item){

            if($item["SHOW_AS_LIST"]["VALUE"] == "Y"){
                continue;
            }

            if(!empty($item["PRICE"])){
                foreach ($item["PRICE"] as $price){
                    // array_unique не работает с двумерными массивами, поэтому serialize
                    if(!empty($price["values"])){
                        unset($price["values"]);
                    }
                    $lengths[] = serialize($price);
                }
            }
        }

        if(!$lengths){
            return false;
        }

        $lengths = array_unique($lengths);

        $uniqueLengths = [];
        foreach($lengths as $length){
            $uniqueLengths[] = unserialize($length);
        }

        return $uniqueLengths;
    }

    public static function mixPricesWithLengths($items, $uniqueLengths)
    {
        foreach ($items as $id => $item){

            if(!$item["PRICE"] || $item["SHOW_AS_LIST"]["VALUE"] == "Y"){
                continue;
            }

            $items[$id]["PRICE"] = self::getMixedPricesWithLengths(
                $item["PRICE"],
                $uniqueLengths
            );

        }

        return $items;
    }

    public function orderItemsWoLengthsBelowList($items)
    {
        $resultShowAsList = [];
        $resultShowAsColumn = [];
        foreach($items as $k => $item){
            if($item["SHOW_AS_LIST"]["VALUE"] == "Y"){
                $resultShowAsList[] = $item;
            }else{
                $resultShowAsColumn[] = $item;
            }
        }

        if(empty($resultShowAsList)){
            return $items;
        }

        $resultShowAsList[0]["HOR_DIVIDER"]["VALUE"] = "Y";

        $items = array_merge($resultShowAsColumn, $resultShowAsList);

        $result = [];

        foreach($items as $item){
            $result[$item["ID"]] = $item;
        }

        return $result;
    }

    public function addBasePriceAsSimplePrice($items)
    {
        foreach($items as $k => $item){
            if($item["BASE_PRICE"]["VALUE"] && empty($item["PRICE"])){
                $items[$k]["PRICE"][] = [
                    "value" => $item["BASE_PRICE"]["VALUE"]." руб."
                ];
            }
        }

        return $items;
    }

    private static function getMixedPricesWithLengths($pricesLengths, $uniqueLengths)
    {
        $result = [];
        foreach($uniqueLengths as $uniqueLength){
            $index = self::searchUniqueLengthInPricesLengths($uniqueLength, $pricesLengths);
            if($index === false){
                $result[] = ""; // пустая ячейка
            }else{
                $result[] = $pricesLengths[$index]["values"][0];
            }
        }

        return $result;
    }

    private static function searchUniqueLengthInPricesLengths($uniqueLength, $pricesLengths)
    {
        $uniqueLength = serialize($uniqueLength);

        foreach($pricesLengths as $index => $priceLength){

            if(!empty($priceLength["values"])){
                unset($priceLength["values"]);
            }

            if(serialize($priceLength) == $uniqueLength){
                return $index;
            }

        }

        return false;
    }

}
