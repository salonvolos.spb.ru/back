<?
class ModelPriceHelper
{
    public static function preparePrices($items)
    {
        foreach($items as $id => $item){
            $itemPricesRaw = $items[$id]["PRICE"]["VALUE"];
            $itemPricesPure = [];

            foreach($itemPricesRaw as $itemPriceRaw){
                $pricePure = self::parsePriceString($itemPriceRaw);
                if($pricePure){
                    $itemPricesPure[] = $pricePure;
                }
            }

            $itemPricesPure = self::addRubSignToPrice($itemPricesPure);

            $items[$id]["PRICE"] = $itemPricesPure;

            if(self::isPricesHaveDiscount($itemPricesPure)){
                $items[$id]["isPricesHaveDiscount"] = true;
            }

        }

        return $items;
    }

    private static function parsePriceString($priceString)
    {
        // если в названии цены нет двоеточия - выход
        if(!$priceString || strpos($priceString, ":") == false){
            return false;
        }

        $result = [];

        $explodeByColon = explode(":", $priceString);
        $name = trim($explodeByColon[0]);
        $values = trim($explodeByColon[1]);

        // Если в названии Цены есть Заметка - получаю "название Цены без заметки" и саму "Заметку"
        $note = ModelFunctions::getNoteFromBracketsInText($name);

        if($note){
            $name = $note["text"];
            $result["note"] = $note["note"];
        }

        $result["name"] = $name;

        // если одна цена (нет верт. черты) - добавить цену в результат и вернуть его
        if(strpos($priceString, "|") === false){
           $priceValues[] = $values;
        }else{
            $explodeByVerticalBar = explode("|", $values);
            $priceValues = array_map("trim", $explodeByVerticalBar);
        }

        $priceValues = self::getDiscountFromPriceString($priceValues);

        $result["values"] = $priceValues;

        return $result;
    }

    private static function isPricesHaveDiscount($prices)
    {
        foreach($prices as $price){
            foreach($price["values"] as $value){
                if(!empty($value["discount"])){
                    return true;
                }
            }
        }

        return false;
    }

    private static function getDiscountFromPriceString($prices)
    {
        foreach($prices as $key => $price){

            // Если в значении Цены есть Дискаунт - получаю "Цену без Дискаунта" и сам "Дискаунт"
            $discount = ModelFunctions::getDiscountFromParenthesesInText($price);

            if(!$discount){
                $prices[$key] = ["value" => $price];
            }else{
                $prices[$key] = [
                    "value" => $discount["text"],
                    "discount" => $discount["discount"],
                ];
            }

        }

        return $prices;
    }

    private static function addRubSignToPrice($prices)
    {
        foreach($prices as $kPrices => $price){
            foreach($price["values"] as $kValues => $value){
                if(is_numeric($value["value"])){
                    $prices[$kPrices]["values"][$kValues]["value"] = $value["value"]. " руб.";
                }
                if(is_numeric($value["discount"])){
                    $prices[$kPrices]["values"][$kValues]["discount"] = $value["discount"]. " руб.";
                }
            }
        }

        return $prices;
    }

}
