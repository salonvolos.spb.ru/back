<? // Цикл по Разделам Цен ?>
<? foreach ($arResult["ITEMS"] as $key => $arItem): ?>

    <? if($key == "ROOT"){continue;} ?>

    <? if(empty($arResult["ROOT_SECTION_NAME"])): // выводим заголовок Раздела только в корневом Разделе ?>
        <div class="prices-table__row prices-table__row--header">
            <div class="prices-table__cell prices-table__cell--title"><?= $arItem["NAME"] ?></div>
        </div>
     <? endif ?>

    <? if($arItem["ITEMS"]): ?>
        <? foreach($arItem["ITEMS"] as $service): ?>
            <? include __DIR__."/item.php" ?>
        <? endforeach ?>
    <? else: ?>
        <p>В разделе нет цен</p>
    <? endif ?>

<? endforeach ?>

<? if(!empty($arResult["ITEMS"]["ROOT"])): ?>
    <div class="prices-table__row" style="margin-top: 30px;"></div>
    <? $arItem = $arResult["ITEMS"]["ROOT"] ?>
    <? foreach($arItem["ITEMS"] as $service): ?>
        <? include __DIR__."/item.php" ?>
    <? endforeach ?>
<? endif ?>