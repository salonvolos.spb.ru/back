<div class="prices-block__bottom" id="page_online_record">
    <a
        class="btn prices-block__record-online"
        href="#"
        onclick="show_online_record(248, <?= $serviceOnlineCode ?>); return false;"
    >Записаться online</a>
    <div class="prices-block__clarification">
        При записи вы выбираете день, <br>время и мастера.
    </div>
    <div class="prices-block__phone phone_switch">+7 (812) 385-01-51</div>

    <? if($arResult["URL_TO_PRICE_SECTION"]): ?>
        <div class="prices-block__all-prices">
            <a href="<?= $arResult["URL_TO_PRICE_SECTION"] ?>">Все цены</a>
        </div>
    <? endif ?>

</div>