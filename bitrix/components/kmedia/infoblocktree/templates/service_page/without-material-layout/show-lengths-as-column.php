<div
    class="
        prices-table__row
        <?= ($service["isPricesHaveDiscount"]) ? "prices-table__row--discount" : "" ?>
        <?= ($service["HOR_DIVIDER"]["VALUE"] == "Y") ? "prices-table__row--sep" : "" ?>
        <?= ($service["SHOW_AS_TITLE"]["VALUE"] == "Y") ? "prices-table__row--subheader" : "" ?>
    "
>

     <div
        class="
            prices-table__cell
            <?= ($service["SHOW_AS_TITLE"]["VALUE"] == "Y") ? "prices-table__cell--title" : "prices-table__cell--row-header" ?>
        "
    >
        <?= $service["NAME"] ?><? if($service["NAME_NOTE"]): ?><div class="prices-table__clarification"><?= $service["NAME_NOTE"] ?></div><? endif ?>
     </div>

    <? if($service["PRICE"]): ?>
        <? foreach ($service["PRICE"] as $price): ?>
            <div class="prices-table__cell"><?= $price["value"] ?></div>
        <? endforeach ?>
    <? endif ?>

</div>
