<h2 class="page-subheader">Услуги и цены</h2>

<div class="block-wrap  block-wrap_wrap block-wrap_align-center block-wrap_justify-center ">
    <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width10 block-wrap__item_s-width6">
        <div class="prices-block">

            <div class="prices-table">
                <? foreach($arResult["ITEMS"] as $service): ?>
                    <div class="prices-table__row prices-table__row--subheader">

                        <div class="prices-table__cell prices-table__cell--title">
                            <?= $service["NAME"] ?><? if($service["NAME_NOTE"]): ?><div class="prices-table__clarification"><?= $service["NAME_NOTE"] ?></div><? endif ?>
                        </div>

                        <? if($service["BASE_PRICE"]["VALUE"] != ""): ?>
                            <div class="prices-table__cell"><?= $service["BASE_PRICE"]["VALUE"] ?> руб.</div>
                        <? endif ?>

                    </div>
                <? endforeach ?>
            </div>

            <? include __DIR__."/../page_online_record.php"; ?>

        </div>
    </div>
</div>
