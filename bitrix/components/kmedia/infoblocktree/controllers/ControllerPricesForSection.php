<?
class ControllerPricesForSection
{
    public static function getData($sectionCode, $arResult)
    {
        // Формируем крошки и добавляем SEO на страницу (если не главная Раздела, а подРаздел)
        $modelBreadcrumbsAndSeo = new ModelBreadcrumbsAndSeo($sectionCode);
        $modelBreadcrumbsAndSeo->add();

        $arParentSection = $modelBreadcrumbsAndSeo->arParentSection;

        // Запрос по Разделам
        $modelSections = new ModelSections($sectionCode, $arParentSection);
        $sections = $modelSections->getSections();
        $sectionsId = array_keys($sections);

        // Запрос по Элементам
        $modelItems = new ModelItems($sectionCode, $arParentSection);
        $bxItems = $modelItems->getPricesForSection($sectionsId);
        $bxItems = $modelItems->sortChildPricesRightUnderParent($bxItems);

        // Ставим Короткое название как основное для Дочерней Цены
        $bxItems = $modelItems->setShortNameAsMainNameByCondition($bxItems, true);

        // Если есть цены Материалов без Длин -> перенести Материалы в Длины
        $bxItems = $modelItems->flipMaterialsToLengths($bxItems);

        $items = $modelItems->prepareItems($bxItems);

        // Формируем результирующий массив Элементов с разбивкой по род. Разделам
        $sections = $modelSections->combineSectionsAndItems($sections, $items);

        $arResult["ITEMS"] = $sections;
        $arResult["ROOT_SECTION_NAME"] = $modelBreadcrumbsAndSeo->rootSectionName;
        $arResult["ROOT_SECTION_DESCRIPTION"] = $modelBreadcrumbsAndSeo->rootSectionDescription;

        return $arResult;
    }
}