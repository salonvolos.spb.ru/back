<?
class ControllerPricesForService
{
    private static $layout;
    private static $uniqueMaterials;
    private static $uniqueLengths;

    public static function getData($serviceId, $arResult)
    {
        $modelItems = new ModelItems;

        $bxItems = $modelItems->getPricesForService($serviceId);
        $bxItems = ModelItemsOverrideProperties::get($bxItems, $serviceId);

        // Ставим Короткое название как основное для Дочерней Цены если у Родителя установлено свойство "Подзаголовок"
        $bxItems = $modelItems->setShortNameAsMainNameByCondition($bxItems);

        // Если есть цены Материалов без Длин -> перенести Материалы в Базовую цену
        $bxItems = $modelItems->flipMaterialsToBasePrice($bxItems);

        // Если есть цены и для Материалов и для Длин -> удалить цены Материалов
        $bxItems = $modelItems->deleteMaterialPrices($bxItems);

        $items = $modelItems->prepareItems($bxItems);

        if(ModelMaterial::isPricestWithMaterials($items)){
            $uniqueMaterials = ModelMaterial::getUniqueMaterials($items);
            $items = ModelMaterial::mixPricesWithMaterials($items, $uniqueMaterials); // добавить пустые ячейки
            self::$uniqueMaterials = $uniqueMaterials; // для шапки
            self::$layout = "with_materials"; // для подключения шаблона
        }else{
            if(ModelLength::isPricestWithLengths($items)){
                $uniqueLengths = ModelLength::getUniqueLengths($items);
                if($uniqueLengths){
                    $items = ModelLength::mixPricesWithLengths($items, $uniqueLengths); // добавить пустые ячейки

                    // расположить Цены с пометкой "Всегда выводить списком" внизу от Цен с длинами + отделить гор. бордером
                    $items = ModelLength::orderItemsWoLengthsBelowList($items);

                    // Если есть только Базовая цена - добавить ее как простуцю цену
                    //$items = ModelLength::addBasePriceAsSimplePrice($items);

                    self::$uniqueLengths = $uniqueLengths; // для шапки
                }
                self::$layout = "without_materials";
            }else{
                self::$layout = "only_base_price";
            }
        }

        $items = $modelItems->sortChildPricesRightUnderParent($items);

        $arResult["ITEMS"] = $items;
        $arResult["LAYOUT"] = self::$layout;
        $arResult["UNIQUE_MATERIALS"] = self::$uniqueMaterials;
        $arResult["UNIQUE_LENGTHS"] = self::$uniqueLengths;
        $arResult["URL_TO_PRICE_SECTION"] = ModelSections::getUrlToPriceSection($serviceId);

        return $arResult;
    }


}