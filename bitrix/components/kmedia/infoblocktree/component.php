<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

require __DIR__."/includes/include_controllers.php";
require __DIR__."/includes/include_models.php";

// Мы на странице Услуги
if($arParams["SERVICE_ID"]){
    try {
        $arResult = ControllerPricesForService::getData($arParams["SERVICE_ID"], $arResult);
    }
    catch(Exception $e) {
        $arResult["ERROR"] = $e->getMessage();
    }
}

// Мы на странице Цен
if(isset($arParams["SECTION_CODE"])){
    try{
        $arResult = ControllerPricesForSection::getData($arParams["SECTION_CODE"], $arResult);
    }catch (Exception $e){
        $arResult["ERROR"] = $e->getMessage();
    }
}

$this->IncludeComponentTemplate();