<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="robots" content="index, follow">
  <meta name="keywords" content="Keywords">
  <meta name="description" content="Cалон красоты у м. Фрунзенская и Балтийская, парикмахерские услуги">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="/local/front/template/styles/core.css?v=213" rel="stylesheet">
  <title>Салон красоты у м. Фрунзенская и Балтийская, парикмахерские услуги</title>
</head>
<body class="page">
  <header class="header">
    <nav class="header-sites container header__sites">
      <div class="content">
        <ul class="header-sites__list">
          <li class="header-sites__item header-sites__item--hairdesign"><a href="http://www.hairdesign.ru"><span class="hidden-xs hidden-s hidden-m">Центр<br>замещения волос</span><span class="hidden-l hidden-xl hidden-hd">Системы <br> волос </span></a></li>
          <li class="header-sites__item header-sites__item--salonvolos header-sites__item--active hidden-xs hidden-s hidden-m">
              <div class="a-repl"><span>Салон красоты</span></div>
          </li>
          <li class="header-sites__item header-sites__item--vsepariki"><a href="http://www.vsepariki.com"><span class="hidden-xs hidden-s hidden-m">Парики и<br>головные уборы</span><span class="hidden-l hidden-xl hidden-hd">Парики <br> и&nbsp;накладки</span></a></li>
          <li class="header-sites__item header-sites__item--triholog"><a href="http://www.triholog.org/"><span class="hidden-xs hidden-s hidden-m">Лечебная косметика<br>для волос</span><span class="hidden-l hidden-xl hidden-hd">Лечебная <br> косметика </span></a></li>
          <li class="header-sites__item header-sites__item--zagustitel"><a href="https://zagustitelvolos.com/"><span class="hidden-xs hidden-s hidden-m">Загустители волос</span><span class="hidden-l hidden-xl hidden-hd">Загустители <br> для волос</span></a></li>
        </ul>
      </div>
    </nav>
    <div class="container">
      <div class="content header__inner">
        <a class="logo header__logo" href="/"></a>
        <div class="header__address">
          <span class="icon-inline icon-inline--clock header__address-item hidden-m hidden-s hidden-xs">пн-пт: с 10:00 до 21:00</span>

          <span class="icon-inline icon-inline--clock header__address-item hidden-m hidden-s hidden-xs">сб-вс: с 10:00 до 19:00</span>

          <span class="icon-inline icon-inline--pin header__address-item hidden-m hidden-s hidden-xs">СПб, наб. Обводного канала 108</span>

          <span class="icon-inline icon-inline--metro header__address-item hidden-m hidden-s hidden-xs">Балтийская / Фрунзенская</span>

          <a class="icon-inline icon-inline--pin header__address-item hidden-hd hidden-xl hidden-l" href="/kontakty/">СПб, наб. Обводного канала 108</a>
        </div>
        <div class="header__contacts hidden-m hidden-s hidden-xs">
          <a class="phone phone--icon header__phone" href="tel:+78123850151">+7 (812) 385-01-51</a>
          <a class="header__call-back" href="#" onclick="Comagic.openSitePhonePanel()">Заказать обратный звонок</a>
        </div>
        <a class="header__message hidden-hd hidden-xl hidden-l" href="whatsapp://send?phone=79817245628&amp;text="></a>
        <a class="header__call hidden-hd hidden-xl hidden-l" href="tel:+78123850151"></a>
      </div>
    </div>
  </header>
  <main>
    <section class="container page-section">
      <div class="content">
        <p style="margin-bottom: 32px;color: #b91d33;"><strong>В конце теста обязательно укажите ваше имя и фамилию, нажав на ссылку "Введите ваше имя", которая расположена над зеленой полосой с результатами.</strong></p>
        <!-- Online Test Pad Test Widget #306075)-->
        <div id="otp_wgt_h6mqfot6bacp2"></div>
        <script type="text/javascript">
          var otp_wjs_dt = (new Date).getTime();
          (function (w, d, n, s, rp) {
            w[n] = w[n] || [];
            rp = {};
            w[n].push(function () {
                otp_render_widget(d.getElementById("otp_wgt_h6mqfot6bacp2"), 'onlinetestpad.com', 'h6mqfot6bacp2', rp);
            }); 
            s = d.createElement("script");
            s.type = "text/javascript";
            s.src = "//onlinetestpad.com/js/widget.js?" + otp_wjs_dt;
            s.async = true;
            d.getElementsByTagName("head")[0].appendChild(s);
          })(this, this.document, "otp_widget_callbacks");
        </script>
      </div>
    </section>
  </main>
</body>
<link href="/local/front/template/styles/home.css?v=213" rel="stylesheet">
<link href="/local/front/template/styles/rest.css?v=213" rel="stylesheet">
