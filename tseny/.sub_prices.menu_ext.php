<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)die();

global $APPLICATION;

CModule::IncludeModule("iblock");

$arOrder = ["SORT" => "ASC"];
$arFilter = [
    "IBLOCK_ID" => ID_IBLOCK_PRICES,
    "ACTIVE" => "Y",
    "GLOBAL_ACTIVE" => "Y",
    "DEPTH_LEVEL" => "1",
];
$bIncCnt = false;
$Select = [
    "ID",
    "NAME",
    "IBLOCK_ID",
    "SECTION_PAGE_URL"
];
$NavStartParams = false;

$res = CIBlockSection::GetList($arOrder, $arFilter, $bIncCnt, $Select, $NavStartParams);

$aMenuLinksExt = [];
// наполняем массив меню пунктами меню
while($sectionObj = $res->GetNext())
{
     $aMenuLinksExt[] = [
        $sectionObj["NAME"],
        $sectionObj["SECTION_PAGE_URL"],
        [],
        [],
        ""
    ];
}

// меню сформировано
$aMenuLinks = array_merge($aMenuLinks, $aMenuLinksExt);