<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

$APPLICATION->IncludeComponent("bitrix:menu", "prices", Array(
    "ROOT_MENU_TYPE" => "sub_prices",	// Тип меню для первого уровня
        "MAX_LEVEL" => "1",	// Уровень вложенности меню
        "CHILD_MENU_TYPE" => "",	// Тип меню для остальных уровней
        "USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
        "DELAY" => "N",	// Откладывать выполнение шаблона меню
        "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
        "MENU_CACHE_TYPE" => "N",	// Тип кеширования
        "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
        "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
        "MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
        "COMPONENT_TEMPLATE" => "prices"
    ),
    false
);