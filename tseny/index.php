<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Цены");
$APPLICATION->SetPageProperty("title", 'Цены на услуги в салоне красоты "Hair Care Center"');
$APPLICATION->SetPageProperty("description", 'Цены на услуги в салоне красоты "Hair Care Center. Профессионализм, отдельные кабинеты. Среди наших клиентов актрисы театра и кино, звезды реалити-шоу ★ Записывайтесь: +7 (812) 385-01-51');

$priceSubsectionCode = parse_url($_GET["price_subsection_code"]);
$priceSubsectionCode = $priceSubsectionCode["path"];
$priceSubsectionCode = stripslashes(trim(htmlspecialchars($priceSubsectionCode, ENT_QUOTES)));

include __DIR__."/menu.php";

$APPLICATION->IncludeComponent(
    "kmedia:infoblocktree",
    ".default",
    array(
        "SECTION_CODE" => $priceSubsectionCode,
    ),
    false
);

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');
