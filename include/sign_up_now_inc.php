<section class="container page-section home-appointment">
    <div class="content grid grid--justify--flex-end grid--align-items--center home-appointment__inner">
        <div class="grid__cell grid__cell--l--6 grid__cell--m--6 grid__cell--xs--12 grid grid--justify--center home-appointment__info">
            <div class="grid__cell grid__cell--l--8 grid__cell--xs--12">
                <h2 class="home-appointment__title appear saturate">Запишитесь сейчас</h2>
                <p class="home-appointment__description appear saturate">online на удобное время</p>
                <a class="button home-appointment__button appear saturate ms_booking" data-href="https://n371027.yclients.com/company:170918">Выбрать время и специалиста
                </a>
            </div>
        </div>
    </div>
</section>
<section class="container page-section page-section--home-address">
    <div class="content">
        <h2 class="h1">Наш адрес</h2>
        <div class="page-subsection grid grid--align-items--flex-end block-shifted">
            <div class="grid__cell grid__cell--l--6 grid__cell--xs--12 block-shifted__image">
                <figure class="figure">
                    <picture>
                        <source srcset="/images/home-office.webp, /images/home-office@2x.webp 2x" type="image/webp"/>
                        <source srcset="/images/home-office.jpg, /images/home-office@2x.jpg 2x"/><img class="figure__image" src="/images/home-office.jpg" loading="lazy" decoding="async"/>
                    </picture>
                    <figcaption class="figure__caption figure__caption--article"></figcaption>
                </figure>
            </div>
            <div class="block-marked block-marked--to-border block-marked--to-border--right grid__cell grid__cell--l--6 grid__cell--xs--12 block-shifted__content">
                <div class="page-subsection">
                    <h3>Санкт-Петербург, наб. Обводного канала 108</h3>
                    <p class="text-marked text-marked--padding--left font-size-14">пн-пт: с 10:00 до 21:00
                    </p>
                    <p class="text-marked text-marked--padding--left font-size-14">сб-вс: с 10:00 до 19:00
                    </p>
                </div>
                <div class="page-subsection">
                    <h3>Припарковаться на машине</h3>
                    <p class="text-marked text-marked--padding--left font-size-14">Перед входом парковочные места. Обычно 2-3 места свободные. Если все занято, то правее от здания есть площадка для машин.
                    </p>
                </div>
                <div class="page-subsection">
                    <h3>Добраться пешком</h3>
                    <p class="text-marked text-marked--padding--left font-size-14">
                      <span class="icon-inline icon-inline--metro">Балтийская – 10 мин
                      </span>
                    </p>
                    <p class="text-marked text-marked--padding--left font-size-14">
                      <span class="icon-inline icon-inline--metro">Фрунзенская – 5 мин
                      </span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="page-section page-section--home-map page-section--no-padding--top page-section--no-padding--bottom">
    <iframe src="https://yandex.ru/map-widget/v1/?scroll=false&amp;um=constructor%3A0jFSeud2k6YiSEPpq1L9FIjWP8F6PbZr" frameborder="0" allowfullscreen="true" width="100%" height="450" style="display: block;"></iframe>
</section>
