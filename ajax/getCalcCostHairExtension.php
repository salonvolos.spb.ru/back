<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

class Model
{
    private $db;

    function __construct()
    {
        global $DB;
        $this->db = $DB;
    }

    public function getExtensionCapsulesQty($extensionType, $hairLength, $haircut, $ownHairVolume, $desiredLength)
    {
        $querySelect = "
            SELECT 
              UF_QNT_CAP_CAPSULNOE,
              UF_QNT_CAP_LENTOCHNO
            FROM
              `hl_quantity_capsuls`
            JOIN 
              `hl_hair_length` ON `hl_hair_length`.ID = `hl_quantity_capsuls`.UF_HAIR_LENGTH
            JOIN 
              `hl_haircut` ON `hl_haircut`.ID = `hl_quantity_capsuls`.UF_HAIRCUT
            JOIN 
              `hl_own_hair_volume` ON `hl_own_hair_volume`.ID = `hl_quantity_capsuls`.UF_OWN_HAIR_VOLUME
            JOIN 
              `hl_desired_length` ON `hl_desired_length`.ID = `hl_quantity_capsuls`.UF_DESIRED_LENGTH
            WHERE
                `hl_hair_length`.UF_ALIAS = '$hairLength'
                AND `hl_haircut`.UF_ALIAS = '$haircut'  
                AND `hl_own_hair_volume`.UF_ALIAS = '$ownHairVolume'  
                AND `hl_desired_length`.UF_ALIAS = '$desiredLength'  
        ";

        $resource = $this->db->Query($querySelect);

        if(!$resource){
            return false;
        }

        $qty = $resource->Fetch();

        if($extensionType == "kapsulnoe"){
            return $qty["UF_QNT_CAP_CAPSULNOE"];
        }elseif($extensionType == "lentochnoe"){
            return $qty["UF_QNT_CAP_LENTOCHNO"];
        }

        return false;
    }

    public function getPricePerCapsule($desiredLength, $extensionType)
    {
        $querySelect = "
            SELECT 
              UF_PRICE_CAP_CAPSULN,
              UF_PRICE_CAP_LENTOCH
            FROM
              `hl_price_per_capsula`
            JOIN 
              `hl_desired_length` ON `hl_desired_length`.ID = `hl_price_per_capsula`.UF_DESIRED_LENGTH
            WHERE
                `hl_desired_length`.UF_ALIAS = '$desiredLength'  
        ";

        $resource = $this->db->Query($querySelect);

        if(!$resource){
            return false;
        }

        $price = $resource->Fetch();

        if($extensionType == "kapsulnoe"){
            return $price["UF_PRICE_CAP_CAPSULN"];
        }elseif($extensionType == "lentochnoe"){
            return $price["UF_PRICE_CAP_LENTOCH"];
        }

        return false;
    }

    public function getPriceForWork($extensionType, $nameOfService)
    {
        $alias = $nameOfService."_".$extensionType;

        $querySelect = "
            SELECT 
              UF_PRICE
            FROM
              `hl_price_for_work`
            WHERE
                UF_ALIAS = '$alias'  
        ";

        $resource = $this->db->Query($querySelect);

        if(!$resource){
            return false;
        }

        $price = $resource->Fetch();

        return $price["UF_PRICE"];
    }

}

// Init POST to vars
$extensionType = htmlspecialcharsbx($_POST["extension_type"]);
$hairLength = htmlspecialcharsbx($_POST["hair_length"]);
$haircut = htmlspecialcharsbx($_POST["haircut"]);
$ownHairVolume = htmlspecialcharsbx($_POST["own_hair_volume"]);
$desiredLength = (int)$_POST["desired_length"];

$qty = false;
if(!empty($_POST["ready_qty_from_user"])){
    $qty = (int)$_POST["ready_qty_from_user"];
}

$nameOfService = htmlspecialcharsbx($_POST["name_of_service"]);

$model = new Model();

if(!$qty){
    $qty = $model->getExtensionCapsulesQty($extensionType, $hairLength, $haircut, $ownHairVolume, $desiredLength);
}

if(!$qty){
    die(json_encode([
        "result" => "error",
        "message" => "Ошибка при получении количества капсул"
    ]));
}

$pricePerCapsule = 0;

if($nameOfService == "extension"){
    $pricePerCapsule = $model->getPricePerCapsule($desiredLength, $extensionType);
    if(!$pricePerCapsule){
        die(json_encode([
            "result" => "error",
            "message" => "Ошибка при получении стоимости за капсулу"
        ]));
    }
}


$priceForWork = $model->getPriceForWork($extensionType, $nameOfService);

if(!$priceForWork){
    die(json_encode([
        "result" => "error",
        "message" => "Ошибка при получении стоимости за работу"
    ]));
}

$priceMaterials = $qty * $pricePerCapsule;
$priceWorks = $qty * $priceForWork;
$priceTotal = $priceMaterials + $priceWorks;

die(json_encode([
    "result" => "success",
    "message" => [
        "priceMaterials" => $priceMaterials,
        "priceWorks" => $priceWorks,
        "priceTotal" => $priceTotal,
    ]
]));







