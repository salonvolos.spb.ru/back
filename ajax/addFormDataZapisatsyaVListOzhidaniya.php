<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if(empty($_POST)) {
    die("No data in request");
}

$siteCode = "s1";
$emailEventName = "NEW_ZAPISATSYA_V_LIST_OZHIDANIYA";

//init data
$fio = htmlspecialcharsbx($_POST['fio']);
$phone = htmlspecialcharsbx($_POST['phone']);
$question = htmlspecialcharsbx($_POST['question']);
$reqName = htmlspecialcharsbx($_POST['req_name']);

// AntiSpam
if($reqName){
    die(json_encode([
        "result" => "error",
        "message" => "Ошибка данных формы"
    ]));
}

$dateForUser = date('Y/m/d H:i:s');
$dateForCode = date('Y-m-d-H-i-s');

if(!$fio || !$phone || !$question){
    die(json_encode([
        "result" => "error",
        "message" => "Fio or Phone or Question is empty"
    ]));
}


if($GLOBALS['prod']) {

    $arEventFields = array(
        "DATE" => $dateForUser,
        "FIO" => $fio,
        "PHONE" => $phone,
        "QUESTION" => $question,
    );

    $resSend = CEvent::Send($emailEventName, $siteCode, $arEventFields);
    $returnMsg .= "; Message send: " . $resSend;
}

die(json_encode([
    "result" => "success",
    "message" => $returnMsg
]));