<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$siteCode = "s1";
$emailEventName = "CALCULATOR_FORM_SEND";
$phone = htmlspecialcharsbx($_POST['phone']);
$comment = htmlspecialcharsbx($_POST['comment']);
$returnMsg = "send mail";

    $arEventFields = array(
        'PHONE' => $phone,
        'COMMENTS' => $comment
    );
try {
    if (!empty($phone) && !empty($comment)) {
        $resSend = CEvent::Send($emailEventName, $siteCode, $arEventFields);
        $returnMsg .= "; Message send: " . $resSend;
        die(json_encode([
            "result" => "success",
            "message" => $returnMsg
        ]));
    }
}catch (Exception $e) {
    die(json_encode([
        "result" => "error",
        "message" => $e
    ]));
}

