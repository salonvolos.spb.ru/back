<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');
CHTTP::SetStatus("404 Not Found");
//@define("ERROR_404","Y");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Страница не найдена");
$APPLICATION->SetPageProperty("TITLE", "Страница не существует");
$APPLICATION->SetPageProperty("description", 'К сожалению запрашиваемая вами страница не найдена. Для продолжения работы с сайтом Вы можете вернуться на главную страницу или воспользоваться картой сайта');
$APPLICATION->AddChainItem("Страница не найдена");
?>
    <p class="text-align-center">Неправильно набран адрес или такой страницы на сайте больше не существует.</p>
    <p class="text-align-center">Вернитесь <a href="/">на главную</a> или воспользуйтесь картой сайта.</p>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
